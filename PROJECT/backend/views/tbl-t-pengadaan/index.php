<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('/tbl-r-barang/_menu') ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        
         <?= Html::button('Pengadaan Barang Habis Pakai', ['value'=>Url::to('index.php?r=tbl-t-pengadaan/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>


<?php
        Modal::begin([
                'header'=>'<h4>Form Pengadaan Barang</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           'pengadaan_id',
            // [
            //     'attribute' => 'Tipe Pengadaan',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         $tipe  = backend\models\TblRStatus::find()->where(['kode'=>'tipe_pengadaan_barang', 'no'=>$model->tipe])->one();
            //         if($tipe != NULL){
            //             return $tipe->nama;
            //         }else{
            //             return NULL;
            //         }
                    
            //     },
            // ],
            


            
            'kode_trans',
            'tanggal_trans',
         //   'bulan_trans',
            // 'tahun_trans',
            // 'keterangan:ntext',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
              
            [
                
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
    

</div>
