<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model backend\models\TblRBarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rbarang-search">
    <?php Pjax::begin(); ?>
    
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <!--
    <?= $form->field($model, 'nama')->textInput(['style'=>'width:300px']); ?>
--><div class="form-group">
      <?= $form->field($model, 'kategori')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kategori_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kategori Barang-', 'style'=>'width:300px']) ?>

    <?= $form->field($model, 'jenis')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'jenis_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Jenis Barang-', 'style'=>'width:300px']) ?>

    <?= Html::submitButton('Cari', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
</div>
  

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
