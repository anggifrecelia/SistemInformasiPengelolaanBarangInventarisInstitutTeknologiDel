<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTStokbarang;

/**
 * TblTStokbarangSearch represents the model behind the search form about `backend\models\TblTStokbarang`.
 */
class TblTStokbarangSearch extends TblTStokbarang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stok_id', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['barang_id', 'kode', 'satuan', 'deskripsi', 'deleted', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTStokbarang::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('barang');

        $query->andFilterWhere([
            'stok_id' => $this->stok_id,
//            'barang_id' => $this->barang_id,
//            'gedung_id' => $this->gedung_id,
            'jumlah' => $this->jumlah,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'tbl_t_stokbarang.deleted', 0])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'tbl_r_barang.nama', $this->barang_id])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);
        return $dataProvider;
    }
}
