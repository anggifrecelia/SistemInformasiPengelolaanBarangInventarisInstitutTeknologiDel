<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPeminjamanDetail */

$this->title = $model->peminjaman_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpeminjaman Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->peminjaman_detail_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->peminjaman_detail_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'peminjaman_detail_id',
            'peminjaman_id',
            'barang_id',
            'jumlah',
            'satuan',
            'keterangan',
            'status_pinjam',
            'status_kembali',
            'status_kembali_by',
            'tanggal_kembali',
            'created_date',
            'created_by',
            'created_ip',
            'modified_date',
            'modified_by',
            'modified_ip',
        ],
    ]) ?>

</div>
