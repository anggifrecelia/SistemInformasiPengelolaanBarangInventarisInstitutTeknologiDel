<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblRKerusakanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kerusakan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rkerusakan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('/tbl-t-buku-inventory/_menu') ?>
  
<!-- 
    <p>
        <?= Html::a('Create Tbl Rkerusakan', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'kerusakan_id',
//            'barang_id',
            [
                'attribute' => 'Nama Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $barang = \backend\models\TblRBarang::find()->where(['barang_id'=>$model->barang_id])->one();
                    return $barang->nama;
                },
            ],
            [
                'attribute' => 'Gedung',
                'format' => 'raw',
                'value' => function ($model) {
                    $gedung = backend\models\TblRGedung::findOne(['gedung_id'=>$model->gedung_id]);
                    return $gedung->nama;
                },
            ],
            //'berita_acara_id',
//            'no_laporan',
//            [
//                'attribute' => 'Gedung',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $gedung = backend\models\TblRGedung::find()->where(['gedung_id'=>$model->gedung_id])->one();
//                    return $gedung->nama;
//                },
//            ],
            //'gedung_id',
            'tanggal',
            'jumlah',
            //'satuan',
            // [
            //     'attribute' => 'Satuan',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         $satuan = backend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
            //         return $satuan->nama;
            //     },
            // ],
            'keterangan:ntext',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
