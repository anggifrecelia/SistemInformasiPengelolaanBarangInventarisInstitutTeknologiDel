<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_peminjaman_detail".
 *
 * @property string $peminjaman_detail_id
 * @property string $peminjaman_id
 * @property integer $barang_id
 * @property integer $jumlah
 * @property string $satuan
 * @property string $keterangan
 * @property integer $status_pinjam
 * @property integer $status_kembali
 * @property integer $status_kembali_by
 * @property string $tanggal_kembali
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTPeminjaman $peminjaman
 * @property TblRBarang $barang
 */
class TblTPeminjamanDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_peminjaman_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['peminjaman_id', 'barang_id'], 'required'],
            [['peminjaman_id', 'barang_id', 'jumlah', 'status_pinjam', 'status_kembali', 'status_kembali_by', 'created_by', 'modified_by'], 'integer'],
            [['tanggal_kembali', 'created_date', 'modified_date'], 'safe'],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['keterangan'], 'string', 'max' => 255],
            [['peminjaman_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTPeminjaman::className(), 'targetAttribute' => ['peminjaman_id' => 'peminjaman_id']],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'peminjaman_detail_id' => 'Peminjaman Detail ID',
            'peminjaman_id' => 'Peminjaman ID',
            'barang_id' => 'Nama Barang',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'status_pinjam' => 'Status Pinjam',
            'status_kembali' => 'Status Kembali',
            'status_kembali_by' => 'Status Kembali By',
            'tanggal_kembali' => 'Tanggal Kembali',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeminjaman()
    {
        return $this->hasOne(TblTPeminjaman::className(), ['peminjaman_id' => 'peminjaman_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }
}
