<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblRBarang;
use backend\models\TblRGedung;
use backend\models\TblRStatus;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTBukuInventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tbuku-inventory-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'barang_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Barang -'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'gedung_id')->dropDownList(ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),['prompt'=>'-Pilih Gedung-']) ?>

    <?= $form->field($model, 'baik')->textInput() ?>

    <?= $form->field($model, 'rusak')->textInput() ?>

    <?= $form->field($model, 'satuan')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'satuan_barang')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Satuan']) ?>

    <?= $form->field($model, 'deskripsi')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Lapor', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
