<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_pengadaan_detail".
 *
 * @property string $pengadaan_detail_id
 * @property string $pengadaan_id
 * @property integer $barang_id
 * @property integer $jumlah
 * @property integer $satuan
 * @property string $keterangan
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRBarang $barang
 * @property TblTPengadaan $pengadaan
 */
class TblTPengadaanDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_pengadaan_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'barang_id'], 'required'],
            [['pengadaan_id', 'barang_id', 'jumlah', 'satuan', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['keterangan'], 'string', 'max' => 255],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
            [['pengadaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTPengadaan::className(), 'targetAttribute' => ['pengadaan_id' => 'pengadaan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pengadaan_detail_id' => 'Pengadaan Detail ID',
            'pengadaan_id' => 'Pengadaan ID',
            'barang_id' => 'Barang ID',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengadaan()
    {
        return $this->hasOne(TblTPengadaan::className(), ['pengadaan_id' => 'pengadaan_id']);
    }
}
