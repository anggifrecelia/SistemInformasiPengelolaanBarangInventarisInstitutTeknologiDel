<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTPermintaanDetail;
use frontend\models\TblTPermintaanDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\TblTPermintaanSearch;

/**
 * TblTPermintaanDetailController implements the CRUD actions for TblTPermintaanDetail model.
 */
class TblTPermintaanDetailController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPermintaanDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPermintaanDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPermintaanDetail model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new TblTPermintaanDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTPermintaanDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->permintaan_detail_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTPermintaanDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->permintaan_detail_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPermintaanDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

//    public function actionApprove($id)
//    {
//        $searchModel = new TblTPermintaanSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        
//        $model = $this->findModel($id);
//        
//        $model->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Approved'")->queryScalar();
//        $model->status_by = Yii::$app->user->id;
//        $model->save();
//        
//        return $this->render('/tbl-t-permintaan/index', [
//                'model' => $model,
//                'dataProvider' => $dataProvider,
//            ]);
//    }
//    
//    public function actionReject($id)
//    {
//        $searchModel = new TblTPermintaanSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        
//        $model = $this->findModel($id);
//        
//        $model->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
//        $model->status_by = Yii::$app->user->id;
//        $model->save();
//        
//        return $this->render('/tbl-t-permintaan/index', [
//                'model' => $model,
//                'dataProvider' => $dataProvider,
//            ]);
//    }
    /**
     * Finds the TblTPermintaanDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPermintaanDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPermintaanDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
