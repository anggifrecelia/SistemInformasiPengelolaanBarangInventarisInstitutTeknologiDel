-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2018 at 05:09 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('staff', '2', 1465811438),
('staff', '34', 1526611815),
('staff', '36', 1527353967),
('staff_inventory', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, NULL, NULL),
('approve_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('approve_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('barang', 2, NULL, NULL, NULL, NULL, NULL),
('buku_inventory', 2, NULL, NULL, NULL, NULL, NULL),
('change_password', 3, NULL, NULL, NULL, NULL, NULL),
('create_barang', 3, NULL, NULL, NULL, NULL, NULL),
('create_gedung', 3, NULL, NULL, NULL, NULL, NULL),
('create_kerusakan', 3, NULL, NULL, NULL, NULL, NULL),
('create_pelepasan', 3, NULL, NULL, NULL, NULL, NULL),
('create_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('create_pencatatan', 3, NULL, NULL, NULL, NULL, NULL),
('create_pengadaan', 3, NULL, NULL, NULL, NULL, NULL),
('create_pengeluaran', 3, NULL, NULL, NULL, NULL, NULL),
('create_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('create_status', 3, NULL, NULL, NULL, NULL, NULL),
('create_stok', 3, NULL, NULL, NULL, NULL, NULL),
('delete_barang', 3, NULL, NULL, NULL, NULL, NULL),
('delete_buku', 3, NULL, NULL, NULL, NULL, NULL),
('delete_gedung', 3, NULL, NULL, NULL, NULL, NULL),
('delete_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('delete_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('delete_status', 3, NULL, NULL, NULL, NULL, NULL),
('delete_stok', 3, NULL, NULL, NULL, NULL, NULL),
('dosen', 1, NULL, NULL, NULL, NULL, NULL),
('gedung', 2, NULL, NULL, NULL, NULL, NULL),
('index_barang', 3, NULL, NULL, NULL, NULL, NULL),
('index_buku', 3, NULL, NULL, NULL, NULL, NULL),
('index_gedung', 3, NULL, NULL, NULL, NULL, NULL),
('index_mutasi', 3, NULL, NULL, NULL, NULL, NULL),
('index_pelepasan', 3, NULL, NULL, NULL, NULL, NULL),
('index_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('index_pencatatan', 3, NULL, NULL, NULL, NULL, NULL),
('index_pengadaan', 3, NULL, NULL, NULL, NULL, NULL),
('index_pengeluaran', 3, NULL, NULL, NULL, NULL, NULL),
('index_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('index_status', 3, NULL, NULL, NULL, NULL, NULL),
('index_stok', 3, NULL, NULL, NULL, NULL, NULL),
('kembalikan_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('kerusakan', 2, NULL, NULL, NULL, NULL, NULL),
('mutasi_aset', 2, NULL, NULL, NULL, NULL, NULL),
('pelepasan_aset', 2, NULL, NULL, NULL, NULL, NULL),
('peminjaman', 2, NULL, NULL, NULL, NULL, NULL),
('pencatatan_aset', 2, NULL, NULL, NULL, NULL, NULL),
('pengadaan_barang', 2, NULL, NULL, NULL, NULL, NULL),
('pengeluaran_barang', 2, NULL, NULL, NULL, NULL, NULL),
('pengguna', 2, NULL, NULL, NULL, NULL, NULL),
('permintaan', 2, NULL, NULL, NULL, NULL, NULL),
('reject_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('reject_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('report_berita_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('report_kerusakan', 3, NULL, NULL, NULL, NULL, NULL),
('report_mutasi', 3, NULL, NULL, NULL, NULL, NULL),
('report_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('report_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('search_barang', 3, NULL, NULL, NULL, NULL, NULL),
('search_buku', 3, NULL, NULL, NULL, NULL, NULL),
('search_gedung', 3, NULL, NULL, NULL, NULL, NULL),
('search_kerusakan', 3, NULL, NULL, NULL, NULL, NULL),
('search_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('search_permintaan', 3, NULL, NULL, NULL, NULL, NULL),
('search_stok', 3, NULL, NULL, NULL, NULL, NULL),
('serahkan_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('staff', 1, NULL, NULL, NULL, NULL, NULL),
('staff_inventory', 1, NULL, NULL, NULL, NULL, NULL),
('status', 2, NULL, NULL, NULL, NULL, NULL),
('stok_barang', 2, NULL, NULL, NULL, NULL, NULL),
('update_status', 3, NULL, NULL, NULL, NULL, NULL),
('view_peminjaman', 3, NULL, NULL, NULL, NULL, NULL),
('view_pengadaan', 3, NULL, NULL, NULL, NULL, NULL),
('view_pengeluaran', 3, NULL, NULL, NULL, NULL, NULL),
('view_permintaan', 3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('barang', 'create_barang'),
('barang', 'delete_barang'),
('barang', 'index_barang'),
('barang', 'search_barang'),
('buku_inventory', 'delete_buku'),
('buku_inventory', 'index_buku'),
('buku_inventory', 'search_buku'),
('gedung', 'create_gedung'),
('gedung', 'delete_gedung'),
('gedung', 'index_gedung'),
('gedung', 'search_gedung'),
('kerusakan', 'create_kerusakan'),
('kerusakan', 'report_kerusakan'),
('kerusakan', 'search_kerusakan'),
('mutasi_aset', 'index_mutasi'),
('mutasi_aset', 'report_mutasi'),
('pelepasan_aset', 'create_pelepasan'),
('pelepasan_aset', 'index_pelepasan'),
('peminjaman', 'create_peminjaman'),
('peminjaman', 'index_peminjaman'),
('peminjaman', 'kembalikan_peminjaman'),
('peminjaman', 'search_peminjaman'),
('peminjaman', 'view_peminjaman'),
('pencatatan_aset', 'create_pencatatan'),
('pencatatan_aset', 'index_pencatatan'),
('pengadaan_barang', 'create_pengadaan'),
('pengadaan_barang', 'index_pengadaan'),
('pengadaan_barang', 'view_pengadaan'),
('pengeluaran_barang', 'create_pengeluaran'),
('pengeluaran_barang', 'index_pengeluaran'),
('pengeluaran_barang', 'view_pengeluaran'),
('pengguna', 'change_password'),
('permintaan', 'create_permintaan'),
('permintaan', 'index_permintaan'),
('permintaan', 'search_permintaan'),
('permintaan', 'view_permintaan'),
('status', 'create_status'),
('status', 'delete_status'),
('status', 'index_status'),
('status', 'update_status'),
('stok_barang', 'create_stok'),
('stok_barang', 'delete_stok'),
('stok_barang', 'index_stok'),
('stok_barang', 'search_stok');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1465393237),
('m140506_102106_rbac_init', 1465393244);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_r_barang`
--

CREATE TABLE `tbl_r_barang` (
  `barang_id` int(10) NOT NULL,
  `kategori` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `no_register` varchar(255) NOT NULL COMMENT 'baiknya di buku inventtory',
  `jenis` varchar(255) NOT NULL,
  `Merek` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_r_barang`
--

INSERT INTO `tbl_r_barang` (`barang_id`, `kategori`, `nama`, `no_register`, `jenis`, `Merek`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(15, 2, 'kursi roda ', 'ITD/kkrk_001/512/inv/2001', '0', NULL, 'test', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(16, 2, 'kursi biasa', 'ITD/kkb_001/512/inv/2013 - ITD/kkb_027/512/inv/201', '0', NULL, '-', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(17, 2, 'meja dosen besar', 'ITD/mdb_001/512/inv/2001 - ITD/mdb_009/512/inv/200', '0', NULL, '-', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(18, 2, 'lemari arsip kecil kayu pintu 2', 'ITD/lakw2_001/512/inv/2003', '0', NULL, '-', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(19, 2, 'lemari arsip kecil kayu pintu 1', 'ITD/lakw1_001/512/inv/2001', '0', NULL, '-', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(20, 2, 'kursi roda ', 'ITD/kkrk_001/512/inv/2001', '0', NULL, 'test2', '1', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(21, 2, 'kursi roda ', 'ITD/kkrk_001/512/inv/2001', '2', NULL, 'test3', '0', '2016-06-08', 3, '::1', '2018-05-18 00:00:00', 29, '::1'),
(22, 2, 'kursi biasa', 'ITD/kkb_001/512/inv/2013 - ITD/kkb_027/512/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(23, 2, 'meja dosen besar ', 'ITD/mdb_001/512/inv/2001 - ITD/mdb_009/512/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(24, 2, 'lemari arsip kecil kayu pintu 2', 'ITD/lakw2_001/512/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(25, 2, 'lemari arsip kecil kayu pintu 1', 'ITD/lakw1_001/512/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(26, 2, 'papan tulis berbingkai besar', 'ITD/ptb_001/512/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(27, 2, 'jam dinding', 'ITD/djd_001/512/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(28, 2, 'infokus', 'ITD/eif_001/512/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(29, 2, 'speaker', 'ITD/esp_001/512/inv/2015 - ITD/esp_004/512/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(30, 2, 'flip chart', 'ITD/ptfc_001/512/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(31, 2, 'view screen', 'ITD/dvs_001/512/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(32, 2, 'gorden 1 nako', 'ITD/gk1_001/512/inv/2013 - ITD/gk1_002/512/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(33, 2, 'gorden 2 nako', 'ITD/gk2_001/512/inv/2013 - ITD/gk2_005/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(34, 2, 'lampu ', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(35, 2, 'tong sampah bertutup', 'ITD/tsk_001/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(36, 2, 'meja panjang biru', 'ITD/mkb_001/513/inv/2013 - ITD/mkb_020/513/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(37, 2, 'meja dosen kaca', 'ITD/mkk_001/513/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(38, 2, 'kursi biasa archigamma', 'ITD/kkb_028/513/inv/201 - ITD/kkb_060/513/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(39, 2, 'kursi roda  archigamma', 'ITD/kkrk_002/513/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(40, 2, 'infokus', 'ITD/eif_002/513/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(41, 2, 'view screen', 'ITD/dvs_002/513/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(42, 2, 'papan tulis berbingkai besar', 'ITD/ptb_002/513/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(43, 2, 'AC mitsubishi slim', 'ITD/eAC_001/513/inv/2007 - ITD/eAC_002/513/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(44, 2, 'jam dinding', 'ITD/djd_002/513/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(45, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_002/513/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(46, 2, 'printer canon', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(47, 2, 'gorden 1 nako', 'ITD/gk1_003/513/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(48, 2, 'gorden 2 nako', 'ITD/gk2_006/513/inv/2013 - ITD/gk2_009/513/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(49, 2, 'lemari besi/loker', 'ITD/lol1_001/513/inv/2008 - ITD/lol1_003/513/inv/2', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(50, 2, 'tong sampah abu-abu', 'ITD/tsk_002/513/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(51, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(52, 2, 'meja biru panjang', 'ITD/mkb_021/514/inv/2013 - ITD/mkb_040/514/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(53, 2, 'kursi biasa  archigamma', 'ITD/kkb_061/514/inv/2001 - ITD/kkb_084/514/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(54, 2, 'kursi roda archigamma', 'ITD/kkrk_003/514/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(55, 2, 'speaker', 'ITD/esp_005/514/inv/2015 - ITD/esp_009/514/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(56, 2, 'jam dinding', 'ITD/djd_003/514/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(57, 2, 'screen view', 'ITD/dvs_003/514/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(58, 2, 'infokus', 'ITD/eif_003/514/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(59, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_003/514/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(60, 2, 'meja komputer berkaca', 'ITD/mkk_002/514/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(61, 2, 'jam dinding', 'ITD/djd_003/514/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(62, 2, 'lemari arsip kecil kayu 2 pintu', 'ITD/lakw2_003/514/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(63, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(64, 2, 'speaker', 'ITD/esp_005/514/inv/2015 - ITD/esp_008/514/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(65, 2, 'gorden 1 nako', 'ITD/gk1_004/514/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(66, 2, 'gorden 2 nako', 'ITD/gk2_010/514/inv/2013 - ITD/gk2_013/514/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(67, 2, 'tong sampah abu-abu', 'ITD/tsk_003/514/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(68, 2, 'meja komputer putih', 'ITD/mkp_001/515/inv/2001/2015 - ITD/mkp_036/515/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(69, 2, 'kursi biasa  archigamma', 'ITD/kkb_085/515/inv/2003 - ITD/kkb_110/515/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(70, 2, 'kursi roda high point', 'ITD/kkrk_004/515/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(71, 2, 'meja komputer berkaca', 'ITD/mkk_003/515/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(72, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_004/515/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(73, 2, 'screen view', 'ITD/dvs_004/515/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(74, 2, 'infokus', 'ITD/eif_004/515/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(75, 2, 'jam dinding', 'ITD/djd_003/514/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(76, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_004/515/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(77, 2, 'gorden 2 nako', 'ITD/gk2_014/515/inv/2013 - ITD/gk2_017/515/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(78, 2, 'lampu ', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(79, 2, 'tong sampah abu-abu', 'ITD/tsk_004/515/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(80, 2, 'UPS', 'ITD/eUPS_001/515/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(81, 2, 'kursi kelas archigamma', 'ITD/kkm_001/516/inv/2001 - ITD/kkm_058/516/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(82, 2, 'kursi biasa', 'ITD/kkb_111/516/inv/2001 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(83, 2, 'meja komputer berkaca', 'ITD/mkk_004/516/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(84, 2, 'kursi roda archigamma', 'ITD/kkrk_005/516/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(85, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_005/516/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(86, 2, 'screen view', 'ITD/dvs_005/516/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(87, 2, 'infokus', 'ITD/eif_005/516/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(88, 2, 'jam dinding', 'ITD/djd_004/516/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(89, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_005/516/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(90, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(91, 2, 'speaker', 'ITD/esp_010/516/inv/2015 - ITD/esp_013/516/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(92, 2, 'gorden 2 nako', 'ITD/gk2_018/516/inv/2013 - ITD/gk2_021/516/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(93, 2, 'tong sampah biru', 'ITD/tsk_005/516/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(94, 2, 'meja komputer putih', 'ITD/mkp_037/521/inv/2001/2015 - ITD/mkp_067/521/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(95, 2, 'kursi biasa archigamma', 'ITD/kkb_111/521/inv/2001 - ITD/kkb_143/521/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(96, 2, 'meja komputer berkaca', 'ITD/mkk_005/521/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(97, 2, 'kursi roda merah high point', 'ITD/kkrk_006/521/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(98, 2, 'UPS', 'ITD/eUPS_002/521/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(99, 2, 'AC national', 'ITD/eAC_003/521/inv/2003- ITD/eAC_004/521/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(100, 2, 'PC students', 'ITD/ePC_001/521/duk/2014 - ITD/ePC_031/521/duk/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(101, 2, 'papan tulis besar berbingkai', 'ITD/ptb_006/521/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(102, 2, 'papan tulis kecil', 'ITD/ptk_001/521/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(103, 2, 'screen view', 'ITD/dvs_006/521/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(104, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_006/521/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(105, 2, 'jam dinding', 'ITD/djd_005/521/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(106, 2, 'gorden 1 nako', 'ITD/gk1_005/521/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(107, 2, 'gorden 2 nako', 'ITD/gk2_022/521/inv/2013 - ITD/gk2_026/521/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(108, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(109, 2, 'tong sampah', 'ITD/tsk_006/521/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(110, 2, 'infokus', 'ITD/eif_006/521/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(111, 2, 'meja komputer kaca', 'ITD/mkk_006/522/inv/2001 - ITD/mkk_035/522/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(112, 2, 'kursi biasa archigamma', 'ITD/kkb_144/522/inv/2001 - ITD/kkb_174/522/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(113, 2, 'kursi roda archigamma', 'ITD/kkrk_007/522/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(114, 2, 'UPS', 'ITD/eUPS_003/522/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(115, 2, 'screen view', 'ITD/dvs_007/522/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(116, 2, 'infokus', 'ITD/eif_007/522/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(117, 2, 'papan tulis besar berbingkai', 'ITD/ptb_007/522/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(118, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_007/522/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(119, 2, 'gorden 1 nako', 'ITD/gk1_006/522/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(120, 2, 'gorden 2 nako', 'ITD/gk2_027/522/inv/2013 - ITD/gk2_031/522/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(121, 2, 'jam dinding', 'ITD/djd_006/522/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(122, 2, 'AC national', 'ITD/eAC_005/522/inv/2003- ITD/eAC_006/522/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(123, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(124, 2, 'tong sampah', 'ITD/tsk_007/522/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(125, 2, 'meja komputer putih', 'ITD/mkp_068/523/inv/2001/2015 - ITD/mkp_098/523/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(126, 2, 'kursi biasa archigamma', 'ITD/kkb_175/523/inv/2001 - ITD/kkb_206/523/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(127, 2, 'meja komputer kaca', 'ITD/mkk_036/523/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(128, 2, 'kursi roda archigamma', 'ITD/kkrk_008/523/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(129, 2, 'UPS', 'ITD/eUPS_004/523/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(130, 2, 'screen view', 'ITD/dvs_008/523/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(131, 2, 'infokus', 'ITD/eif_008/523/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(132, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_008/523/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(133, 2, 'AC mitsubishi slim', 'ITD/eAC_007/523/inv/2012- ITD/eAC_008/523/inv/2012', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(134, 2, 'papan tulis besar berbingkai', 'ITD/ptb_008/523/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(135, 2, 'papan tulis kecil', ' ITD/ptk_002/523/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(136, 2, 'jam dinding', 'ITD/djd_007/523/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(137, 2, 'gorden 1 nako', 'ITD/gk1_007/523/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(138, 2, 'gorden 2 nako', 'ITD/gk2_032/523/inv/2013 - ITD/gk2_036/523/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(139, 2, 'lampu ', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(140, 2, 'tong sampah', 'ITD/tsk_008/523/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(141, 2, 'loker', 'ITD/lol1_004/523/inv/2008 - ITD/lol1_008/523/inv/2', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(142, 2, 'meja komputer putih', 'ITD/mkp_099/524/inv/2001/2015 - ITD/mkp_129/524/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(143, 2, 'kursi biasa archigamma', 'ITD/kkb_207/524/inv/2001 - ITD/kkb_237/524/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(144, 2, 'meja komputer kaca', 'ITD/mkk_037/524/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(145, 2, 'kursi roda', 'ITD/kkrk_009/524/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(146, 2, 'kursi kelas', 'ITD/kkm_059/524/inv/2001 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(147, 2, 'AC mitsubishi', 'ITD/eAC_009/524/inv/2003- ITD/eAC_010/524/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(148, 2, 'papan tulis besar berbingkai', 'ITD/ptb_009/524/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(149, 2, 'papan tulis kecil', ' ITD/ptk_003/524/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(150, 2, 'screen view', 'ITD/dvs_009/524/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(151, 2, 'infokus', 'ITD/eif_009/524/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(152, 2, 'jam dinding', 'ITD/djd_008/524/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(153, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_009/524/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(154, 2, 'gorden 1 nako', 'ITD/gk1_008/524/inv/2013 ', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(155, 2, 'gorden 2 nako', 'ITD/gk2_037/524/inv/2013 - ITD/gk2_041/524/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(156, 2, 'tong sampah', 'ITD/tsk_009/524/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(157, 2, 'meja komputer putih', 'ITD/mkp_130/525/inv/2001/2015 - ITD/mkp_158/525/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(158, 2, 'kursi biasa  archigamma', 'ITD/kkb_238/525/inv/2001 - ITD/kkb_266/525/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(159, 2, 'meja hitam', 'ITD/mkh_001/525/inv/2009 - ITD/mkh_005/525/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(160, 2, 'meja komputer kaca', 'ITD/mkk_038/525/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(161, 2, 'kursi roda', 'ITD/kkrk_010/525/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(162, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_010/525/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(163, 2, 'screen view', 'ITD/dvs_010/525/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(164, 2, 'infokus', 'ITD/eif_010/525/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(165, 2, 'AC national', 'ITD/eAC_011/525/inv/2003- ITD/eAC_012/525/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(166, 2, 'UPS', 'ITD/eUPS_005/525/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(167, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_010/525/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(168, 2, 'gorden 2 nako', 'ITD/gk2_042/525/inv/2013 - ITD/gk2_046/525/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(169, 2, 'jam dinding', 'ITD/djd_009/525/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(170, 2, 'tong sampah', 'ITD/tsk_010/525/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(171, 2, 'meja komputer putih', 'ITD/mkp_159/526/inv/2001/2015 - ITD/mkp_188/526/in', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(172, 2, 'kursi roda ', 'ITD/kkrk_011/526/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(173, 2, 'kursi biasa archigamma', 'ITD/kkb_267/526/inv/2001 - ITD/kkb_296/526/inv/200', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(174, 2, 'papan tulis berbingkai besar', 'ITD/ptb_011/526/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(175, 2, 'meja kaca', 'ITD/mkk_039/526/inv/2001', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(176, 2, 'UPS', 'ITD/eUPS_006/526/inv/2007', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(177, 2, 'AC national', 'ITD/eAC_013/526/inv/2003- ITD/eAC_014/526/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(178, 2, 'screen view', 'ITD/dvs_011/526/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(179, 2, 'infokus', 'ITD/eif_011/526/inv/2014', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(180, 2, 'loker', 'ITD/lol1_009/526/inv/2008 - ITD/lol1_010/526/inv/2', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(181, 2, 'gorden 2 nako', 'ITD/gk2_047/526/inv/2013 - ITD/gk2_051/526/inv/201', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(182, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_011/526/inv/2003', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(183, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(184, 2, 'tong sampah', 'ITD/tsk_011/526/inv/2013', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(185, 2, 'jam dinding', 'ITD/djd_010/526/inv/2015', '0', NULL, '-', '0', '2016-06-08', 3, '::1', NULL, NULL, NULL),
(186, 2, 'kursi kelas brother', 'ITD/kkmb_001/711/inv/2013 - ITD/kkmb_048/711/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(187, 2, 'kursi kelas archigamma', 'ITD/kkm_059/711/inv/2001 - ITD/kkm_063/711/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(188, 2, 'meja komputer kaca', 'ITD/mkk_040/711/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(189, 2, 'kursi roda', 'ITD/kkrk_012/711/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(190, 2, 'meja komputer biasa kecil', 'ITD/mdk_001/711/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(191, 2, 'papan tulis besar kapur', 'ITD/ptb_012/711/inv/2015 - ITD/ptb_013/711/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(192, 2, 'jam dinding', 'ITD/djd_011/711/inv/2011', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(193, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_012/711/inv/2003 - ITD/lakw2_013/711/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(194, 2, 'screen view', 'ITD/dvs_012/711/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(195, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(196, 2, 'tong sampah abu-abu', 'ITD/tsk_012/711/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(197, 2, 'gorden 2 nako', 'ITD/gk2_052/711/inv/2014 - ITD/gk2_058/711/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(198, 2, 'infokus', 'ITD/eif_012/711/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(199, 2, 'kursi kelas archigamma', 'ITD/kkm_064/712/inv/2001 - ITD/kkm_120/712/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(200, 2, 'kursi brother/chitose', 'ITD/kkmb_049/712/inv/2013 - ITD/kkmb_083/712/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(201, 2, 'kursi biasa', 'ITD/kkb_297/712/inv/2001 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(202, 2, 'meja kaca', 'ITD/mkk_042/712/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(203, 2, 'infokus', 'ITD/eif_013/712/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(204, 2, 'screen view', 'ITD/dvs_013/712/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(205, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_014/712/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(206, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_014/712/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(207, 2, 'jam dinding', 'ITD/djd_012/712/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(208, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(209, 2, 'tong sampah', 'ITD/tsk_013/712/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(210, 2, 'gorden 2 nako', 'ITD/gk2_059/712/inv/2014 - ITD/gk2_065/712/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(211, 2, 'speaker', 'ITD/esp_014/712/inv/2015 - ITD/esp_017/712/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(212, 2, 'meja kayu segitiga', 'ITD/mks_001/713/inv/2012 - ITD/mks_026/713/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(213, 2, 'meja kaca segitiga', 'ITD/mkk_001/713/inv/2012 - ITD/mkk_016/inv/2012', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(214, 2, 'kursi biasa archigamma', 'ITD/kkb_298/713/inv/2001 - ITD/kkb_334/713/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(215, 2, 'kursi roda ', 'ITD/kkrk_013/713/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(216, 2, 'papan tulis besar tidak berbingkai', 'ITD/ptb_015/713/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(217, 2, 'screen view', 'ITD/dvs_014/713/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(218, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(219, 2, 'tong sampah', 'ITD/tsk_014/713/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(220, 2, 'gorden 2 nako', 'ITD/gk2_066/713/inv/2014 - ITD/gk2_070/713/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(221, 2, 'infokus', 'ITD/eif_014/713/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(222, 2, 'meja kaca', 'ITD/mkk_043/713/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(223, 2, 'jam dinding', 'ITD/djd_013/713/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(224, 2, 'kursi kelas chitose', 'ITD/kkmc_001/721/inv/2013 - ITD/kkmc_057/721/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(225, 2, 'kursi kelas archigamma', 'ITD/kkm_121/721/inv/2001 - ITD/kkm_129/721/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(226, 2, 'kursi biasa archigamma', 'ITD/kkb_335/721/inv/2001 - ITD/kkb_336/721/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(227, 2, 'papan tulis besar kapur', 'ITD/ptb_016/721/inv/2015 - ITD/ptb_017/721/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(228, 2, 'infokus', 'ITD/eif_014/721/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(229, 2, 'screen view', 'ITD/dvs_015/721/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(230, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_015/721/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(231, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(232, 2, 'tong sampah', 'ITD/tsk_015/721/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(233, 2, 'meja kaca', 'ITD/mkk_044/721/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(234, 2, 'gorden 2 nako', 'ITD/gk2_071/721/inv/2014 - ITD/gk2_076/721/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(235, 2, 'kursi kelas biru handuk vinoti', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(236, 2, 'kursi roda merah high point', 'ITD/kkrk_014/722-723/inv/2013 - ITD/kkrk_015/722-7', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(237, 2, 'kursi roda archigamma', 'ITD/kkrk_016/722-723/inv/2001 - ITD/kkrk_017/722-7', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(238, 2, 'kursi biasa', 'ITD/kkb_337/722-723/inv/2001 - ITD/kkb_343/722-723', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(239, 2, 'meja kaca', 'ITD/mkk_045/722-723/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(240, 2, 'meja biasa', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(241, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_016/722-723/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(242, 2, 'AC mitsubishi', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(243, 2, 'jam dinding', 'ITD/djd_014/722-723/inv/2013 - ITD/djd_015/722-723', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(244, 2, 'screen view', 'ITD/dvs_016/722-723/inv/2014 - ITD/dvs_017/722-723', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(245, 2, 'infokus', 'ITD/eif_015/722-723/inv/2014 - ITD/eif_016/722-723', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(246, 2, 'papan tulis roda', 'ITD/ptbr_001/722-723/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(247, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(248, 2, 'remote ac', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(249, 2, 'speaker', 'ITD/esp_018/722-723/inv/2014 - ITD/esp_021/722-723', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(250, 2, 'kursi putar lab', 'ITD/kkpl_001/811/inv/2015 - ITD/kkpl_050/811/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(251, 2, 'papan tulis besar', 'ITD/ptb_001/811/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(252, 2, 'papan tulis kecil', 'ITD/ptk_001/811/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(253, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(254, 2, 'meja MP3', 'ITD/ml3_001/811/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(255, 2, 'infokus', 'ITD/eif_001/811/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(256, 2, 'screen view', 'ITD/dvs_001/811/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(257, 2, 'lemari wallbench no. 3 6 pintu laci 4', 'ITD/lawb3_001/811/inv/2015 - ITD/lawb3_002/811/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(258, 2, 'meja lab MP2', 'ITD/ml2_001/811/inv/2015 - ITD/ml2_008/811/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(259, 2, 'kursi putar lab', 'ITD/kkpl_051/812/inv/2015 - ITD/kkpl_085/812/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(260, 2, 'papan tulis besar', 'ITD/ptb_002/812/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(261, 2, 'papan tulis kecil', 'ITD/ptk_002/812/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(262, 2, 'meja MP3', 'ITD/ml3_002/812/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(263, 2, 'lemari kaca pintu geser', 'ITD/lakg_001/812/inv/2015 - ITD/lakg_005/812/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(264, 2, 'lemari pintu 2', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(265, 2, 'wallbench no 3', 'ITD/lawb3_003/812/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(266, 2, 'meja MP2', 'ITD/ml2_009/812/inv/2015 - ITD/ml2_024/812/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(267, 2, 'meja sink 1', 'ITD/mjs1_001/812/inv/2015 - ITD/mjs1_008/812/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(268, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(269, 2, 'infokus', 'ITD/eif_002/812/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(270, 2, 'screen view', 'ITD/dvs_002/812/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(271, 2, 'wallbench no 3', 'ITD/lawb3_004/813/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(272, 2, 'kursi putar lab donati', 'ITD/kkpd_001/813/inv/2015 - ITD/kkpd_035/813/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(273, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(274, 2, 'infokus', 'ITD/eif_003/813/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(275, 2, 'screen view', 'ITD/dvs_003/813/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(276, 2, 'kursi putar lab donati', 'ITD/kkpd_036/814/inv/2015 - ITD/kkpd_075/814/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(277, 2, 'wallbench no 1 4 laci 2 pintu', 'ITD/lawb1_001/815/inv/2015 - ITD/lawb1_002/815/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(278, 2, 'kursi putar lab donati', 'ITD/kkpl_076/815/inv/2015 - ITD/kkpd_115/815/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(279, 2, 'lemari kaca pintu geser', 'ITD/lakg_006/815/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(280, 2, 'lemari atas 2', 'ITD/laa2_001/815/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(281, 2, 'kursi kelas chitose', 'ITD/kkmc_001/821/inv/2015 - ITD/kkmc_060/821/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(282, 2, 'meja dosen besar kayu', 'ITD/mdb_001/821/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(283, 2, 'papan tulis besar', 'ITD/mdb_001/821/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(284, 2, 'papn tulis roda', 'ITD/ptr_001/821/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(285, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(286, 2, 'kipas hisap', 'ITD/ekh_001/821/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(287, 2, 'proyektor', 'ITD/eif_004/821/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(288, 2, 'screen view', 'ITD/dvs_004/821/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(289, 2, 'kursi kelas chitose', 'ITD/kkmc_061/822/inv/2015 - ITD/kkmc_120/822/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(290, 2, 'meja dosen besar ', 'ITD/mdb_002/822/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(291, 2, 'papan tulis besar', 'ITD/ptb_005/822/inv/2015 - ITD/ptb_006/822/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(292, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(293, 2, 'kipas hisap', 'ITD/ekh_002/822/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(294, 2, 'infokus', 'ITD/eif_005/822/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(295, 2, 'screen view', 'ITD/dvs_005/822/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(296, 2, 'kursi kelas chitose', 'ITD/kkmc_121/823/inv/2015 - ITD/kkmc_180/823/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(297, 2, 'meja dosen besar', 'ITD/mdb_003/823/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(298, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(299, 2, 'kipas hisap ', 'ITD/ekh_003/823/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(300, 2, 'papan tulis', 'ITD/ptb_008/823/inv/2015 - ITD/ptb_009/823/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(301, 2, 'infokus', 'ITD/eif_006/823/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(302, 2, 'screen view', 'ITD/dvs_006/823/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(303, 2, 'papan tulis roda', 'ITD/ptr_002/823/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(304, 2, 'kursi putar lab', 'ITD/kkpl_086/824/inv/2015 - ITD/kkpl_120/824/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(305, 2, 'lampu ', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(306, 2, 'infokus', 'ITD/eif_007/824/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(307, 2, 'screen view', 'ITD/dvs_007/824/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(308, 2, 'meja lab MP1', 'ITD/ml1_001/825/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(309, 2, 'rak meja MP1', 'ITD/mkr1_001/825/inv/2015 - ITD/mkr1_004/825/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(310, 2, 'meja sink 2', 'ITD/mjs2_001/825/inv/2015 - ITD/mjs2_004/825/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(311, 2, 'wallbench no 2', 'ITD/lawb2_001/825/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(312, 2, 'lemari atas 3', 'ITD/laa3_001/825/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(313, 2, 'gantungan gelas fiber', 'ITD/dgf_001/825/inv/2015 - ITD/dgf_008/825/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(314, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(315, 2, 'papan tulis besar', 'ITD/ptb_010/825/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(316, 2, 'papan tulis kecil', 'ITD/ptk_003/825/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(317, 2, 'meja lab MP1', 'ITD/ml1_002/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(318, 2, 'rak meja MP1', 'ITD/mkr1_005/826/inv/2015 - ITD/mkr1_008/826/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(319, 2, 'meja sink 2', 'ITD/mjs2_005/826/inv/2015 - ITD/mjs2_008/826/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(320, 2, 'lemari kaca pintu geser', 'ITD/lakg_007/826/inv/2015 - ITD/lakg_011/826/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(321, 2, 'wallbench no 1', 'ITD/lawb1_003/826/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(322, 2, 'wallbench no 2', 'ITD/lawb2_002/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(323, 2, 'gantungan gelas fiber', 'ITD/dgf_009/826/inv/2015 - ITD/dgf_016/826/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(324, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(325, 2, 'papan tulis besar', 'ITD/ptb_011/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(326, 2, 'papan tulis kecil', 'ITD/ptk_004/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(327, 2, 'lemari atas 2', 'ITD/laa2_002/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(328, 2, 'lemari atas 3', 'ITD/laa3_002/826/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(329, 2, 'meja dosen besar kayu', 'ITD/mdb_004/R.Dosen1/inv/2015 - ITD/mdb_005/R.Dose', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(330, 2, 'meja komputer besar', 'ITD/mkb_001/R.Dosen1/inv/2015 - ITD/mkd_002/R.Dose', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(331, 2, 'kursi roda donati besar', 'ITD/kkrd_001/R.Dosen1/inv/2015 - ITD/kkrd_002/R.Do', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(332, 2, 'kursi roda donati kecil', 'ITD/kkrdk_001/R.Dosen1/inv/2015 - ITD/kkrdk_004/R.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(333, 2, 'laci arsip kecil 3', 'ITD/laak3_001/R.Dosen1/inv/2015 - ITD/laak3_002/R.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(334, 2, 'lemari arsip kaca pintu geser', 'ITD/lakg_012/R. Dosen 1/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(335, 2, 'lemari arsip crezenda', 'ITD/latw_001/R.Dosen/inv/2015 - ITD/latw_002/R.Dos', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(336, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(337, 2, 'dispenser miyako', 'ITD/ed_001/R.Dosen1/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(338, 2, 'meja dosen besar', 'ITD/mdb_006/R. AssFD/inv/2015 - ITD/mdb_010/R. Ass', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(339, 2, 'meja komputer ', 'ITD/mkb_003/R.AssFD/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(340, 2, 'laci arsip kecil 2 laci 5', 'ITD/laak2_001/R.AssFD/inv/2015 - ITD/laak2_005/R.A', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(341, 2, 'kursi roda donati kecil', 'ITD/kkrdk_005/R.AssFD/inv/2015 - ITD/kkrdk_010/R.A', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(342, 2, 'kursi roda donati besar', 'ITD/kkrd_003/R.AssFD/inv/2015 - ITD/kkrd_004/R.Ass', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(343, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(344, 2, 'meja dosen besar kayu', 'ITD/mdb_011/R.AssKD/inv/2015 - ITD/mdb_012/R.AssKD', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(345, 2, 'meja komputer besar', 'ITD/mkb_004/R.AssKD/inv/2015 - ITD/mkd_005/R.AssKD', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(346, 2, 'kursi roda donati besar', 'ITD/kkrd_005/R.AssKD/inv/2015 - ITD/kkrd_006/R.Ass', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(347, 2, 'kursi roda donati kecil', 'ITD/kkrdk_011/R.AssKD/inv/2015 - ITD/kkrdk_012/R.A', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(348, 2, 'laci arsip kecil 2', 'ITD/laak2_006/R.AssKD/inv/2015 - ITD/laak2_009/R.A', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(349, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(350, 2, 'dispenser miyako', 'ITD/ed_002/R.AssKD/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(351, 2, 'meja dosen besar kayu', 'ITD/mdb_013/R.admin1/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(352, 2, 'meja komputer besar', 'ITD/mkb_006/R.Admin1/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(353, 2, 'laci arsip kecil 3', 'ITD/laak3_003/R.Admin1/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(354, 2, 'lemari arsip kaca pintu geser', 'ITD/lakg_013/R. Admin 1/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(355, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(356, 2, 'meja dosen besar kayu', 'ITD/mdb_014/R.Ass3/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(357, 2, 'meja komputer besar', 'ITD/mkb_007/R.Ass3/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(358, 2, 'kursi roda donati besar', 'ITD/kkrd_007/R.Ass3/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(359, 2, 'kursi roda donati kecil', 'ITD/kkrdk_013/R.Ass3/inv/2015 - ITD/kkrdk_014/R.As', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(360, 2, 'lemari arsip kaca pintu geser', 'ITD/lakg_014/R. Ass3/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(361, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(362, 2, 'meja counter 1+2', 'ITD/mjcot_001/R.Admin/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(363, 2, 'meja komputer besar', 'ITD/mkb_008/R.Admin/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(364, 2, 'meja dosen besar kayu', 'ITD/mdb_015/R.Admin/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(365, 2, 'kursi roda donati besar', 'ITD/kkrd_008/R.Admin/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(366, 2, 'laci arsip besar 4', 'ITD/laab4_001/R.Admin/inv/2015 - ITD/laab4_002/R.A', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(367, 2, 'kursi tunggu stainless', 'ITD/dkt_001/R.Admin/inv/2015 - ITD/dkt_003/R.Admin', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(368, 2, 'lemari arsip kaca besi', 'ITD/labk_001/R.Admin/inv/2015 - ITD/labk_002/R.Adm', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(369, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(370, 2, 'meja dosen besar kayu', 'ITD/mdb_016/R.KaP/inv/2015 - ITD/mdb_017/R.KaP/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(371, 2, 'meja komputer besar', 'ITD/mkb_009/R.KaP/inv/2015 - ITD/mkd_010/R.KaP/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(372, 2, 'kursi roda donati besar', 'ITD/kkrd_009/R.KaP/inv/2015 - ITD/kkrd_010/R.KaP/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(373, 2, 'kursi roda donati kecil', 'ITD/kkrdk_015/R.KaP/inv/2015 - ITD/kkrdk_018/R.KaP', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(374, 2, 'laci arsip kecil 3', 'ITD/laak3_004/R.KaP/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(375, 2, 'lemari arsip crezenda', 'ITD/latw_003/R.R.KaP/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(376, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(377, 2, 'dispenser miyako', 'ITD/ed_003/R.KaP/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(378, 2, 'meja dosen besar kayu', 'ITD/mdb_018/R.Dekan/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(379, 2, 'meja komputer besar', 'ITD/mkb_011/R.Dekan/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(380, 2, 'Kursi roda exe', 'ITD/kkrb_001/R.Dekan/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(381, 2, 'sofa high point 4 kaki', 'ITD/stk_001/R.Dekan/inv/2015 - ITD/stk_003/R.Dekan', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(382, 2, 'laci arsip kecil 3', 'ITD/laak3_005/R.Dekan/inv/2015 - ITD/laak3_006/R.D', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(383, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(384, 2, 'meja dosen besar kayu', 'ITD/mdb_019/R.Dosen2/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(385, 2, 'meja komputer besar', 'ITD/mkb_012/R.Dosen2/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(386, 2, 'kursi roda donati besar', 'ITD/kkrd_011/R.Dosen2/inv/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(387, 2, 'kursi roda donati kecil', 'ITD/kkrdk_019/R.Dosen2/inv/2015 - ITD/kkrdk_020/R.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(388, 2, 'lemari arsip pintu geser', 'ITD/lakg_015/R. Dosen2/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(389, 2, 'laci arsip kecil 3', 'ITD/laak3_007/R.Dosen2/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(390, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(391, 2, 'meja dosen besar kayu', 'ITD/mdb_020/R. KaL/inv/2015 - ITD/mdb_021/R.Kal/in', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(392, 2, 'meja komputer besar', 'ITD/mkb_013/R.KaL/inv/2015 - ITD/mkd_014/R.KaL/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(393, 2, 'kursi roda donati besar', 'ITD/kkrd_012/R.KaL/inv/2015 - ITD/kkrd_013/R.KaL/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(394, 2, 'kursi roda donati kecil', 'ITD/kkrdk_021/R.KaL/inv/2015 - ITD/kkrdk_024/R.KaL', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(395, 2, 'laci arsip kecil 3', 'ITD/laak3_008/R.KaL/inv/2015 - ITD/laak3_009/R.KaL', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(396, 2, 'laci arsip besar 4', 'ITD/laab4_003/R.Admin/inv/2015 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(397, 2, 'lemari arsip kaca pintu geser', 'ITD/lakg_016/R. Ka.Lab/inv/2015 - ITD/lakg_018/R. ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(398, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(399, 2, 'meja dosen besar kayu', 'ITD/mdb_022/R.Diskusi/inv/2015 - ITD/mdb_023/R.Dis', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(400, 2, 'kursi kelas chitose', 'ITD/kkmc_181/R. Diskusi/inv/2015 - ITD/kkmc_200/R.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(401, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(402, 2, 'dispenser', 'ITD/ed_004/pantry/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(403, 2, 'kompor gas', 'ITD/dkg_001/pantry/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(404, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(405, 2, 'meja dosen besar kayu', 'ITD/mdb_024/R. rapat/inv/2015 - ITD/mdb_028/R.Rapa', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(406, 2, 'meja samping', 'ITD/mds_001/R.Rapat/inv/2015 - ITD/mds_004/R.Rapat', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(407, 2, 'kursi dosen biasa donati', 'ITD/kkbb_001/R.Rapat/inv/2015 - ITD/kkbb_016/R.rap', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(408, 2, 'infokus', 'ITD/eif_008/R. rapat/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(409, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(410, 2, 'screen view', 'ITD/dvs_008/R. rapat/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(411, 2, 'kursi biasa tumpuk donati', 'ITD/kkb_001/R.rapat/inv/2015 - ITD/kkb_008/R.rapat', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL);
INSERT INTO `tbl_r_barang` (`barang_id`, `kategori`, `nama`, `no_register`, `jenis`, `Merek`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(412, 2, 'lemari arsip kaca pintu geser', 'ITD/lakg_019/R. rapat/inv/2015 - ITD/lakg_020/R. R', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(413, 2, 'kipas hisap', 'ITD/ekh_004/R.Rapat/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(414, 2, 'meja kerja kecil kayu yacab', 'Yacab/mdk_001/rektor/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(415, 2, 'meja kerja besar kayu yacab', 'Yacab/mdb_001/rektor/yacab/2001 - Yacab/mdb_002/re', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(416, 2, 'kursi roda yadel', 'Yacab/kkrk_001/rektor/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(417, 2, 'kursi roda archigamma', 'ITD/kkrk_001/rektor/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(418, 2, 'kursi tamu yadel', 'Yacab/kkbb_001/rektor/yacab/2001 - Yacab/kkbb_002/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(419, 2, 'sofa tamu yadel', 'Yacab/stk_001/rektor/yacab/2001 - yacab/stk_005/re', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(420, 2, 'dispenser national', 'ITD/ed_001/rektor/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(421, 2, 'lemari arsip yadel', 'yacab/latw_001/rektor/yacab/2001', '0', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(422, 2, 'lemari arsip yadel', 'Yacab/labw2_001/rektor/yacab/2003-yacab/labw2_009/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(423, 2, 'lemari crezenda yacab', 'yacab/latw_001/rektor/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(424, 2, 'meja tamu yadel', 'yacab/mtkk_001/rektor/yacab/2001 - yacab/mtkk_003/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(425, 2, 'jam dinding', 'ITD/djd_001/rektor/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(426, 2, 'lukisan', 'ITD/dl_001/rektor/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(427, 2, 'PC', 'ITD/ePC_001/rektor/inv/2014 - ITD/ePC_002/rektor/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(428, 2, 'telepon', 'ITD/et_001/rektor/inv/2001 - ITD/et_002/rektor/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(429, 2, 'meja kerja besar Yadel', 'Yacab/mdb_003/inv/yacab/2001 -  Yacab/mdb_004/inv/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(430, 2, 'kursi roda archigamma', 'ITD/kkrk_002/inv/inv/2001 - ITD/kkrk_003/inv/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(431, 2, 'kursi biasa archigamma', 'ITD/kkb_001/inv/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(432, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_001/inv/inv/2001 - ITD/lakw2_002/inv/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(433, 2, 'lemari besar 3 laci', 'ITD/laab3_001/inv/inv/2013 - ITD/laab3_002/inv/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(434, 2, 'lemari kecil 2 laci', 'ITD/laak2_001/inv/inv/2001 - ITD/laak2_002/inv/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(435, 2, 'lemari kecil 3 laci', 'ITD/laak3_001/inv/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(436, 2, 'printer laserjet HP 078', 'ITD/ep_001/inv/duktek/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(437, 2, 'meja segitiga', 'ITD/mks_001/inv/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(438, 2, 'PC', 'ITD/ePC_003/inv/inv/2014 - ITD/ePC_004/inv/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(439, 2, 'telepon', 'ITD/et_003/inv/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(440, 2, 'jam dinding', 'ITD/djd_002/inv/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(441, 2, 'lampu neon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(442, 2, 'meja kerja kecil', 'ITD/mdk_001/HRD/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(443, 2, 'meja kaca', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(444, 2, 'kursi roda high point', 'ITD/kkrhp_001/hrd/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(445, 2, 'kursi roda archigamma', 'ITD/kkrk_004/hrd/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(446, 2, 'PC', 'ITD/ePC_005/HRD/inv/2014 - ITD/ePC_006/HRD/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(447, 2, 'lemari besar 3 laci', 'ITD/laab3_003/HRD/inv/2013 - ITD/laab3_004/HRD/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(448, 2, 'lemari besar 2 laci', 'ITD/laab2_001/HRD/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(449, 2, 'lemari kecil 2 laci', 'ITD/laak2_003/HRD/inv/2001 - ITD/laak2_004/HRD/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(450, 2, 'tong sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(451, 2, 'lampu neon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(452, 2, 'kursi biasa arhigamma', 'ITD/kkb_002/HRD/inv/2001 - ITD/kkb_003/HRD/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(453, 2, 'meja kerja kecil', 'Yacab/mdk_002/LPPM/yacab/2001 - Yacab/mdk_003/LPPM', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(454, 2, 'PC', 'ITD/ePC_007/LPPM/inv/2014 - ITD/ePC_008/LPPM/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(455, 2, 'kursi beroda high point', 'ITD/kkrhp_002/LPPM/inv/2013 -  ITD/kkrhp_003/LPPM/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(456, 2, 'lemari besar 3 laci', 'ITD/laab3_005/LPPM/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(457, 2, 'kursi biasa archigamma', 'ITD/kkb_004/LPPM/inv/2001 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(458, 2, 'lemari kecil berlaci 2', 'ITD/laak2_005/LPPM/inv/2001 - ITD/laak2_006/LPPM/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(459, 2, 'TV', 'ITD/eTV_001/LPPM/duktek/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(460, 2, 'lampu jari ', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(461, 2, 'meja kerja besar yadel', 'Yacab/mdb_005/WR3/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(462, 2, 'meja kerja kecil yadel', 'Yacab/mdk_004/WR3/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(463, 2, 'lemari kaca besar yadel', 'yacab/labk2_001/WR3/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(464, 2, 'meja komputer putih', 'ITD/mkp_001/WR3/yacab/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(465, 2, 'kursi roda yadel', 'Yacab/kkrk_002/rektor/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(466, 2, 'kursi biasa yadel', 'yacab/latw_002/WR3/yacab/2001', '0', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(467, 2, 'kursi biasa yadel', 'Yacab/kkbb_003/WR3/yacab/2001 - Yacab/kkbb_004/WR3', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(468, 2, 'PC', 'ITD/ePC_009/WR3/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(469, 2, 'lemari kecil berlaci 2', 'ITD/laak2_007/WR3/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(470, 2, 'lemari kayu yadel', 'yacab/latw_002/WR3/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(471, 2, 'lampu neon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(472, 2, 'lemari besar berlaci 3', 'ITD/laab3_006/WR3/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(473, 2, 'meja kerja besar yadel', 'Yacab/mdb_006/WR2/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(474, 2, 'meja kerja kecil', 'ITD/mdk_002/WR2/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(475, 2, 'kursi roda achigamma', 'ITD/kkrk_005/WR2/inv/2001 - ITD/kkrk_006/WR2/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(476, 2, 'kursi kerja archigamma', 'ITD/kkrb_001/WR2/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(477, 2, 'lemari arsip kaca berpintu 2', 'ITD/labk_001/WR2/inv/2001 - ITD/labk_002/WR2/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(478, 2, 'lemari besar 3 laci', 'ITD/laab3_007/WR2/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(479, 2, 'lemari kayu yadel', 'yacab/latw_003/WR2/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(480, 2, 'PC', 'ITD/ePC_010/WR2/inv/2014 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(481, 2, 'telepon', 'ITD/et_004/WR2/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(482, 2, 'lampu neon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(483, 2, 'printer officejet Hp', 'ITD/ep_001/inv/duktek/2015', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(484, 2, 'meja tamu Yadel', 'yacab/mtkb_001/keu/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(485, 2, 'lemari arsip kayu', 'ITD/lat_001/keu/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(486, 2, 'meja kerja berpintu 1', 'ITD/mdw1_001/keu/inv/2001 - ITD/mdw1_002/keu/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(487, 2, 'kursi beroda high point', 'ITD/kkrhp_004/keu/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(488, 2, 'kursi beroda archigamma', 'ITD/kkrk_007/keu/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(489, 2, 'kursi biasa archigamma', 'ITD/kkb_005/keu/inv/2001 - ITD/kkb_006/keu/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(490, 2, 'lampu neon 36 watt', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(491, 2, 'lemari berlaci 3 besar', 'ITD/laab3_008/keu/inv/2013 - ITD/laab3_009/keu/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(492, 2, 'kursi bulat', 'ITD/dkb_001/keu/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(493, 2, 'PC', 'ITD/ePC_011/keu/inv/2014 - ITD/ePC_012/keu/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(494, 2, 'alat penghitung uang trissor', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(495, 2, 'lemari berlaci 2 kecil', 'ITD/laak2_006/keu/inv/2001 - ITD/laak2_007/keu/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(496, 2, 'meja tamu', 'yacab/mtkk_004/selasar/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(497, 2, 'sofa tamu single', 'Yacab/stk_006/selasar/yacab/2001 - yacab/stk_010/s', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(498, 2, 'meja rapat besar', 'yacab/mdb_001/selasar/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(499, 2, 'kursi rapat yadel', 'yacab/kkbb_001/selasar/yacab/2001 - yacab/kkbb_008', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(500, 2, 'papan tulis roda', 'yacab/ptbr_001/selasar/yacab/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(501, 2, 'lampu jari', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(502, 2, 'lemari arsip 3 tingkat berpintu 2', 'ITD/labw2_001/CMR/inv', '0', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(503, 2, 'lemari arsip kaca', 'ITD/lakg_001/CMR/inv', '0', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(504, 2, 'sofa (2+1+1)', 'ITD/stb_001/CMR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(505, 2, 'lemari arsip 3 tingkat berpintu 2', 'ITD/labw2_001/CMR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(506, 2, 'lemari arsip kaca', 'ITD/lakg_001/CMR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(507, 2, 'meja tamu', 'ITD/mtwb_001/CMR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(508, 2, 'meja makan cantika', 'ITD/mmk_001/CMR/inv/2013 - ITD/mmk_002/CMR/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(509, 2, 'kursi makan cantika', 'ITD/kmw_001/CMR/inv/2013 - ITD/kmw_008/CMR/inv/201', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(510, 2, 'meja sudut bundar', 'ITD/mdr_001/CMR/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(511, 2, 'lemari arsip kecil 2 laci', 'ITD/laak2_001/CMR/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(512, 2, 'dispenser', 'ITD/ed_001/CMR/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(513, 2, 'jam dinding', 'ITD/djd_001/CMR/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(514, 2, 'tirai plastik 2 nako', 'ITD/dtrai2_001/CMR/inv/2007 - ITD/dtrai2_004/CMR/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(515, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(516, 2, 'meja komputer biasa berpintu 1', 'ITD/mdw1_001/Admin/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(517, 2, 'meja kayu berpintu 1', 'ITD/mdw1_002/Admin/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(518, 2, 'meja komputer kaca', 'ITD/mkk_001/Admin/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(519, 2, 'kursi roda archigamma', 'ITD/kkrk_001/Admin/inv/2001 - ITD/kkrk_004/Admin/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(520, 2, 'kursi roda merah high point', 'ITD/kkrhp_001/Admin/inv/2013 - ITD/kkrhp_002/Admin', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(521, 2, 'lemari besar berlaci 3', 'ITD/laab3_001/Admin/inv/2013 -  ITD/laab3_003/Admi', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(522, 2, 'lemari kecil berlaci 2', 'ITD/laak2_002/Admin/inv/2001 - ITD/laak2_006/Admin', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(523, 2, 'rak buku besi', 'ITD/lar_001/Admin/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(524, 2, 'printer hp', 'ITD/ep_001/Admin/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(525, 2, 'mesin fax', 'ITD/efax_001/Admin/inv/2012', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(526, 2, 'PC', 'ITD/ePC_001/Admin/duktek/2013 - ITD/ePC_004/Admin/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(527, 2, 'lemari kecil berpintu 2', 'ITD/lakw2_001/Admin/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(528, 2, 'tirai bambu', 'ITD/dtrai_001/CMR/inv/2007 - ITD/dtrai_002/CMR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(529, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(530, 2, 'jam dinding', 'ITD/djd_001/Admin/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(531, 2, 'tong sampah biasa', 'ITD/tsk_001/Admin/inv/2014 - ITD/tsk_02/Admin/inv/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(532, 2, 'alat pemotong kertas', 'ITD/epk_001/Admin/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(533, 2, 'meja kayu berpintu 2', 'ITD/mdw2_001/Admin/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(534, 2, 'telepon', 'ITD/et_001/Admin/inv/2003 - ITD/et_002/Admin/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(535, 2, 'meja kayu biasa', 'ITD/mdb_001/MR/inv/2003 - ITD/mdb_004/MR/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(536, 2, 'kursi roda archigamma', 'ITD/kkrk_005/MR/inv/2001 - ITD/kkrk_012/MR/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(537, 2, 'kursi plastik/bakso', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(538, 2, 'meja sudut besar 2 pintu', 'ITD/mdw2_002/MR/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(539, 2, 'tirai plastik biru 2 nako', 'ITD/dtrai2_005/MR/inv/2013 - ITD/dtrai2_007/MR/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(540, 2, 'tirai plastik biru 5 nako', 'ITD/dtrai5_001/MR/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(541, 2, 'papan tulis besar berbingkai', 'ITD/ptb_001/MR/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(542, 2, 'tong sampah', 'ITD/tsk_001/MR/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(543, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(544, 2, 'jam dinding', 'ITD/djd_003/MR/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(545, 2, 'meja komputer kecil', 'ITD/mdk_001/BAAK/inv/2003 - ITD/mdk_005/BAAK/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(546, 2, 'lemari laci 3 pintu besar', 'ITD/laab3_004/BAAK/inv/2013 -  ITD/laab3_006/BAAK/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(547, 2, 'lemari laci 2 pintu besar', 'ITD/laab2_001/BAAK/inv/2001 - ITD/laab2_003/BAAK/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(548, 2, 'lemari laci 2 pintu kecil', 'ITD/laak2_007/BAAK/inv/2001 - ITD/laak2_009/BAAK/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(549, 2, 'kursi roda high point', 'ITD/kkrhp_003/BAAK/inv/2013 - ITD/kkrhp_008/BAAK/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(550, 2, 'kursi bulat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(551, 2, 'PC', 'ITD/ePC_005/BAAK/duktek/2013 - ITD/ePC_008/BAAK/du', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(552, 2, 'tirai', 'ITD/dtrai_003/BAAK/inv/2007 - ITD/dtrai_005/BAAK/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(553, 2, 'loker tempel dinding', 'Kantor ITD-Kantor Staff-R.BAAK', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(554, 2, 'loker tempel dinding', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(555, 2, 'keranjang sampah biasa', 'ITD/tsk_001/BAAK/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(556, 2, 'keranjang sampah tutup', 'ITD/tsk_002/BAAK/inv/2013 - ITD/tsk_003/BAAK/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(557, 2, 'printer epson L120', 'ITD/ep_002/BAAK/inv/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(558, 2, 'meja kabinet', 'ITD/mdt_001/WKB/inv/2001 - ITD/mdt_002/WKB/inv/200', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(559, 2, 'kursi roda high point', 'ITD/kkrhp_009/WKB/inv/2013 - ITD/kkrhp_011/WKB/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(560, 2, 'PC', 'ITD/ePC_009/WKB/duktek/2013 - ITD/ePC_011/WKB/dukt', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(561, 2, 'kursi roda archigamma', 'ITD/kkrk_013/WKB/inv/2001 - ITD/kkrk_014/WKB/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(562, 2, 'lemari besar laci 3', 'ITD/laab3_007/WKB/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(563, 2, 'lemari besar laci 2', 'ITD/laab2_004/WKB/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(564, 2, 'keranjang sampah biasa', 'ITD/tsk_002/BAAK/inv/2013 - ITD/tsk_003/BAAK/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(565, 2, 'tirai', 'ITD/dtrai_006/WKB/inv/2007 - ITD/dtrai_008/WKB/inv', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(566, 2, 'meja komputer besi beroda', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(567, 2, 'lemari kecil berpintu 2', 'ITD/lakw2_002/WKB/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(568, 2, 'rak buku besi', 'ITD/lar_002/WKB/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(569, 2, 'kursi archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(570, 2, 'meja kerja besar', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(571, 2, 'meja kerja berlaci', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(572, 2, 'kursi roda high point', 'ITD/kkrhp_012/Ka.Kor/inv/2013 - ITD/kkrhp_016/Ka.K', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(573, 2, 'kursi roda archigamma', '', '0', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(574, 2, 'kursi roda archigamma', 'ITD/kkrk_015/Ka.Kor/inv/2001 - ITD/kkrk_016/Ka.Kor', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(575, 2, 'kursi archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(576, 2, 'lemari besar laci 3', 'ITD/laab3_008/Ka.Kor/inv/2013 -  ITD/laab3_010/Ka.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(577, 2, 'lemari besar laci 2', 'ITD/laab2_005/Ka.Kor/inv/2001 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(578, 2, 'lemari kecil berpintu 2', 'ITD/lakw2_003/Ka.Kor/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(579, 2, 'PC', 'ITD/ePC_012/Ka.Kor/duktek/2013 - ITD/ePC_014/KA.ko', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(580, 2, 'telepon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(581, 2, 'rak besi', 'ITD/lar_003/Ka.kor/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(582, 2, 'lemari arsip besar berpintu 2', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(583, 2, 'keranjang sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(584, 2, 'lemari kecil berlaci 2', 'ITD/laak2_010/Ka.Kor/inv/2001 -  ITD/laak2_011/Ka.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(585, 2, 'lemari kecil berlaci 3', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(586, 2, 'meja sekat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(587, 2, 'kursi roda high point', 'ITD/kkrhp_017/prodi/inv/2013 - ITD/kkrhp_021/prodi', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(588, 2, 'kursi roda archigamma', 'ITD/kkrk_017/Prodi/inv/2001 - ITD/kkrk_021/Prodi/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(589, 2, 'kursi archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(590, 2, 'lemari besar laci 3', 'ITD/laab3_011/Prodi/inv/2013 -  ITD/laab3_012/Prod', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(591, 2, 'lemari besar laci 2', 'ITD/laab2_006/Prodi/inv/2001 - ITD/laab2_010/Prodi', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(592, 2, 'lemari kecil berpintu 2', 'ITD/lakw2_004/prodi/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(593, 2, 'lemari kecil laci 2', 'ITD/laak2_012/Prodi/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(594, 2, 'PC', 'ITD/ePC_015/Prodi/duktek/2013 - ITD/ePC_018/Prodi/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(595, 2, 'keranjang sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(596, 2, 'meja bundar', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(597, 2, 'lemari besar 2 laci', 'ITD/laab2_011/selasar/inv/2001 - ITD/laab2_013/sel', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(598, 2, 'lemari arsip kaca pendek', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(599, 2, 'lemari arsip besar berpintu 2', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(600, 2, 'kursi roda archigamma', 'ITD/kkrk_021/Selasar/inv/2001 - ITD/kkrk_022/Selas', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(601, 2, 'kursi archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(602, 2, 'lemari kecil 2 laci', 'ITD/laak2_013/selasar/inv/2001 - ITD/laak2_014/sel', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(603, 2, 'lemari kecil 3 laci', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(604, 2, 'lemari es', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(605, 2, 'dispenser', 'ITD/ed_002/selasar/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(606, 2, 'mesin fotocopy', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(607, 2, 'printer 05', 'ITD/ep_003/selasar/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(608, 2, 'printer brother', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(609, 2, 'telepon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(610, 2, 'jam dinding', 'ITD/djd_004/Selasar/inv/2016', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(611, 2, 'lemari kecil 2 pintu', 'ITD/lakw2_005/selasar/inv/2003 - ITD/lakw2_008/sel', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(612, 2, 'meja bundar + sekat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(613, 2, 'kursi beroda archigamma', 'ITD/kkrk_023/R.Atas/inv/2001 - ITD/kkrk_028/R.Atas', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(614, 2, 'kursi beroda high point', 'ITD/kkrhp_022/R.Atas/inv/2013 - ITD/kkrhp_025/R.At', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(615, 2, 'lemari arsip besar 3 laci', 'ITD/laab3_013/R.Atas/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(616, 2, 'lemari arsip besar 2 laci', 'ITD/laab2_014/R.Atas/inv/2001 - ITD/laab2_022/R.At', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(617, 2, 'lemari arsip kecil 3 laci', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(618, 2, 'lemari arsip kecil 2 laci', 'ITD/laak2_015/R. Atas/inv/2001', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(619, 2, 'PC', 'ITD/ePC_019/R.Atas/duktek/2013 - ITD/ePC_025/R.Ata', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(620, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(621, 2, 'telepon', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(622, 2, 'kursi archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(623, 2, 'meja jepara', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(624, 2, 'kursi kayu jepara', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(625, 2, 'kursi bulat plastik', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(626, 2, 'finger print', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(627, 2, 'lampu hias', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(628, 2, 'lampu bulat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(629, 2, 'meja bundar + sekat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(630, 2, 'kursi roda archigamma', 'ITD/kkrk_030/K.Dosen/inv/2001 - ITD/kkrk_038/K.Dos', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(631, 2, 'kursi roda high point', 'ITD/kkrhp_026/Dosen/inv/2013 - ITD/kkrhp_027/Dosen', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(632, 2, 'lemari arsip besar 2 laci', 'ITD/laab2_025/dosen/inv/2001 - ITD/laab2_035/Dosen', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(633, 2, 'lemari arsip kecil 3 laci', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(634, 2, 'lemari arsip kecil 2 laci', 'ITD/laak2_019/K. Dosen/inv/2001 - ITD/laak2_028/K.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(635, 2, 'lemari kecil berpintu 2', 'ITD/lakw2_009/duktek/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(636, 2, 'PC', 'ITD/ePC_026/Dosen/duktek/2013 - ITD/ePC_036/dosen/', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(637, 2, 'AC', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(638, 2, 'dispenser', 'ITD/ed_003/K.Dosen/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(639, 2, 'meja dispenser', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(640, 2, 'printer hp 05', 'ITD/ep_005/Dosen/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(641, 2, 'meja kaca', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(642, 2, 'UPS', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(643, 2, 'tirai plastik 2 nako', 'ITD/dtrai2_008/K.Dosen/inv/2013 - ITD/dtrai2_012/K', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(644, 2, 'tirai plastik 1 nako', 'ITD/dtrai1_001/K.Dosen/inv/2013 - ITD/dtrai1_002/K', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(645, 2, 'jam dinding', 'ITD/djd_006/K.Dosen/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(646, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(647, 2, 'tong sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(648, 2, 'papan tulis bingkai besar', 'ITD/ptb_002/MR/inv/2003', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(649, 2, 'papan tulis kecil', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(650, 2, 'meja segitiga', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(651, 2, 'meja kayu biasa', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(652, 2, 'meja komputer putih', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(653, 2, 'meja biasa berpintu 1', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(654, 2, 'kursi biasa archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(655, 2, 'kursi beroda archigamma', 'ITD/kkrk_039/Duktek/inv/2001 - ITD/kkrk_044/Duktek', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(656, 2, 'kursi beroda high point', 'ITD/kkrhp_028/Duktek/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(657, 2, 'lemari arsip 3 tingkat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(658, 2, 'lemari arsip 5 tingkat', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(659, 2, 'papan tulis kecil tidak berbingkai', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(660, 2, 'PC', 'ITD/ePC_037/duktek/duktek/2013 - ITD/ePC_046/dukte', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(661, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(662, 2, 'dispenser', 'ITD/ed_004/Duktek/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(663, 2, 'meja dispenser', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(664, 2, 'lemari arsip besar 3 laci', 'ITD/laab3_014/Duktek/inv/2013 -  ITD/laab3_016/Duk', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(665, 2, 'lemari arsip kecil 3 laci', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(666, 2, 'jam dinding', 'ITD/ejd_007/Duktek/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(667, 2, 'lemari berpintu 2', 'ITD/lakw2_010/duktek/inv/2003 - ITD/lakw2_011/dukt', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(668, 2, 'gorden biru 2 nako', 'ITD/dtrai2_001/Duktek/inv/2014 - ITD/dtrai2_003/Du', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(669, 2, 'tong sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(670, 2, 'meja kayu biasa', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(671, 2, 'meja komputer putih', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(672, 2, 'kursi beroda archigamma', 'ITD/kkrk_045/SDI/inv/2001 - ITD/kkrk_054/SDI/inv/2', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(673, 2, 'kursi biasa archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(674, 2, 'papan tulis kecil tidak berbingkai', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(675, 2, 'dispenser', 'ITD/ed_005/SDI/inv/2013 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(676, 2, 'lemari arsip besar 3 laci', 'ITD/laab3_017/SDI/inv/2013 -  ITD/laab3_018/SDI/in', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(677, 2, 'tong sampah biasa', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(678, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(679, 2, 'papan tulis besar tidak berbingkai', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(680, 2, 'gorden 2 nako biru', 'ITD/dtrai2_004/SDI/inv/2014 - ITD/dtrai2_006/SDI/i', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(681, 2, 'jam dinding', 'ITD/djd_008/SDI/inv/2013', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(682, 2, 'meja bulat putih bongkar', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(683, 2, 'kursi hitam ichiko', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(684, 2, 'PC Apple', 'ITD/ePC_047/R.Dalam/duktek/2014', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(685, 2, 'lemari 2 pintu kaca', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(686, 2, 'lampu 18 watt', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(687, 2, 'lampu jari', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(688, 2, 'keranjang sampah', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(689, 2, 'meja kacar bundar', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(690, 2, 'kursi kayu putih', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(691, 2, 'sofa merah panjang', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(692, 2, 'karpet corak', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(693, 2, 'rak lemari kaca besi', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(694, 2, 'lemari kaca kayu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(695, 2, 'TV', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(696, 2, 'lampu', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(697, 2, 'meja kaca bundar kecil', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(698, 2, 'meja panjang putih', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(699, 2, 'kursi roda ichiko', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(700, 2, 'proyektor', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(701, 2, 'papan tulis kaca', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(702, 2, 'view screen', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(703, 2, 'lampu gantung', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(704, 2, 'rak kecil putih berpintu 2', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(705, 2, 'meja komputer kecil', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(706, 2, 'meja komputer kaca', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(707, 2, 'meja segitiga', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(708, 2, 'kursi roda high point', 'ITD/kkrhp_029/R.Asrama/inv/2013 - ITD/kkrhp_030/R.', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(709, 2, 'kursi roda archigamma', 'ITD/kkrk_055/R.asrama/inv/2001 ', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(710, 2, 'kursi biasa archigamma', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(711, 2, 'lemari besar laci 3', 'ITD/laab3_019/R.Asrama/inv/2013 -  ITD/laab3_020/R', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(712, 2, 'lemari kecil laci 3', '', '0', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(713, 2, 'lemari kecil laci 2', 'ITD/laak2_029/R. Asrama/inv/2001', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(714, 2, 'keranjang sampah', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(715, 2, 'lampu jari', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(716, 2, 'jam dinding', 'ITD/djd_009/R.Asrama/inv/2013', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(717, 2, 'telepon', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(718, 2, 'lemari laci 3 panjang abu-abu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(719, 2, 'papan nama kaca', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(720, 2, 'photo', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(721, 2, 'lampu TL 18', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(722, 2, 'lampu TL 18', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(723, 2, 'lampu bulat', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(724, 2, 'dispenser', 'ITD/ed_006/Selasar/inv/2013 ', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(725, 2, 'meja dispenser', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(726, 2, 'meja kayu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(727, 2, 'kursi jepara', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(728, 2, 'lampu jari', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(729, 2, 'lampu bulat', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(730, 2, 'lemari hias', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(731, 2, 'tong sampah besar', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(732, 2, 'photo', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(733, 2, 'lampu hias', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(734, 2, 'jam dinding', 'ITD/djd_010/selasar/inv/2013', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(735, 2, 'meja jepara pendek', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(736, 2, 'kursi jepara pendek', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(737, 2, 'meja billyard', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(738, 2, 'miniatur bangunan', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(739, 2, 'tong sampah bertutup besar', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(740, 2, 'meja kaca', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(741, 2, 'meja hitam', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(742, 2, 'kursi biasa archigamma', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(743, 2, 'TV 42\'', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(744, 2, 'TV 21\'', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(745, 2, 'lampu jari', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(746, 2, 'lampu TL', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(747, 2, 'bangku panjang kayu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(748, 2, 'tempat sepatu kayu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(749, 2, 'kursi plastik ', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(750, 2, 'meja kerja kecil kayu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(751, 2, 'meja kerja besar kayu', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(752, 2, 'kursi roda yadel', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(753, 2, 'kursi roda archigamma', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(754, 2, 'kursi tamu yadel', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(755, 2, 'sofa tamu yadel', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(756, 2, 'dispenser national', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(757, 2, 'lemari arsip yadel', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(758, 2, 'lemari kayu yadel berpintu 2 berlaci 4', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(759, 2, 'meja tamu yadel', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(760, 2, 'jam dinding', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(761, 2, 'lukisan', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(762, 2, 'PC', '', '', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(763, 1, 'alkohol 96%', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(764, 1, 'amplop coklat besar airmail/29x39', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(765, 1, 'amplop coklat kecil 18.5x28.5', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(766, 1, 'amplop coklat sedang 24x34.5', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(767, 1, 'amplop putih besar 128x28/110', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(768, 1, 'amplop putih kecil 110x230', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(769, 1, 'anak cutter kenko', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(770, 1, 'bambi besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(771, 1, 'bambi kecil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(772, 1, 'baterai AA', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(773, 1, 'baterai AAA', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(774, 1, 'baterai petak', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(775, 1, 'baterai pointer', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(776, 1, 'binder', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(777, 1, 'binder clip no 155', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(778, 1, 'binder clip no 200', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(779, 1, 'binder clip no 260', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(780, 1, 'blok print KXFA43', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(781, 1, 'bolpoint ball liner biru', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(782, 1, 'bolpoint biru', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(783, 1, 'bolpoint hitam', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(784, 1, 'bolpoint merah', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(785, 1, 'buku besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(786, 1, 'buku ekspedisi', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(787, 1, 'buku telepon', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(788, 1, 'buku tulis', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(789, 1, 'businnes file', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(790, 1, 'catridge LC 400 BK', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(791, 1, 'catridge LC 400 C', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(792, 1, 'catridge LC 400 M', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(793, 1, 'catridge LC 400 Y', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(794, 1, 'clear holder 80', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(795, 1, 'clear holder 40', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(796, 1, 'clip file/spring file', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(797, 1, 'clip trigonal', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(798, 1, 'cutter kenko L500', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(799, 1, 'double folio', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(800, 1, 'double tip sedang', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(801, 1, 'DVD', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(802, 1, 'etona 03', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(803, 1, 'etona HD 10', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(804, 1, 'etona HD 23/15', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(805, 1, 'etona HD 23/10', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(806, 1, 'etona HD 24', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(807, 1, 'file box', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(808, 1, 'gunting kenko sc 848N', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(809, 1, 'isolasi bening besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(810, 1, 'isolasi bening kecil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(811, 1, 'isolasi bening sedang', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(812, 1, 'isolasi coklat besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(813, 1, 'isolasi hitam besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(814, 1, 'isolasi hitam kecil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(815, 1, 'isolasi hitam sedang', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(816, 1, 'kertas A4 100 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(817, 1, 'kertas A4 70 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(818, 1, 'kertas A4 80 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(819, 1, 'kertas BC putih', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(820, 1, 'kertas block print', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(821, 1, 'kertas concorde kream 90 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(822, 1, 'kertas F4 70 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(823, 1, 'kertas F4 warna', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(824, 1, 'kertas laminating', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(825, 1, 'kertas linen putih', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(826, 1, 'kwitansi besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(827, 1, 'kwitansi kecil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(828, 1, 'laserjet 05 A', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(829, 1, 'laserjet 131A Hitam', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(830, 1, 'laserjet 131A cyan', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(831, 1, 'laserjet 131A Yellow', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(832, 1, 'laserjet 131A Blue', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(833, 1, 'laserjet 42A', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(834, 1, 'laserjet 78A', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(835, 1, 'laserjet 80A', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(836, 1, 'label', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(837, 1, 'lem povinol 25 ml', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(838, 1, 'lem povinol 75 ml', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(839, 1, 'lem povinol refill', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(840, 1, 'lem setan', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(841, 1, 'lem stik 25 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(842, 1, 'lem stik 8 gr', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(843, 1, 'map 5001', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(844, 1, 'map kertas', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(845, 1, 'map plastik', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(846, 1, 'marker permanent biru', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(847, 1, 'marker permanent hitam', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(848, 1, 'marker permanent merah', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(849, 1, 'marker wb biru', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(850, 1, 'marker wb hitam', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(851, 1, 'marker wb merah', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(852, 1, 'pengahapus wb', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(853, 1, 'penggaris', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(854, 1, 'penghapus pensil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(855, 1, 'pensil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL);
INSERT INTO `tbl_r_barang` (`barang_id`, `kategori`, `nama`, `no_register`, `jenis`, `Merek`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(856, 1, 'post it besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(857, 1, 'post it kecil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(858, 1, 'post it sedang', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(859, 1, 'punch/pembolong kertas', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(860, 1, 'push pin', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(861, 1, 'rautan pensil', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(862, 1, 'refill snowman', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(863, 1, 'stabilo', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(864, 1, 'stamp ink yamura', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(865, 1, 'stapler HD 10 ', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(866, 1, 'stapler HD 24', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(867, 1, 'stapler HD 50', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(868, 1, 'stil grib', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(869, 1, 'super pp folder', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(870, 1, 'tape dispenser besar', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(871, 1, 'tape dispensser kecil no 50', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(872, 1, 'tempat CD', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(873, 1, 'tinta brother Black', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(874, 1, 'tinta brother cyan', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(875, 1, 'tinta brother magenta', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(876, 1, 'tinta brother yellow', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(877, 1, 'tinta epson BK T6641/3117', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(878, 1, 'tinta offjet 920 black', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(879, 1, 'tinta offjet 920 colour', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(880, 1, 'tinta foto copy', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(881, 1, 'tip-ex', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(882, 1, 'VCD', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(883, 1, 'view file', '', '1', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(884, 1, 'bayclean', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(885, 1, 'baygon semprot', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(886, 1, 'brus kamar mandi biasa', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(887, 1, 'brus kamar mandi tangkai', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(888, 1, 'brush WC tangkai', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(889, 1, 'bubuk the bendera', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(890, 1, 'cat vinilex 25 kg', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(891, 1, 'cif cream besar', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(892, 1, 'cif cream refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(893, 1, 'cling refill 450 ml', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(894, 1, 'cling semprot', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(895, 1, 'cream ekonomi', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(896, 1, 'detergent cair 1000 ml refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(897, 1, 'dettol handsanitizer', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(898, 1, 'dispenser', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(899, 1, 'ember', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(900, 1, 'gayung', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(901, 1, 'glade matic', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(902, 1, 'glade matic refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(903, 1, 'glade pewangi batang', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(904, 1, 'glade pewangi batang', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(905, 1, 'glade semprot', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(906, 1, 'gula sachet', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(907, 1, 'handsoap ', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(908, 1, 'handsoap refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(909, 1, 'kalo 1712', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(910, 1, 'kanebo', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(911, 1, 'kapur barus bulat', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(912, 1, 'keranjang sampah', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(913, 1, 'lampu baret', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(914, 1, 'lampu jari 11 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(915, 1, 'lampu jari 14 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(916, 1, 'lampu jari 18 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(917, 1, 'lampu jari 42 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(918, 1, 'lampu jari 45 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(919, 1, 'lampu neon 18 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(920, 1, 'lampu neon 36 watt', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(921, 1, 'lampu baret', '', '2', NULL, '-', '1', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(922, 1, 'lampu baret', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(923, 1, 'nescafe sachet', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(924, 1, 'odol gigi 25 gr', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(925, 1, 'plastik sampah', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(926, 1, 'papan setrikaan', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(927, 1, 'pel lantai', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(928, 1, 'pengharum pakaian refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(929, 1, 'pladge refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(930, 1, 'pladge semprot', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(931, 1, 'porstek ', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(932, 1, 'sabun mandi cair', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(933, 1, 'sabun mandi cair refill', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(934, 1, 'sapu lantai', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(935, 1, 'sapu lidi ', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(936, 1, 'sapu lidi plastik', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(937, 1, 'sariwangi ', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(938, 1, 'sarung tangan karet', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(939, 1, 'serbet', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(940, 1, 'serokan air', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(941, 1, 'serokan sampah', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(942, 1, 'setrikaan', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(943, 1, 'shampo sachet', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(944, 1, 'sikat gigi', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(945, 1, 'SOS', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(946, 1, 'silinder kunci', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(947, 1, 'spons cuci piring', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(948, 1, 'stater lampu', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(949, 1, 'sunlight refill 800 ml', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(950, 1, 'tisu gulung', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(951, 1, 'tisu kotak', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(952, 1, 'tisu makan', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(953, 1, 'tisu plastik', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(954, 1, 'trafo lampu 40 w', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(955, 1, 'wipol refill 800 ml', '', '2', NULL, '-', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(956, 1, 'tinta pulpen', 'Tinta Pulpen merek Standard', '1', NULL, 'qwerty', '0', '2016-06-17', 1, '::1', '2018-05-18 00:00:00', 29, '::1'),
(957, 2, 'Jedai Usefull', 'Aset/Jedai/01', '3', NULL, 'Jedai untuk Mahasiswa', '0', '2018-06-03', 29, '::1', NULL, NULL, NULL),
(958, 2, 'asetnta', 'Aset/Jedai/01', '2', NULL, 'aser', NULL, '0000-00-00', 29, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_r_gedung`
--

CREATE TABLE `tbl_r_gedung` (
  `gedung_id` int(5) NOT NULL,
  `tipe` tinyint(2) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tahun_pembuatan` year(4) DEFAULT NULL,
  `usia` varchar(30) DEFAULT NULL,
  `kelompok` varchar(100) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  `created_date` date NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_r_gedung`
--

INSERT INTO `tbl_r_gedung` (`gedung_id`, `tipe`, `nama`, `tahun_pembuatan`, `usia`, `kelompok`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(4, 1, 'GD 512', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(5, 1, 'GD 513', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(7, 1, 'GD 514', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(8, 1, 'GD 515', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(9, 1, 'GD 516', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(10, 1, 'GD 521', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(11, 1, 'GD 522', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(12, 1, 'GD 523', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(13, 1, 'GD 524', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(14, 1, 'GD 525', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(15, 1, 'GD 526', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(16, 1, 'GD 711', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(17, 1, 'GD 712', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(18, 1, 'GD 713', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(19, 1, 'GD 721', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(20, 1, 'GD 722', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(21, 1, 'GD 723', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(22, 1, 'GD 811', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(23, 1, 'GD 812', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(24, 1, 'GD 813', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(25, 1, 'GD 814', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(26, 1, 'GD 815', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(27, 1, 'GD 816', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(28, 1, 'GD 821', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(29, 1, 'GD 822', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(30, 1, 'GD 823', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(31, 1, 'GD 824', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(32, 1, 'GD 825', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(33, 1, 'GD 826', NULL, NULL, '1', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(34, 3, 'Yayasan Cabang', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(35, 3, 'Kantor Staff', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(36, 3, 'Kantor Dosen', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(37, 3, 'Kantor Duktek', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(38, 3, 'Kantor SDI', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(39, 3, 'TDV', NULL, NULL, '3', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(40, 1, 'Entrance Hall', NULL, NULL, '5', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(41, 2, 'Mansionet', NULL, NULL, '4', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(42, 2, 'Asrama Putri 1', NULL, NULL, '2', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(45, 2, 'Asrama Rusunawa', NULL, NULL, '2', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(46, 2, 'Asrama Putra 1', NULL, NULL, '2', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(47, 2, 'Asrama Putra 2', NULL, NULL, '2', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(48, 2, 'Asrama Putra 3', NULL, NULL, '2', '', '0', '2016-06-09', 3, '::1', NULL, NULL, NULL),
(49, 1, 'Kamar 9 Lantai 2 Asrama Mamre', NULL, NULL, '2', 'Kamar 9 Lantai 2 Asrama Mamre', '0', '2018-06-03', 29, '::1', NULL, NULL, NULL),
(50, 1, 'GD 914', NULL, NULL, '3', 'gedung', '0', '2018-06-06', 29, '::1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_r_kerusakan`
--

CREATE TABLE `tbl_r_kerusakan` (
  `kerusakan_id` int(20) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `berita_acara_id` int(20) DEFAULT NULL,
  `no_laporan` varchar(30) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `bulan` smallint(2) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `jumlah` int(5) DEFAULT '0',
  `satuan` smallint(2) DEFAULT NULL,
  `keterangan` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL,
  `gedung_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_r_kerusakan`
--

INSERT INTO `tbl_r_kerusakan` (`kerusakan_id`, `barang_id`, `berita_acara_id`, `no_laporan`, `tanggal`, `bulan`, `tahun`, `jumlah`, `satuan`, `keterangan`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `gedung_id`) VALUES
(1, 15, NULL, '1', '2018-06-06', NULL, NULL, 120, 1, 'Kursi Roda', NULL, NULL, NULL, NULL, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_r_status`
--

CREATE TABLE `tbl_r_status` (
  `status_id` int(5) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `no` smallint(2) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `grup` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_r_status`
--

INSERT INTO `tbl_r_status` (`status_id`, `kode`, `no`, `nama`, `grup`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, 'kategori_barang', 1, 'Barang Habis', '', 'Barang Habis', '0', NULL, NULL, NULL, '2016-04-25 04:04:54', 1, '::1'),
(2, 'kategori_barang', 2, 'Barang Aset', '', 'Barang Asset', '0', '2016-04-25 00:00:00', 1, '::1', '2016-04-25 05:04:10', 1, '::1'),
(3, 'jenis_barang', 1, 'ATK', '', '', '0', '2016-04-25 00:00:00', 1, '::1', NULL, NULL, NULL),
(4, 'jenis_barang', 2, 'Rumah Tangga', '', '', '0', '2016-04-25 00:00:00', 1, '::1', NULL, NULL, NULL),
(5, 'jenis_barang', 3, 'Mahasiswa', '', '', '0', '2016-04-25 00:00:00', 1, '::1', NULL, NULL, NULL),
(6, 'tipe_gedung', 1, 'Ruangan', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-06-09 10:24:44', 1, '::1'),
(7, 'tipe_gedung', 2, 'Perumahan', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-04-25 09:04:02', 1, '::1'),
(8, 'tipe_gedung', 3, 'Perkantoran', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-06-09 10:25:01', 1, '::1'),
(9, 'kelompok_gedung', 1, 'Ruang Kelas/Lab', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-06-09 10:25:17', 1, '::1'),
(10, 'kelompok_gedung', 2, 'Asrama', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-06-09 10:25:31', 1, '::1'),
(11, 'kelompok_gedung', 3, 'Ruang Dosen/Staff', '', '', '0', '2016-04-25 00:00:00', 1, '::1', '2016-06-09 10:25:48', 1, '::1'),
(12, 'tipe_permintaan', 1, 'Permintaan', '', '', '0', '2016-04-28 00:00:00', 1, '::1', '2016-06-07 05:32:50', 1, '::1'),
(14, 'status_permintaan', 1, 'Requested', '', '', '0', '2016-04-28 00:00:00', 1, '::1', NULL, NULL, NULL),
(15, 'status_permintaan', 2, 'Aprroved', '', '', '0', '2016-04-28 00:00:00', 1, '::1', NULL, NULL, NULL),
(16, 'status_permintaan', 3, 'Rejected', '', '', '0', '2016-04-28 00:00:00', 1, '::1', NULL, NULL, NULL),
(17, 'bulan', 1, 'Januari', '', '', '0', '2016-04-28 05:40:59', 1, '::1', NULL, NULL, NULL),
(18, 'bulan', 2, 'Februari', '', '', '0', '2016-04-28 05:41:35', 1, '::1', NULL, NULL, NULL),
(19, 'bulan', 3, 'Maret', '', '', '0', '2016-04-28 05:41:46', 1, '::1', NULL, NULL, NULL),
(20, 'bulan', 4, 'April', '', '', '0', '2016-04-28 05:42:01', 1, '::1', NULL, NULL, NULL),
(21, 'bulan', 5, 'Mei', '', '', '0', '2016-04-28 05:42:19', 1, '::1', NULL, NULL, NULL),
(22, 'bulan', 6, 'Juni', '', '', '0', '2016-04-28 05:42:36', 1, '::1', NULL, NULL, NULL),
(23, 'bulan', 7, 'Juli', '', '', '0', '2016-04-28 05:42:59', 1, '::1', NULL, NULL, NULL),
(24, 'bulan', 8, 'Agustus', '', '', '0', '2016-04-28 05:44:14', 1, '::1', NULL, NULL, NULL),
(25, 'bulan', 9, 'September', '', '', '0', '2016-04-28 05:44:29', 1, '::1', NULL, NULL, NULL),
(26, 'bulan', 10, 'Oktober', '', '', '0', '2016-04-28 05:44:42', 1, '::1', NULL, NULL, NULL),
(27, 'bulan', 11, 'November', '', '', '0', '2016-04-28 05:45:01', 1, '::1', NULL, NULL, NULL),
(28, 'bulan', 12, 'Desember', '', '', '0', '2016-04-28 05:45:09', 1, '::1', NULL, NULL, NULL),
(29, 'tahun', 2015, '2015', '', '', '0', '2016-04-28 05:45:40', 1, '::1', NULL, NULL, NULL),
(30, 'tahun', 2016, '2016', '', '', '0', '2016-04-28 05:45:49', 1, '::1', NULL, NULL, NULL),
(31, 'tahun', 2017, '2017', '', '', '0', '2016-04-28 05:45:58', 1, '::1', NULL, NULL, NULL),
(32, 'tahun', 2018, '2018', '', '', '0', '2016-04-28 05:46:08', 1, '::1', NULL, NULL, NULL),
(35, 'kondisi_barang', 1, 'Baik', '', '', '0', '2016-06-06 12:12:29', 1, '::1', NULL, NULL, NULL),
(36, 'kondisi_barang', 2, 'Rusak', '', '', '0', '2016-06-06 12:12:38', 1, '::1', NULL, NULL, NULL),
(39, 'satuan_barang', 1, 'Buah', '', '', '0', '2016-06-07 04:09:18', 1, '::1', '2016-06-07 05:39:29', 1, '::1'),
(40, 'divisi', 1, 'Staff Inventory', '', '', '0', '2016-06-07 06:41:40', 1, '::1', NULL, NULL, NULL),
(41, 'divisi', 2, 'Mahasiswa', '', '', '0', '2016-06-07 06:41:53', 1, '::1', NULL, NULL, NULL),
(42, 'divisi', 3, 'Dosen/Staf', '', '', '0', '2016-06-07 06:42:03', 1, '::1', NULL, NULL, NULL),
(43, 'approval', 1, 'Requested', '', '', '0', '2016-06-09 06:07:05', 2, '::1', '2018-06-06 03:17:12', 29, '::1'),
(44, 'approval', 2, 'Approved', '', '', '0', '2016-06-09 06:07:15', 2, '::1', NULL, NULL, NULL),
(45, 'approval', 3, 'Rejected', '', '', '0', '2016-06-09 06:07:29', 2, '::1', NULL, NULL, NULL),
(46, 'status_pinjam', 1, 'Requested', '', '', '0', '2016-06-09 06:07:45', 2, '::1', NULL, NULL, NULL),
(47, 'status_pinjam', 2, 'Disetujui', '', '', '0', '2016-06-09 06:08:05', 2, '::1', NULL, NULL, NULL),
(48, 'status_pinjam', 3, 'Ditolak', '', '', '0', '2016-06-09 06:08:25', 2, '::1', NULL, NULL, NULL),
(49, 'status_kembali', 1, 'Sudah Dikembalikan', '', '', '0', '2016-06-09 06:08:50', 2, '::1', NULL, NULL, NULL),
(50, 'status_kembali', 2, 'Belum DIkembalikan', '', '', '0', '2016-06-09 06:09:04', 2, '::1', NULL, NULL, NULL),
(51, 'kelompok_gedung', 4, 'Rumah Dosen/Staff', '', '', '0', '2016-06-09 10:26:31', 1, '::1', NULL, NULL, NULL),
(52, 'kelompok_gedung', 5, 'Beranda', '', '', '0', '2016-06-09 10:26:46', 1, '::1', NULL, NULL, NULL),
(53, 'jenis_barang', 0, '-', '', '', '0', '2016-06-09 12:08:45', 1, '::1', NULL, NULL, NULL),
(54, 'satuan_barang', 2, 'Unit', '', '', '0', '2016-06-09 12:31:03', 1, '::1', '2016-06-13 07:00:32', 1, '::1'),
(55, 'satuan_barang', 3, 'Botol', '', '', '0', '2016-06-09 12:31:26', 1, '::1', '2016-06-13 07:00:46', 1, '::1'),
(56, 'tipe_pengadaan_barang', 1, 'Request', '', '', '0', '2016-06-10 03:31:14', 1, '::1', '2016-06-10 03:32:35', 1, '::1'),
(57, 'tipe_pengadaan_barang', 2, 'Needed', '', '', '0', '2016-06-10 03:31:31', 1, '::1', '2016-06-10 03:32:48', 1, '::1'),
(58, 'satuan_barang', 4, 'Bungkus', '', '', '0', '2016-06-13 07:02:36', 1, '::1', NULL, NULL, NULL),
(59, 'satuan_barang', 5, 'Kotak', '', '', '0', '2016-06-13 07:02:47', 1, '::1', NULL, NULL, NULL),
(60, 'satuan_barang', 6, 'Peal', '', '', '0', '2016-06-13 07:03:34', 1, '::1', NULL, NULL, NULL),
(61, 'satuan_barang', 7, 'Bal', '', '', '0', '2016-06-13 07:03:46', 1, '::1', NULL, NULL, NULL),
(62, 'satuan_barang', 8, 'Rim', '', '', '0', '2016-06-13 07:04:40', 1, '::1', NULL, NULL, NULL),
(63, 'satuan_barang', 9, 'Jirigen', '', '', '0', '2016-06-13 07:05:49', 1, '::1', NULL, NULL, NULL),
(64, 'satuan_barang', 10, 'Liter', NULL, '', '0', '2016-06-23 16:21:15', 1, '::1', NULL, NULL, NULL),
(65, 'satuan_barang', 11, 'Ton', NULL, '', '0', '2016-06-24 04:21:19', 1, '::1', NULL, NULL, NULL),
(66, 'satuan_barang', 12, 'KG', NULL, '', '0', '2016-06-24 09:19:19', 1, '::1', NULL, NULL, NULL),
(67, 'tipe_pengadaan_barang', 3, 'test', NULL, '', '0', '2016-06-24 09:22:57', 1, '::1', NULL, NULL, NULL),
(68, 'divisi', 4, 'Administrator', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(69, 'status_mutasi', 1, 'Berhasil', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(70, 'status_mutasi', 2, 'Tidak Berhasil', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(71, 'status_pindah', 1, 'Sudah Dipindahkan', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(72, 'status_pindah', 2, 'Belum Dipindahkan', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(73, 'status_pencatatan', 1, 'Berhasil', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL),
(74, 'staus_pencatatan', 2, 'Tidak Berhasil', NULL, '', '0', '2018-05-21 21:43:39', 29, '::1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_bukuinventory`
--

CREATE TABLE `tbl_t_bukuinventory` (
  `buku_id` int(10) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `gedung_id` int(5) NOT NULL,
  `lokasi` smallint(2) DEFAULT NULL,
  `no_register` varchar(50) DEFAULT NULL COMMENT 'baiknya disini atau di barang',
  `baik` int(5) DEFAULT '0',
  `rusak` int(5) DEFAULT '0',
  `jumlah` int(5) DEFAULT '0',
  `satuan` smallint(2) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `deleted` char(1) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL,
  `merk` varchar(50) DEFAULT NULL,
  `pencatatan_id` int(10) DEFAULT NULL,
  `available` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_bukuinventory`
--

INSERT INTO `tbl_t_bukuinventory` (`buku_id`, `barang_id`, `gedung_id`, `lokasi`, `no_register`, `baik`, `rusak`, `jumlah`, `satuan`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `merk`, `pencatatan_id`, `available`) VALUES
(142, 15, 4, NULL, NULL, 980, 120, 1100, 1, 'Kursi Roda', '0', '2018-06-05', 29, '::1', NULL, NULL, NULL, NULL, NULL, 900),
(143, 17, 5, NULL, NULL, 20, 0, 20, 2, 'meja dosen besar', '0', '2018-06-06', 29, '::1', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_mutasi_aset`
--

CREATE TABLE `tbl_t_mutasi_aset` (
  `mutasi_id` bigint(10) NOT NULL,
  `pencatatan_id` int(10) NOT NULL,
  `kode_trans` varchar(30) NOT NULL,
  `tanggal_trans` date NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `lokasi_tujuan` varchar(255) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `alasan_mutasi` varchar(255) NOT NULL,
  `status_mutasi` varchar(255) DEFAULT NULL,
  `status_pindah` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL,
  `barang_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_mutasi_aset`
--

INSERT INTO `tbl_t_mutasi_aset` (`mutasi_id`, `pencatatan_id`, `kode_trans`, `tanggal_trans`, `lokasi`, `lokasi_tujuan`, `jumlah`, `alasan_mutasi`, `status_mutasi`, `status_pindah`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `barang_id`) VALUES
(12, 18, '01/MA/MD', '2018-06-06', '5', '12', 15, 'urusan kuliah', '1', '2', '2018-06-06 00:00:00', 29, '::1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_peminjaman`
--

CREATE TABLE `tbl_t_peminjaman` (
  `peminjaman_id` bigint(10) NOT NULL,
  `no_dokumen` varchar(50) DEFAULT NULL,
  `pemohon` int(5) NOT NULL,
  `tanggal_trans` date NOT NULL,
  `tanggal_pinjam` date DEFAULT NULL,
  `rencana_kembali` date DEFAULT NULL,
  `rencana_waktu_kembali` time DEFAULT NULL,
  `keterangan` text,
  `approval` tinyint(1) DEFAULT NULL,
  `approval_by` int(5) DEFAULT NULL,
  `alasan_penolakan` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_peminjaman`
--

INSERT INTO `tbl_t_peminjaman` (`peminjaman_id`, `no_dokumen`, `pemohon`, `tanggal_trans`, `tanggal_pinjam`, `rencana_kembali`, `rencana_waktu_kembali`, `keterangan`, `approval`, `approval_by`, `alasan_penolakan`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, 'DOCPEMINJAMAN274', 30, '2018-06-06', '2018-06-08', '2018-06-13', '08:50:20', 'untuk acara himsi', 2, 29, NULL, '2018-06-06 00:00:00', 30, '::1', NULL, NULL, NULL),
(2, 'DOCPEMINJAMAN274', 30, '2018-06-10', '2018-06-12', '2018-06-14', '07:36:35', 'Untuk Perkuliahan', 2, 29, NULL, '2018-06-10 00:00:00', 30, '::1', NULL, NULL, NULL),
(3, NULL, 30, '2018-06-12', '2018-06-14', '2018-06-18', '23:45:05', 'Acara Himsi', 1, NULL, NULL, '2018-06-12 00:00:00', 30, '::1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_peminjaman_detail`
--

CREATE TABLE `tbl_t_peminjaman_detail` (
  `peminjaman_detail_id` bigint(20) NOT NULL,
  `peminjaman_id` bigint(10) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `jumlah` int(5) DEFAULT '0',
  `satuan` smallint(2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status_pinjam` tinyint(1) DEFAULT NULL,
  `status_kembali` tinyint(1) DEFAULT NULL,
  `status_kembali_by` int(5) DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_peminjaman_detail`
--

INSERT INTO `tbl_t_peminjaman_detail` (`peminjaman_detail_id`, `peminjaman_id`, `barang_id`, `jumlah`, `satuan`, `keterangan`, `status_pinjam`, `status_kembali`, `status_kembali_by`, `tanggal_kembali`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, 1, 15, 6, 1, 'untuk acara himsi', 2, 1, NULL, NULL, '2018-06-06 00:00:00', 30, '::1', NULL, NULL, NULL),
(2, 2, 17, 2, NULL, NULL, 2, NULL, NULL, NULL, '2018-06-10 00:00:00', 30, '::1', NULL, NULL, NULL),
(3, 3, 15, 2, NULL, NULL, 1, NULL, NULL, NULL, '2018-06-12 00:00:00', 30, '::1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_pencatatan_aset`
--

CREATE TABLE `tbl_t_pencatatan_aset` (
  `pencatatan_id` int(10) NOT NULL,
  `barang_id` int(10) DEFAULT NULL,
  `gedung_id` int(5) NOT NULL,
  `kode_trans` varchar(30) NOT NULL,
  `tanggal_trans` date NOT NULL,
  `jumlah` int(5) NOT NULL,
  `satuan` smallint(2) NOT NULL,
  `keterangan` text NOT NULL,
  `sumber_perolehan` tinyint(1) DEFAULT NULL,
  `kondisi` tinyint(1) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL,
  `buku_id` int(10) NOT NULL,
  `status_pencatatan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_pencatatan_aset`
--

INSERT INTO `tbl_t_pencatatan_aset` (`pencatatan_id`, `barang_id`, `gedung_id`, `kode_trans`, `tanggal_trans`, `jumlah`, `satuan`, `keterangan`, `sumber_perolehan`, `kondisi`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `buku_id`, `status_pencatatan`) VALUES
(17, 15, 5, 'Trns/Pncttn/Kursi Roda', '2018-06-15', 90, 1, 'Kursi Roda', NULL, NULL, '2018-06-05 00:00:00', 29, '::1', NULL, NULL, NULL, 142, '1'),
(18, 17, 5, '01', '2018-06-06', 5, 2, 'meja dosen besar', NULL, NULL, '2018-06-06 00:00:00', 29, '::1', NULL, NULL, NULL, 143, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_pengadaan`
--

CREATE TABLE `tbl_t_pengadaan` (
  `pengadaan_id` bigint(10) NOT NULL,
  `tipe` tinyint(2) DEFAULT NULL,
  `kode_trans` varchar(30) NOT NULL,
  `tanggal_trans` date NOT NULL,
  `bulan_trans` smallint(2) NOT NULL,
  `tahun_trans` year(4) NOT NULL,
  `keterangan` text NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_pengadaan`
--

INSERT INTO `tbl_t_pengadaan` (`pengadaan_id`, `tipe`, `kode_trans`, `tanggal_trans`, `bulan_trans`, `tahun_trans`, `keterangan`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, NULL, '01/P/2018', '2018-06-06', 0, 0000, '', '0000-00-00 00:00:00', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_pengadaan_detail`
--

CREATE TABLE `tbl_t_pengadaan_detail` (
  `pengadaan_detail_id` bigint(20) NOT NULL,
  `pengadaan_id` bigint(10) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `jumlah` int(5) DEFAULT '0',
  `satuan` smallint(2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_pengadaan_detail`
--

INSERT INTO `tbl_t_pengadaan_detail` (`pengadaan_detail_id`, `pengadaan_id`, `barang_id`, `jumlah`, `satuan`, `keterangan`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, 1, 763, 10, 3, 'alkohol 96%', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_permintaan`
--

CREATE TABLE `tbl_t_permintaan` (
  `permintaan_id` bigint(10) NOT NULL,
  `tipe` tinyint(2) DEFAULT NULL,
  `no_dokumen` varchar(50) DEFAULT NULL,
  `pemohon` int(11) NOT NULL,
  `tanggal_trans` date DEFAULT NULL,
  `bulan_trans` smallint(2) DEFAULT NULL,
  `tahun_trans` year(4) DEFAULT NULL,
  `keterangan` text,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` year(4) DEFAULT NULL,
  `alasan_penolakan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_permintaan`
--

INSERT INTO `tbl_t_permintaan` (`permintaan_id`, `tipe`, `no_dokumen`, `pemohon`, `tanggal_trans`, `bulan_trans`, `tahun_trans`, `keterangan`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `alasan_penolakan`) VALUES
(1, 1, 'DOCPERMINTAAN274', 30, '2018-06-08', 6, 2018, 'untuk acara himsi', '2018-06-06 00:00:00', 30, '::1', NULL, NULL, NULL, NULL),
(2, 1, 'DOCPERMINTAAN274', 30, '2018-06-12', 6, 2018, 'Untuk perkuliahan', '2018-06-10 00:00:00', 30, '::1', NULL, NULL, NULL, NULL),
(3, 1, 'DOCPERMINTAAN274', 30, '2018-06-14', 6, 2018, 'Acara Himsi', '2018-06-12 00:00:00', 30, '::1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_permintaan_detail`
--

CREATE TABLE `tbl_t_permintaan_detail` (
  `permintaan_detail_id` bigint(20) NOT NULL,
  `permintaan_id` bigint(10) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `jumlah` int(5) DEFAULT '0',
  `satuan` smallint(2) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `status_by` int(5) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_permintaan_detail`
--

INSERT INTO `tbl_t_permintaan_detail` (`permintaan_detail_id`, `permintaan_id`, `barang_id`, `jumlah`, `satuan`, `keterangan`, `status`, `status_by`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`) VALUES
(1, 1, 763, 9, 3, 'untuk acara himsi', 2, 29, '2018-06-06 00:00:00', 30, '::1', NULL, NULL, NULL),
(2, 2, 763, 0, NULL, NULL, 2, 29, '2018-06-10 00:00:00', 30, '::1', NULL, NULL, NULL),
(3, 3, 763, 5, NULL, NULL, 2, 29, '2018-06-12 00:00:00', 30, '::1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_stokbarang`
--

CREATE TABLE `tbl_t_stokbarang` (
  `stok_id` int(10) NOT NULL,
  `barang_id` int(10) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `jumlah` int(5) NOT NULL DEFAULT '0',
  `gedung_id` int(11) DEFAULT NULL,
  `satuan` varchar(15) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `deleted` char(1) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(5) NOT NULL,
  `created_ip` varchar(15) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` int(5) DEFAULT NULL,
  `modified_ip` varchar(15) DEFAULT NULL,
  `available` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_t_stokbarang`
--

INSERT INTO `tbl_t_stokbarang` (`stok_id`, `barang_id`, `kode`, `jumlah`, `gedung_id`, `satuan`, `deskripsi`, `deleted`, `created_date`, `created_by`, `created_ip`, `modified_date`, `modified_by`, `modified_ip`, `available`) VALUES
(1, 763, '01', 21, 4, '3', 'alkohol 96%', '0', '2018-06-06 00:00:00', 29, '::1', '2018-06-06', 29, '::1', 16),
(2, 765, 'wdwd', 1, 7, '2', 'df', '0', '2018-06-06 00:00:00', 29, '::1', NULL, NULL, NULL, 1),
(3, 765, 'sdd', 89, 7, '5', 'wded', '0', '2018-06-06 00:00:00', 29, '::1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `divisi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `username`, `password`, `divisi`) VALUES
(2, 'Suci Monika Sinaga', 'sucimonika@gmail.com', 'sucimonika', '$2y$13$g8NEjnnVMHPCvRMmKXFIU.H8Fk/5IG.LOOirQ7k2y9SeG2X8yOSpW', 1),
(28, 'test', 'test@gmail.com', 'test', '$2y$13$t8E.7y0RNrIDGnFYWAasP.mTNfGCvd14auNpf9dzVcwLgzkMYKsB6', 2),
(29, 'Mei', 'iss15038@students.del.ac.id', 'suci', '$2y$13$5.xoi2N3zpBgBx0pcFwZnuNY9f3216q2/TZW.WvqRLlJJ1k2LpH96', 1),
(30, 'anggi', 'iss15048@students.del.ac.id', 'anggi', '$2y$13$eNQpNrzbOLPIJYMjg4l2yOCf5AQPFvXCmE9tk.tmuPKMmVVZXsWi.', 2),
(31, 'widya', 'iss15042@students.del.ac.id', 'widya', '$2y$13$K4SLXwC61UbAULnAA0xkhO8eAxsBZXn77uDNpJOcmh2Vh.UsV19YW', 3),
(32, 'Safiah', 'iss15012@students.del.ac.id', 'safiah', '$2y$13$fdBZAlpq9DkZjN1wLEzkfO67xFfSVAEsrPbo5yupikZYiW9D3I7mW', 1),
(33, 'Sarah', 'iss15014@students.del.ac.id', 'sarah', '$2y$13$r5hzaBhOJBnyUPTFtXYABek3pQuuVPswoBRCqOSgZzFRAjzVfOJ3i', 2),
(34, 'desy', 'iss15049@students.del.ac.id', 'desy', '$2y$13$hgqPTHRBhU6WmWJuMEf/S.37uB9HBmx3HL9sssSObLA65Fus4EXVC', 3),
(35, 'administrator', 'riahta.mei38@gmail.com', 'administrator', '$2y$13$f3GHrV4pqQeN01QBA9tLtuuTnENqyGyRgiqZ3hVBqkFsOad4K3HPu', 4),
(36, 'amen', 'aaaaa@gmail.com', 'amen', '$2y$13$0k.LJ.1OeirKjkoltFcqGuF1Ey6rk//Nzmva5eLuYAuGR33isER7u', 2),
(37, 'Widya Simbolon', 'iss15042@students.del.ac.id', 'WidyaS', '$2y$13$eNQpNrzbOLPIJYMjg4l2yOCf5AQPFvXCmE9tk.tmuPKMmVVZXsWi.', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `tbl_r_barang`
--
ALTER TABLE `tbl_r_barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- Indexes for table `tbl_r_gedung`
--
ALTER TABLE `tbl_r_gedung`
  ADD PRIMARY KEY (`gedung_id`);

--
-- Indexes for table `tbl_r_kerusakan`
--
ALTER TABLE `tbl_r_kerusakan`
  ADD PRIMARY KEY (`kerusakan_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `gedung_id` (`gedung_id`);

--
-- Indexes for table `tbl_r_status`
--
ALTER TABLE `tbl_r_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `tbl_t_bukuinventory`
--
ALTER TABLE `tbl_t_bukuinventory`
  ADD PRIMARY KEY (`buku_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `gedung_id` (`gedung_id`),
  ADD KEY `tbl_t_bukuinventory_ibfk_3` (`pencatatan_id`);

--
-- Indexes for table `tbl_t_mutasi_aset`
--
ALTER TABLE `tbl_t_mutasi_aset`
  ADD PRIMARY KEY (`mutasi_id`),
  ADD KEY `buku_id` (`pencatatan_id`),
  ADD KEY `fk_2` (`created_by`);

--
-- Indexes for table `tbl_t_peminjaman`
--
ALTER TABLE `tbl_t_peminjaman`
  ADD PRIMARY KEY (`peminjaman_id`);

--
-- Indexes for table `tbl_t_peminjaman_detail`
--
ALTER TABLE `tbl_t_peminjaman_detail`
  ADD PRIMARY KEY (`peminjaman_detail_id`),
  ADD KEY `peminjaman_id` (`peminjaman_id`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indexes for table `tbl_t_pencatatan_aset`
--
ALTER TABLE `tbl_t_pencatatan_aset`
  ADD PRIMARY KEY (`pencatatan_id`,`created_date`,`created_by`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `gedung_id` (`gedung_id`);

--
-- Indexes for table `tbl_t_pengadaan`
--
ALTER TABLE `tbl_t_pengadaan`
  ADD PRIMARY KEY (`pengadaan_id`);

--
-- Indexes for table `tbl_t_pengadaan_detail`
--
ALTER TABLE `tbl_t_pengadaan_detail`
  ADD PRIMARY KEY (`pengadaan_detail_id`),
  ADD KEY `pengadaan_id` (`pengadaan_id`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indexes for table `tbl_t_permintaan`
--
ALTER TABLE `tbl_t_permintaan`
  ADD PRIMARY KEY (`permintaan_id`);

--
-- Indexes for table `tbl_t_permintaan_detail`
--
ALTER TABLE `tbl_t_permintaan_detail`
  ADD PRIMARY KEY (`permintaan_detail_id`),
  ADD KEY `permintaan_id` (`permintaan_id`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indexes for table `tbl_t_stokbarang`
--
ALTER TABLE `tbl_t_stokbarang`
  ADD PRIMARY KEY (`stok_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `tbl_t_stokbarang_ibfk_2` (`gedung_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `divisi` (`divisi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_r_barang`
--
ALTER TABLE `tbl_r_barang`
  MODIFY `barang_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=959;
--
-- AUTO_INCREMENT for table `tbl_r_gedung`
--
ALTER TABLE `tbl_r_gedung`
  MODIFY `gedung_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tbl_r_kerusakan`
--
ALTER TABLE `tbl_r_kerusakan`
  MODIFY `kerusakan_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_r_status`
--
ALTER TABLE `tbl_r_status`
  MODIFY `status_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `tbl_t_bukuinventory`
--
ALTER TABLE `tbl_t_bukuinventory`
  MODIFY `buku_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `tbl_t_mutasi_aset`
--
ALTER TABLE `tbl_t_mutasi_aset`
  MODIFY `mutasi_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_t_peminjaman`
--
ALTER TABLE `tbl_t_peminjaman`
  MODIFY `peminjaman_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_t_peminjaman_detail`
--
ALTER TABLE `tbl_t_peminjaman_detail`
  MODIFY `peminjaman_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_t_pencatatan_aset`
--
ALTER TABLE `tbl_t_pencatatan_aset`
  MODIFY `pencatatan_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tbl_t_pengadaan`
--
ALTER TABLE `tbl_t_pengadaan`
  MODIFY `pengadaan_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_t_pengadaan_detail`
--
ALTER TABLE `tbl_t_pengadaan_detail`
  MODIFY `pengadaan_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_t_permintaan`
--
ALTER TABLE `tbl_t_permintaan`
  MODIFY `permintaan_id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_t_permintaan_detail`
--
ALTER TABLE `tbl_t_permintaan_detail`
  MODIFY `permintaan_detail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_t_stokbarang`
--
ALTER TABLE `tbl_t_stokbarang`
  MODIFY `stok_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_r_kerusakan`
--
ALTER TABLE `tbl_r_kerusakan`
  ADD CONSTRAINT `tbl_r_kerusakan_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_r_kerusakan_ibfk_2` FOREIGN KEY (`gedung_id`) REFERENCES `tbl_r_gedung` (`gedung_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_bukuinventory`
--
ALTER TABLE `tbl_t_bukuinventory`
  ADD CONSTRAINT `tbl_t_bukuinventory_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_bukuinventory_ibfk_2` FOREIGN KEY (`gedung_id`) REFERENCES `tbl_r_gedung` (`gedung_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_bukuinventory_ibfk_3` FOREIGN KEY (`pencatatan_id`) REFERENCES `tbl_t_pencatatan_aset` (`pencatatan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_mutasi_aset`
--
ALTER TABLE `tbl_t_mutasi_aset`
  ADD CONSTRAINT `fk_1` FOREIGN KEY (`pencatatan_id`) REFERENCES `tbl_t_pencatatan_aset` (`pencatatan_id`),
  ADD CONSTRAINT `fk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`);

--
-- Constraints for table `tbl_t_peminjaman_detail`
--
ALTER TABLE `tbl_t_peminjaman_detail`
  ADD CONSTRAINT `tbl_t_peminjaman_detail_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_peminjaman_detail_ibfk_2` FOREIGN KEY (`peminjaman_id`) REFERENCES `tbl_t_peminjaman` (`peminjaman_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_pencatatan_aset`
--
ALTER TABLE `tbl_t_pencatatan_aset`
  ADD CONSTRAINT `tbl_t_pencatatan_aset_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_pencatatan_aset_ibfk_2` FOREIGN KEY (`gedung_id`) REFERENCES `tbl_r_gedung` (`gedung_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_pengadaan_detail`
--
ALTER TABLE `tbl_t_pengadaan_detail`
  ADD CONSTRAINT `tbl_t_pengadaan_detail_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_pengadaan_detail_ibfk_2` FOREIGN KEY (`pengadaan_id`) REFERENCES `tbl_t_pengadaan` (`pengadaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_permintaan_detail`
--
ALTER TABLE `tbl_t_permintaan_detail`
  ADD CONSTRAINT `tbl_t_permintaan_detail_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_permintaan_detail_ibfk_2` FOREIGN KEY (`permintaan_id`) REFERENCES `tbl_t_permintaan` (`permintaan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_t_stokbarang`
--
ALTER TABLE `tbl_t_stokbarang`
  ADD CONSTRAINT `tbl_t_stokbarang_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `tbl_r_barang` (`barang_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tbl_t_stokbarang_ibfk_2` FOREIGN KEY (`gedung_id`) REFERENCES `tbl_r_gedung` (`gedung_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
