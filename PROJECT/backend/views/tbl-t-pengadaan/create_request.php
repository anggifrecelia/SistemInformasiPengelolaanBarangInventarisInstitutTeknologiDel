<?php

use yii\helpers\Html;
use backend\models\TblTPengadaanDetail;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaan */
$modelsTblTPengadaanDetail = [new TblTPengadaanDetail];
$this->title = 'Pengadaan Barang ';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpengadaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_detail_request', [
        'model' => $model,
        'modelsTblTPengadaanDetail' => (empty($modelsTblTPengadaanDetail)) ? [new TblTPengadaanDetail] : $modelsTblTPengadaanDetail
    ]) ?>

</div>
