<?php

namespace backend\models;

use Yii;
use backend\models\User;
/**
 * This is the model class for table "tbl_t_peminjaman".
 *
 * @property string $peminjaman_id
 * @property string $no_dokumen
 * @property integer $pemohon
 * @property string $tanggal_trans
 * @property string $tanggal_pinjam
 * @property string $rencana_kembali
 * @property string $rencana_waktu_kembali
 * @property string $keterangan
 * @property integer $approval
 * @property integer $approval_by
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTPeminjamanDetail[] $tblTPeminjamanDetails
 */
class TblTPeminjaman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_peminjaman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_dokumen', 'pemohon', 'tanggal_trans'], 'required'],
            [['pemohon', 'approval', 'approval_by', 'created_by', 'modified_by'], 'integer'],
            [['tanggal_trans', 'tanggal_pinjam', 'rencana_kembali', 'rencana_waktu_kembali', 'created_date', 'modified_date'], 'safe'],
            [['keterangan', 'alasan_penolakan'], 'string'],
            [['no_dokumen'], 'string', 'max' => 50],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'peminjaman_id' => 'Peminjaman ID',
            'no_dokumen' => 'No Dokumen',
            'pemohon' => 'Pemohon',
            'tanggal_trans' => 'Tanggal Trans',
            'tanggal_pinjam' => 'Tanggal Pinjam',
            'rencana_kembali' => 'Rencana Kembali',
            'rencana_waktu_kembali' => 'Rencana Waktu Kembali',
            'keterangan' => 'Keterangan',
            'approval' => 'Approval',
            'approval_by' => 'Approval By',
            'alasan_penolakan' => 'Alasan Penolakan',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPeminjamanDetails()
    {
        return $this->hasMany(TblTPeminjamanDetail::className(), ['peminjaman_id' => 'peminjaman_id']);
    }
    
    public function getPemohon() {
        return $this->hasMany(User::className(), ['id' => 'id']);
    }

     public function getPeminjaman()
    {
        return $this->hasOne(TblRBarang::className(), ['peminjaman_id' => 'peminjaman_id']);
    }

}
