<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAset */

// $this->title = 'Perpindahan/Mutasi Barang Aset';
$this->params['breadcrumbs'][] = ['label' => 'Perpindahan/Mutasi Barang Aset', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
