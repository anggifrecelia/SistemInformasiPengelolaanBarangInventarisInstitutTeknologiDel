<?php

namespace frontend\models;

use Yii;
use frontend\models\User;
/**
 * This is the model class for table "tbl_t_permintaan".
 *
 * @property string $permintaan_id
 * @property integer $tipe
 * @property integer $pemohon
 * @property string $tanggal_trans
 * @property integer $bulan_trans
 * @property string $tahun_trans
 * @property string $keterangan
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTPermintaanDetail[] $tblTPermintaanDetails
 */
class TblTPermintaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_permintaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_dokumen', 'pemohon', 'tahun_trans', 'bulan_trans', 'no_dokumen'], 'required'],
            [['pemohon', 'tahun_trans', 'bulan_trans', 'created_by', 'modified_by'], 'integer'],
            [['tanggal_trans', 'tahun_trans', 'created_date', 'modified_date', 'modified_ip'], 'safe'],
            [['keterangan'], 'string'],
            [['no_dokumen'], 'string', 'max' => 50],
            [['created_ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'permintaan_id' => 'Permintaan ID',
            'no_dokumen' => 'Nomor Dokumen',
            'tipe' => 'Tipe',
            'pemohon' => 'Pemohon',
            'tanggal_trans' => 'Tanggal Transaksi',
            'bulan_trans' => 'Bulan Trans',
            'tahun_trans' => 'Tahun Trans',
            'keterangan' => 'Keterangan',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPermintaanDetails()
    {
        return $this->hasMany(TblTPermintaanDetail::className(), ['permintaan_id' => 'permintaan_id']);
    }
    
    public function getPemohon()
    {
        $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
        return $pemohon->nama;
    }
}
