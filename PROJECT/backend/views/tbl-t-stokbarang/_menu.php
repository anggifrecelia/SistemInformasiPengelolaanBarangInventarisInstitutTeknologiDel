<?php


use yii\bootstrap\Nav;

?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
    ],
    'items' => [
        [
            'label' => Yii::t('app','Permintaan Barang'),
            'url'     => ['/tbl-t-permintaan/index'],
        ],
        // [
        //     'label'   => Yii::t('app', 'Pengadaan Barang'),
        //     'url'     => ['/tbl-t-stokbarang/pengadaan_khusus'],
        // ],
        
    ],
]) ?>
