<?php

use yii\helpers\Html;
use frontend\models\TblTPeminjamanDetail;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPeminjaman */

// $this->title = 'Create Peminajaman';
$this->params['breadcrumbs'][] = ['label' => 'Peminjaman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPeminjamanDetail] : $modelsDetail
    ]) ?>

</div>
