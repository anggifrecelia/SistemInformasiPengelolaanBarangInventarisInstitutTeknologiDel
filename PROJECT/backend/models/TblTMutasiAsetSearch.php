<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTMutasiAset;

/**
 * TblTMutasiAsetSearch represents the model behind the search form about `backend\models\TblTMutasiAset`.
 */
class TblTMutasiAsetSearch extends TblTMutasiAset
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mutasi_id', 'pencatatan_id', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['kode_trans', 'tanggal_trans', 'lokasi', 'lokasi_tujuan', 'alasan_mutasi', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTMutasiAset::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'mutasi_id' => $this->mutasi_id,
            'pencatatan_id' => $this->pencatatan_id,
            'tanggal_trans' => $this->tanggal_trans,
            'jumlah' => $this->jumlah,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'kode_trans', $this->kode_trans])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'lokasi_tujuan', $this->lokasi_tujuan])
            ->andFilterWhere(['like', 'alasan_mutasi', $this->alasan_mutasi])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
