<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use frontend\models\TblTPeminjamanDetail;
use frontend\models\TblRStatus;
use frontend\models\User;



/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPeminjaman */




$this->title = $model->peminjaman_id;
$this->params['breadcrumbs'][] = ['label' => 'Peminjaman', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;

$user = User::findOne(['id'=>$model->pemohon]);
$approvalBy = User::findOne(['id'=>$model->approval_by]);
$status = TblRStatus::findOne(['kode'=>'approval', 'no'=>$model->approval]);



?>

<h1 >Peminjaman Anda</h1>

<div class="tbl-tpeminjaman-view">

    <!--h1><!--?= Html::encode($this->title) ?></h1-->

    <p>
        <!--?= Html::a('Update', ['update', 'id' => $model->peminjaman_id], ['class' => 'btn btn-primary']) ?>
        <!--?= Html::a('Delete', ['delete', 'id' => $model->peminjaman_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'peminjaman_id',
            // 'no_dokumen',
            [
                'label'  => 'Pemohon',
                'value' => $user->nama,
            ],
            'tanggal_trans',
            'tanggal_pinjam',
            'rencana_kembali',
            'rencana_waktu_kembali',
            'keterangan:ntext',
            //'approval',
            [
                'label'  => 'Approval',
                'value' => $status->nama,
            ],
            //'approval_by',
//             [
//                 'label'  => 'Disetujui Oleh',
//                 'value' => $approvalBy->nama,
//             ],
// //            'created_date',
//            'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>
    
    <h2 align="center">Detail Peminjaman</h2>
    <?= GridView::widget([
        'dataProvider'=>$dataProviderPeminjamanDetail,
        //'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'permintaan_id',
//            'jumlah',
//            'satuan',
//            [
//                'attribute' => 'Nama Barang',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                        $sq=mysql_query("select * from tbl_r_barang where barang_id = $data[barang_id]");
//                        while($kk=mysql_fetch_assoc($sq))
//                        {
//                            $listnya = $listnya . $kk['nama'] . "<br>";
//                        }
//                    }
//                    return $listnya;
//                },
//            ],
//            [
//                'attribute' => 'Nama Barang',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $detail = new TblTPermintaanDetail();
//                    $barang = \frontend\models\TblRBarang::find()->where(['barang_id'=>$detail->barang_id])->one();
//                    return $barang->nama;
//                },
//            ],
              'barang.nama',
               'jumlah',
//            [
//                'attribute' => 'Jumlah',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                            $listnya = $listnya . $data['jumlah'] . "<br>";
//                    }
//                    return $listnya;
//                },
//            ],
                        
            'keterangan:ntext',
                        
            [
                'attribute' => 'Status Pinjam',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_pinjam'])->andWhere(['no' => $detail['status_pinjam']])->one();
                    return $status['nama'];
                },
            ],
                        
            [
                'attribute' => 'Status Kembali',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id' => $model->peminjaman_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_kembali'])->andWhere(['no' => $detail['status_kembali']])->one();
                    return $status['nama'];
                },
            ],
                        
            /*[
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){
                    $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();;
                    if($detail['status_pinjam'] == 2){
                    return '<div><center>' . Html::a('Serahkan Barang', ['tbl-t-peminjaman-detail/serah','id'=>$detail->peminjaman_id], ['class' => 'btn btn-success', 'data'
                    ]).'&nbsp;' . Html::a('Barang Kembali', ['tbl-t-peminjaman-detail/kembali', 'id' => $detail->peminjaman_id], ['class' => 'btn btn-success', 'data']). 
                    '</center></div>';
                    }
                    else
                    {
                        return '-';
                    }
                }
            ],*/
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            
        ],
    ]); ?>


 <?= Html::a('Cetak Bukti Peminjaman', ['tbl-t-peminjaman/get-pdf','id'=>$model->peminjaman_id], ['class' => 'btn btn-success']) ?>

</div>
