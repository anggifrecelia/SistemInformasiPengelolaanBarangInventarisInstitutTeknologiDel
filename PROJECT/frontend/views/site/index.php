<?php
use yii\helpers\Html;
// use yii\bootstrap\Modal;



/* @var $this yii\web\View */

$this->title = 'Sistem Informasi Pengelolaan Barang Inventaris';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    
    <!--    <link href="css/site.css" rel="stylesheet">-->
    <script>
        $(document).ready(function () {
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

            allWells.hide();

            navListItems.click(function (e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-indigo').addClass('btn-default');
                    $item.addClass('btn-indigo');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allPrevBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepSteps = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

                prevStepSteps.removeAttr('disabled').trigger('click');
            });

            allNextBtn.click(function(){
                var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

                $(".form-group").removeClass("has-error");
                for(var i=0; i< curInputs.length; i++){
                    if (!curInputs[i].validity.valid){
                        isValid = false;
                        $(curInputs[i]).closest(".form-group").addClass("has-error");
                    }
                }

                if (isValid)
                    nextStepWizard.removeAttr('disabled').trigger('click');
            });

            $('div.setup-panel div a.btn-indigo').trigger('click');
        });
    </script>
    <style>

        .carousel-contain.col-md-3.left-col {
            padding-right:0px;
        }
        .carousel-contain.col-md-6 {
            padding:0px;
        }
        .carousel-contain.col-md-3.right-col {
            padding-left:0px;
        }

        .steps-form {
            display: table;
            width: 100%;
            position: relative; }
        .steps-form .steps-row {
            display: table-row; }
        .steps-form .steps-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc; }
        .steps-form .steps-row .steps-step {
            display: table-cell;
            text-align: center;
            position: relative; }
        .steps-form .steps-row .steps-step p {
            margin-top: 0.5rem; }
        .steps-form .steps-row .steps-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important; }
        .steps-form .steps-row .steps-step .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
            margin-top: 0; }

        .card {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0, 0, 0, 0.125);
            border-radius: 0.25rem;
        }

        .card > hr {
            margin-right: 0;
            margin-left: 0;
        }

        .card > .list-group:first-child .list-group-item:first-child {
            border-top-left-radius: 0.25rem;
            border-top-right-radius: 0.25rem;
        }

        .card > .list-group:last-child .list-group-item:last-child {
            border-bottom-right-radius: 0.25rem;
            border-bottom-left-radius: 0.25rem;
        }

        .card-body {
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            padding: 1.25rem;
        }

        .card-title {
            margin-bottom: 0.75rem;
        }

        .card-subtitle {
            margin-top: -0.375rem;
            margin-bottom: 0;
        }

        .card-text:last-child {
            margin-bottom: 0;
        }

        .card-link:hover {
            text-decoration: none;
        }

        .card-link + .card-link {
            margin-left: 1.25rem;
        }

        .card-header {
            padding: 0.75rem 1.25rem;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header:first-child {
            border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
        }

        .card-header + .list-group .list-group-item:first-child {
            border-top: 0;
        }

        .card-footer {
            padding: 0.75rem 1.25rem;
            background-color: rgba(0, 0, 0, 0.03);
            border-top: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-footer:last-child {
            border-radius: 0 0 calc(0.25rem - 1px) calc(0.25rem - 1px);
        }

        .card-header-tabs {
            margin-right: -0.625rem;
            margin-bottom: -0.75rem;
            margin-left: -0.625rem;
            border-bottom: 0;
        }

        .card-header-pills {
            margin-right: -0.625rem;
            margin-left: -0.625rem;
        }

        .card-img-overlay {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 1.25rem;
        }

        .card-img {
            width: 100%;
            border-radius: calc(0.25rem - 1px);
        }

        .card-img-top {
            width: 100%;
            border-top-left-radius: calc(0.25rem - 1px);
            border-top-right-radius: calc(0.25rem - 1px);
        }

        .card-img-bottom {
            width: 100%;
            border-bottom-right-radius: calc(0.25rem - 1px);
            border-bottom-left-radius: calc(0.25rem - 1px);
        }

        .card-deck {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .card-deck .card {
            margin-bottom: 15px;
        }

        @media (min-width: 576px) {
            .card-deck {
                -ms-flex-flow: row wrap;
                flex-flow: row wrap;
                margin-right: -15px;
                margin-left: -15px;
            }
            .card-deck .card {
                display: -ms-flexbox;
                display: flex;
                -ms-flex: 1 0 0%;
                flex: 1 0 0%;
                -ms-flex-direction: column;
                flex-direction: column;
                margin-right: 15px;
                margin-bottom: 0;
                margin-left: 15px;
            }
        }

        .card-group {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .card-group > .card {
            margin-bottom: 15px;
        }

        @media (min-width: 576px) {
            .card-group {
                -ms-flex-flow: row wrap;
                flex-flow: row wrap;
            }
            .card-group > .card {
                -ms-flex: 1 0 0%;
                flex: 1 0 0%;
                margin-bottom: 0;
            }
            .card-group > .card + .card {
                margin-left: 0;
                border-left: 0;
            }
            .card-group > .card:first-child {
                border-top-right-radius: 0;
                border-bottom-right-radius: 0;
            }
            .card-group > .card:first-child .card-img-top,
            .card-group > .card:first-child .card-header {
                border-top-right-radius: 0;
            }
            .card-group > .card:first-child .card-img-bottom,
            .card-group > .card:first-child .card-footer {
                border-bottom-right-radius: 0;
            }
            .card-group > .card:last-child {
                border-top-left-radius: 0;
                border-bottom-left-radius: 0;
            }
            .card-group > .card:last-child .card-img-top,
            .card-group > .card:last-child .card-header {
                border-top-left-radius: 0;
            }
            .card-group > .card:last-child .card-img-bottom,
            .card-group > .card:last-child .card-footer {
                border-bottom-left-radius: 0;
            }
            .card-group > .card:only-child {
                border-radius: 0.25rem;
            }
            .card-group > .card:only-child .card-img-top,
            .card-group > .card:only-child .card-header {
                border-top-left-radius: 0.25rem;
                border-top-right-radius: 0.25rem;
            }
            .card-group > .card:only-child .card-img-bottom,
            .card-group > .card:only-child .card-footer {
                border-bottom-right-radius: 0.25rem;
                border-bottom-left-radius: 0.25rem;
            }
            .card-group > .card:not(:first-child):not(:last-child):not(:only-child) {
                border-radius: 0;
            }
            .card-group > .card:not(:first-child):not(:last-child):not(:only-child) .card-img-top,
            .card-group > .card:not(:first-child):not(:last-child):not(:only-child) .card-img-bottom,
            .card-group > .card:not(:first-child):not(:last-child):not(:only-child) .card-header,
            .card-group > .card:not(:first-child):not(:last-child):not(:only-child) .card-footer {
                border-radius: 0;
            }
        }

        .card-columns .card {
            margin-bottom: 0.75rem;
        }

        @media (min-width: 576px) {
            .card-columns {
                -webkit-column-count: 3;
                -moz-column-count: 3;
                column-count: 3;
                -webkit-column-gap: 1.25rem;
                -moz-column-gap: 1.25rem;
                column-gap: 1.25rem;
                orphans: 1;
                widows: 1;
            }
            .card-columns .card {
                display: inline-block;
                width: 100%;
            }
        }

        .accordion .card:not(:first-of-type):not(:last-of-type) {
            border-bottom: 0;
            border-radius: 0;
        }

        .accordion .card:not(:first-of-type) .card-header:first-child {
            border-radius: 0;
        }

        .accordion .card:first-of-type {
            border-bottom: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .accordion .card:last-of-type {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .btn btn-default btn-circle{
            
        }
    </style>

</head>


<body>

<div class="container">
    <h2>Institut Teknologi Del</h2>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img src="img/banner1.jpg" alt="Los Angeles" style="width:100%;">
            </div>

            <div class="item">
                <img src="img/baru3.jpg" alt="Chicago" style="width:100%;">
            </div>

            <div class="item">
                <img src="img/baru2.jpg" alt="New york" style="width:100%;">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<br> <br>



<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="bg-success">
                <!-- Steps form -->
                <div class="card">
                    <div class="card-body mb-4">

                        <h2 class="text-center font-weight-bold pt-4 pb-5"><strong>Prosedur Peminjaman Barang</strong></h2>

                        <!-- Stepper -->
                        <div class="steps-form">
                            <div class="steps-row setup-panel">
                                <div class="steps-step">
                                    <a href="#step-9" type="button" class="btn btn-default btn-circle">1</a>
                                    <p>Langkah 1</p>
                                </div>
                                <div class="steps-step">
                                    <a href="#step-10" type="button" class= "btn btn-default btn-circle">2</a>
                                    <p>Langkah 2</p>
                                </div>
                                <div class="steps-step">
                                    <a href="#step-11" type="button" class="btn btn-default btn-circle" >3</a>
                                    <p>Langkah 3</p>
                                </div>
                            </div>
                        </div>

                        <form role="form" action="" method="post">

                            <!-- First Step -->
                            <div class="row setup-content" id="step-9">
                                <div class="col-md-12">
                                    <h3 class="font-weight-bold pl-0 my-4"><strong>1. Login</strong></h3>
                                    <div class="form-group md-form">
                                        <label for="yourName" data-error="wrong" data-success="right">username</label>
                                        <input id="yourName" type="text" required="required" class="form-control validate">
                                    </div>
                                    <div class="form-group md-form mt-3">
                                        <label for="yourLastName" data-error="wrong" data-success="right">password</label>
                                        <input id="yourLastName" type="text" required="required" class="form-control validate">
                                    </div>

                                    <button class="btn btn-indigo btn-rounded nextBtn float-right" type="button">Next</button>
                                </div>
                            </div>

                            <!-- Second Step -->
                            <div class="row setup-content" id="step-10">
                                <div class="col-md-12">
                                    <h3 class="font-weight-bold pl-0 my-4"><strong>2. Mengisi Form Peminjaman Barang</strong></h3>
                                    <div class="form-group md-form">
                                        <label for="companyName" data-error="wrong" data-success="right">Tanggal Peminjaman</label>
                                        <input id="companyName" type="text" required="required" class="form-control validate">
                                    </div>
                                    <div class="form-group md-form mt-3">
                                        <label for="companyAddress" data-error="wrong" data-success="right">Rencana Waktu Kembali</label>
                                        <input id="companyAddress" type="text" required="required" class="form-control validate">
                                    </div>
                                    <div class="form-group md-form">
                                        <label for="companyName" data-error="wrong" data-success="right">Barang</label>
                                        <input id="companyName" type="text" required="required" class="form-control validate">
                                    </div>
                                    <button class="btn btn-indigo btn-rounded nextBtn float-right" type="button">Create</button>
                                </div>
                            </div>

                            <!-- Third Step -->
                            <div class="row setup-content" id="step-11">
                                <div class="col-md-12">
                                    <h3 class="font-weight-bold pl-0 my-4"><strong>3. Menunggu konfirmasi peminjaman dari Pihak Inventory</strong></h3>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <!-- Steps form -->

            </div>
        </div>
        <div class="col-md-6">

            <!-- Steps form -->
            <div class="card">
                <div class="card-body mb-4">

                    <h2 class="text-center font-weight-bold pt-4 pb-5"><strong>Prosedur Permintaan Barang</strong></h2>

                    <!-- Stepper -->
                    <div class="steps-form">
                        <div class="steps-row setup-panel">
                            <div class="steps-step">
                                <a href="#step-9" type="button" class="btn btn-default btn-circle">1</a>
                                <p>Langkah 1</p>
                            </div>
                            <div class="steps-step">
                                <a href="#step-10" type="button" class="btn btn-default btn-circle">2</a>
                                <p>Langkah 2</p>
                            </div>
                            <div class="steps-step">
                                <a href="#step-11" type="button" class="btn btn-default btn-circle">3</a>
                                <p>Langkah 3</p>
                            </div>
                        </div>
                    </div>

                    <form role="form" action="" method="post">

                        <!-- First Step -->
                        <div class="row setup-content" id="step-9">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>1. Login</strong></h3>
                            
                                <div class="form-group md-form mt-3">
                                    <label for="yourLastName" data-error="wrong" data-success="right">username </label>
                                    <input id="yourLastName" type="text" required="required" class="form-control validate">
                                </div>
                                <div class="form-group md-form mt-3">
                                    <label for="yourLastName" data-error="wrong" data-success="right">password</label>
                                    <input id="yourLastName" type="text" required="required" class="form-control validate">
                                </div>
                                
                                
                                <button class="btn btn-indigo btn-rounded nextBtn float-right" type="button">Next</button>
                            </div>
                        </div>

                        <!-- Second Step -->
                        <div class="row setup-content" id="step-10">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>2. Mengisi Form Permintaan Barang</strong></h3>
                                <div class="form-group md-form">
                                    <label for="companyName" data-error="wrong" data-success="right">Tanggal Permintaan</label>
                                    <input id="companyName" type="text" required="required" class="form-control validate">
                                </div>
                                <div class="form-group md-form mt-3">
                                    <label for="companyAddress" data-error="wrong" data-success="right">Keterangan</label>
                                    <input id="companyAddress" type="text" required="required" class="form-control validate">
                                </div>
                                 <div class="form-group md-form mt-3">
                                    <label for="companyAddress" data-error="wrong" data-success="right">Barang  </label>
                                    <input id="companyAddress" type="text" required="required" class="form-control validate">
                                </div>
                                <button class="btn btn-indigo btn-rounded prevBtn float-left" type="button">Create</button>
                                                            </div>
                        </div>

                        <!-- Third Step -->
                        <div class="row setup-content" id="step-11">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>3. Menunggu konfirmasi Permintaan dari Pihak Inventory</strong></h3>
                              
                            </div>
                        </div>

                    </form>

                </div>
            </div>
            <!-- Steps form -->

        </div>
    </div>
</div>

</div>
</div>
</div>



</body>
</html>


