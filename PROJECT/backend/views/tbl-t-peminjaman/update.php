<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPeminjaman */

$this->title = 'Update Peminjaman: ' . $model->peminjaman_id;
$this->params['breadcrumbs'][] = ['label' => 'Peminjaman', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->peminjaman_id, 'url' => ['view', 'id' => $model->peminjaman_id]];
$this->params['breadcrumbs'][] = 'Penolakan Request Peminjaman';
?>

<div class="tbl-tpeminjaman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
