<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Status;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>Please fill out the following fields to register.</p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?= $form->field($model, 'nama') ?>
                <?= $form->field($model, 'email') ?>
                <?=$form->field($model, 'divisi')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'divisi')")->all(),'no', 'nama'),['prompt'=>'-Pilih Divisi Anda-'])?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'repassword')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>