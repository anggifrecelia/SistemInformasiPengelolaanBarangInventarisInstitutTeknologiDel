<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTStokbarang */

$this->title = 'Update Stok Barang: ' . ' ' . $model->stok_id;
$this->params['breadcrumbs'][] = ['label' => 'Stok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->stok_id, 'url' => ['view', 'id' => $model->stok_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tstokbarang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
