<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_pencatatan_aset".
 *
 * @property integer $pencatatan_id
 * @property integer $barang_id
 * @property integer $gedung_id
 * @property string $kode_trans
 * @property string $tanggal_trans
 * @property integer $jumlah
 * @property string $satuan
 * @property string $keterangan
 * @property integer $sumber_perolehan
 * @property integer $kondisi
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRBarang $barang
 * @property TblRGedung $gedung
 */
class TblTPencatatanAset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_pencatatan_aset';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang_id', 'gedung_id', 'jumlah', 'sumber_perolehan', 'kondisi', 'created_by', 'modified_by'], 'integer'],
            [['gedung_id', 'barang_id', 'jumlah', 'sumber_perolehan', 'kondisi', 'kode_trans', 'satuan'], 'required'],
            [['tanggal_trans', 'created_date', 'modified_date'], 'safe'],
            [['keterangan'], 'string'],
            [['kode_trans'], 'string', 'max' => 30],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
            [['gedung_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRGedung::className(), 'targetAttribute' => ['gedung_id' => 'gedung_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pencatatan_id' => 'ID',
            'barang_id' => 'Nama Barang',
            'gedung_id' => 'Gedung',
            'kode_trans' => 'Kode Transaksi',
            'tanggal_trans' => 'Tanggal Transaksi',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'sumber_perolehan' => 'Sumber Perolehan',
            'kondisi' => 'Kondisi',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGedung()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'gedung_id']);
    }
}
