<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPeminjamanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpeminjaman-search" style="width:300px">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pemohon')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'divisi')")->all(),'no', 'nama'),['prompt'=>'-Pilih divisi-', 'style'=>'width:300px'])?>

    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>
    
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
