<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTPengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Tpengadaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Tpengadaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pengadaan_id',
            'tipe',
            'kode_trans',
            'tanggal_trans',
            'bulan_trans',
            // 'tahun_trans',
            // 'keterangan:ntext',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
