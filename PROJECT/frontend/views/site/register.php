<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Status;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;
use yii\captcha\Captcha;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;

?>
<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" >
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />

    <style type="text/css">
        body {
            font-family: 'Varela Round', sans-serif;
        }
        .modal-login {
            color: #636363;
            width: 350px;
        }
        .modal-login .modal-content {
            padding: 20px;
            border-radius: 5px;
            border: none;
        }
        .modal-login .modal-header {
            border-bottom: none;
            position: relative;
            justify-content: center;
        }
        .modal-login h4 {
            text-align: center;
            font-size: 26px;
            margin: 30px 0 -15px;
        }
        .modal-login .form-control:focus {
            border-color: #70c5c0;
        }
        .modal-login .form-control, .modal-login .btn {
            min-height: 40px;
            border-radius: 3px;
        }
        .modal-login .close {
            position: absolute;
            top: -5px;
            right: -5px;
        }
        .modal-login .modal-footer {
            background: #ecf0f1;
            border-color: #dee4e7;
            text-align: center;
            justify-content: center;
            margin: 0 -20px -20px;
            border-radius: 5px;
            font-size: 13px;
        }
        .modal-login .modal-footer a {
            color: #999;
        }
        .modal-login .avatar {
            position: absolute;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: -70px;
            width: 95px;
            height: 95px;
            border-radius: 50%;
            z-index: 9;
            background: #fff;
            padding: 15px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
        }
        .modal-login .avatar img {
            width: 100%;
        }
        .modal-login.modal-dialog {
            margin-top: 80px;
        }
        .modal-login .btn {
            color: #fff;
            border-radius: 4px;
            background: #60c7c1;
            text-decoration: none;
            transition: all 0.4s;
            line-height: normal;
            border: none;
        }
        .modal-login .btn:hover, .modal-login .btn:focus {
            background: #45aba6;
            outline: none;
        }
        .trigger-btn {
            display: inline-block;
            margin: 100px auto;
        }
    </style>
</head>
<body>


<div class="modal-dialog modal-login">
    <div class="modal-content">
        <div class="modal-header">
            <div class="avatar">
                <img src="img/logo.jpg" alt="Avatar">
            </div>
            <div class="site-signup">
                <h4 class="modal-title"><?= Html::encode($this->title) ?></h4> <br>
                <div class="modal-body">

                    <?php $form = ActiveForm::begin(['id' => 'form-signup',
                    ]); ?>
                    <?= $form->field($model, 'nama') ?>
                    <?= $form->field($model, 'email') ?>
                    <?=$form->field($model, 'divisi')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(nama = 'Mahasiswa')")->orWhere("(nama = 'Dosen/Staf')")->all(),'no', 'nama'),['prompt'=>'-Pilih Divisi Anda-'])?>
                    <?= $form->field($model, 'username') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'repassword')->passwordInput() ?>
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'imageOptions' => [
                            'id' => 'my-captcha-image'],
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div></div>
                        <div class="row"><div class="col-lg-8">{input}</div><button class="glyphicon glyphicon-refresh" id="refresh-captcha"></div>',]); ?>
                    <?php $this->registerJs("$('#refresh-captcha').on('click', function(e){
                e.preventDefault();
                $('#my-captcha-image').yiiCaptcha('refresh');})"); ?>
                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-lg btn-block login-btn', 'name' => 'signup-button']) ?>

                    <br/>
                    <div class="form-group">
                    </div>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
</div>
</body>