<?php


use yii\grid\GridView;
use backend\models\User;
use yii\helpers\Html;
use backend\models\TblRBarang;

?>

<h2 class="text-center"><u><b>Laporan Bulan <?= $namaBulan . ' ' . $tahun ?></b></u></h2>

<!--<h3><b>Pelanggaran Piket Makan</b></h3>

<h4><b>Daftar Pelanggar</b></h4>-->
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => 'No.'],
//        'peminjaman_id',
        // 'no_dokumen',
 //       'pemohon',
        [
            'attribute' => 'Pemohon',
          'format' => 'raw',
          'value' => function ($model) {
              $pemohon = User::find()->where(['id' => $model['pemohon']])->one();
              return $pemohon->nama;
           },
              ],
                'tanggal_pinjam',
                'rencana_kembali',
                // 'approval',
                [
                    'attribute' => 'Barang',
          'format' => 'raw',
          'value' => function ($model) {
              $barang = TblRBarang::find()->where(['barang_id' => $model['barang_id']])->one();
              return $barang->nama;
           },
                ],

              [
                'attribute' => 'Disetujui Oleh',
                'format' => 'raw',
                 'value' => function ($model) {
                   $approval = User::find()->where(['id' => $model['approval']])->one();
                    return $approval->nama;
               },
                    ],
//                [
//                    'attribute' => 'Action',
//                    'format' => 'raw',
//                    'value' => function($model){
//                        return '<div><center>' . Html::a('Detail Peminjaman', ['detailview','id'=>$model['peminjaman_id']], ['class' => 'btn btn-success', 'data'
//                        ]);
//                    }
//                ],
                    ],
                    'summary' => '',
                ]);
                ?>
