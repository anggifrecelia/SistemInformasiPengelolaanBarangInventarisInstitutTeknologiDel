<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPermintaanDetail */

$this->title = 'Update Tbl Tpermintaan Detail: ' . ' ' . $model->permintaan_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpermintaan Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->permintaan_detail_id, 'url' => ['view', 'id' => $model->permintaan_detail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpermintaan-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
