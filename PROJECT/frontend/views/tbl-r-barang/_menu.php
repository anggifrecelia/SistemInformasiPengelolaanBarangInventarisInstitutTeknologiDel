<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
    ],
    'items' => [
        [
            'label'   => Yii::t('app', 'Barang'),
            'url'     => ['/tbl-r-barang/index'],
        ],
        [
            'label'   => Yii::t('app', 'Pengadaan Barang'),
            'url'     => ['/tbl-t-pengadaan/index'],
        ],
        [
            'label'   => Yii::t('app', 'Pengeluaran Barang'),
            'url'     => ['/tbl-t-pengeluaran/index'],
        ],
        [
            'label' => Yii::t('app','Stok Barang'),
            'url'     => ['/tbl-t-stokbarang/index'],
        ],
    ],
]) ?>
