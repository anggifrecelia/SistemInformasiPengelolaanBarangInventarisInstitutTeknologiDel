<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblRGedungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gedung';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rgedung-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Gedung', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'gedung_id',
            [
                'attribute' => 'Tipe Gedung',
                'format' => 'raw',
                'value' => function ($model) {
                    $tipe = \frontend\models\TblRStatus::find()->where(['kode'=>'tipe_gedung', 'no'=>$model->tipe])->one();
                    return $tipe->nama;
                },
            ],
            'nama',
            [
                'attribute' => 'Kelompok Gedung',
                'format' => 'raw',
                'value' => function ($model) {
                    $kelompok = \frontend\models\TblRStatus::find()->where(['kode'=>'kelompok_gedung', 'no'=>$model->kelompok])->one();
                    return $kelompok->nama;
                },
            ],
            'deskripsi',
//            [
//                'format' => 'raw',
//                'value' => function($model){
//                    return '<div><center>' . Html::a('Delete', ['delete', 'id' => $model->barang_id], ['class' => 'btn btn-danger', 'data' => [
//                        'confirm' => 'Are you sure you want to delete this item?',
//                        'method' => 'post',
//                    ],
//                    ]). '</center></div>';
//                },
//            ],
            // 'deleted',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            
            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '30'],
                'template' => '{delete}{link}',
            ],
        ],
    ]); ?>

</div>
