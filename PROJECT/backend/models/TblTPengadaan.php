<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_pengadaan".
 *
 * @property string $pengadaan_id
 * @property integer $tipe
 * @property string $kode_trans
 * @property string $tanggal_trans
 * @property integer $bulan_trans
 * @property string $tahun_trans
 * @property string $keterangan
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTPengadaanDetail[] $tblTPengadaanDetails
 */
class TblTPengadaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_pengadaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_trans', 'tanggal_trans', 'bulan_trans', 'tahun_trans', 'keterangan', 'created_date', 'created_by'], 'required'],
            [['tanggal_trans', 'tahun_trans', 'created_date', 'modified_date'], 'safe'],
            [['bulan_trans', 'created_by', 'modified_by'], 'integer'],
            [['keterangan'], 'string'],
            [['tipe'], 'string', 'max' => 2],
            [['kode_trans'], 'string', 'max' => 30],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pengadaan_id' => 'Pengadaan ID',
            'tipe' => 'Tipe',
            'kode_trans' => 'Kode Pengadaan',
            'tanggal_trans' => 'Tanggal Pengadaan',
            'bulan_trans' => 'Bulan Trans',
            'tahun_trans' => 'Tahun Trans',
            'keterangan' => 'Keterangan',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPengadaanDetails()
    {
        return $this->hasMany(TblTPengadaanDetail::className(), ['pengadaan_id' => 'pengadaan_id']);
    }
}
