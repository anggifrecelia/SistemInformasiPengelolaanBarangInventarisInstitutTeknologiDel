<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblRKerusakan */

$this->title = 'Tambahkan Buku Inventory';
$this->params['breadcrumbs'][] = ['label' => 'Buku Inventory', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rkerusakan-create">

    <!--h1><!--?= Html::encode($this->title) ?></h1-->

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
