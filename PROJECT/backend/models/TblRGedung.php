<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_r_gedung".
 *
 * @property integer $gedung_id
 * @property integer $tipe
 * @property string $nama
 * @property string $tahun_pembuatan
 * @property string $usia
 * @property string $kelompok
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRKerusakan[] $tblRKerusakans
 * @property TblTBukuinventory[] $tblTBukuinventories
 * @property TblTPelepasanAset[] $tblTPelepasanAsets
 * @property TblTPencatatanAset[] $tblTPencatatanAsets
 */
class TblRGedung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_r_gedung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe', 'nama', 'kelompok', 'deskripsi', 'created_date', 'created_by'], 'required'],
            [['tipe', 'created_by', 'modified_by','gedung_id'], 'integer'],
            [['tahun_pembuatan', 'created_date', 'modified_date'], 'safe'],
            [['nama'], 'string', 'max' => 50],
            [['usia'], 'string', 'max' => 30],
            [['kelompok'], 'string', 'max' => 100],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gedung_id' => 'Gedung ID',
            'tipe' => 'Tipe',
            'nama' => 'Nama',
            'tahun_pembuatan' => 'Tahun Pembuatan',
            'usia' => 'Usia',
            'kelompok' => 'Kelompok',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRKerusakans()
    {
        return $this->hasMany(TblRKerusakan::className(), ['gedung_id' => 'gedung_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTBukuinventories()
    {
        return $this->hasMany(TblTBukuinventory::className(), ['gedung_id' => 'gedung_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPelepasanAsets()
    {
        return $this->hasMany(TblTPelepasanAset::className(), ['gedung_id' => 'gedung_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPencatatanAsets()
    {
        return $this->hasMany(TblTPencatatanAset::className(), ['gedung_id' => 'gedung_id']);
    }
}
