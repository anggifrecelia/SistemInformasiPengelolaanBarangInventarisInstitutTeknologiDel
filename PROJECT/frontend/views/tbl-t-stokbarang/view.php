<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;






$mei2=$model->stok_id;

// var_dump($mei2); die();
 $mei = (new \yii\db\Query())
                    ->select ('barang_id')
                    ->from('tbl_t_stokbarang')
                    ->where(['stok_id' => $mei2])
                    ->scalar();

 $namaBarang = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('tbl_r_barang')
                    ->where(['barang_id' => $mei])
                    ->scalar();
                    // var_dump($namaBarang);die();










/* @var $this yii\web\View */
/* @var $model frontend\models\TblTStokbarang */

$this->title = $namaBarang;
$this->params['breadcrumbs'][] = ['label' => 'Stok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tstokbarang-view">

    <h1><?= Html::encode($this->title) ?></h1>

   <!--  <p>
        <?= Html::a('Update', ['update', 'id' => $model->stok_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->stok_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stok_id',
            'barang_id',
            'gedung_id',
            'kode',
            'jumlah',
            'satuan',
            'deskripsi',
            // 'deleted',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
        ],
    ]) 




    ?>


<p>
    
   <?= Html::button('Minta Sekarang', ['value'=>Url::to('index.php?r=tbl-t-permintaan/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>


<?php
        Modal::begin([
                'header'=>'<h4>Permintaan Barang Habis</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>



</p>


</div>
