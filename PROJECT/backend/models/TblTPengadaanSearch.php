<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTPengadaan;

/**
 * TblTPengadaanSearch represents the model behind the search form about `backend\models\TblTPengadaan`.
 */
class TblTPengadaanSearch extends TblTPengadaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_id', 'bulan_trans', 'created_by', 'modified_by'], 'integer'],
            [['kode_trans', 'tanggal_trans', 'tahun_trans', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPengadaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pengadaan_id' => $this->pengadaan_id,
            // 'tipe' => $this->tipe,
            'tanggal_trans' => $this->tanggal_trans,
            'bulan_trans' => $this->bulan_trans,
            'tahun_trans' => $this->tahun_trans,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'kode_trans', $this->kode_trans])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
