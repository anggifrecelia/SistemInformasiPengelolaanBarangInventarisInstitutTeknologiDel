<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPengadaan */

$this->title = 'Detail Pengadaan Barang'; //$model->pengadaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-view">

    

<!--    <p>
        ?= Html::a('Update', ['update', 'id' => $model->pengadaan_id], ['class' => 'btn btn-primary']) ?>
        ?= Html::a('Delete', ['delete', 'id' => $model->pengadaan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //pengadaan_id',
           // 'tipe',
            [
                'label'  => 'Tipe',
                'value' => $model->tipe == 1 ? 'Belanja Rutin' : '',
            ],
            'kode_trans',
            'tanggal_trans',
//            'satuan'
//            'bulan_trans',
//            'tahun_trans',
//            'keterangan:ntext',
//            'created_date',
//            'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>

    <p><h2>Detail Pengadaan Barang</h2></p>
  <?=\yii\grid\GridView::widget([
        'dataProvider' => $dataProviderPengadaanDetail,
        'columns' => [
//            'pengadaan_detail_id',
//            'pengadaan_id',
//             [
//                'attribute' => 'Nama Barang',
//                'value' => function($model) {
//                    $id_bar= Yii::$app->db->createCommand('SELECT pengadaan_id FROM tbl_t_pengadaan_detail where pengadaan_id=' . $model->pengadaan_id)->queryScalar();
//
//                    $id_barang = Yii::$app->db->createCommand('SELECT barang_id FROM tbl_t_pengadaan_detail where pengadaan_id = ' . $id_bar)->queryScalar();
//
//                    return Yii::$app->db->createCommand('SELECT nama FROM tbl_r_barang where barang_id = ' . $id_barang)->queryScalar();
//                }
//            ],
            'barang.nama',
            'jumlah',
//            'satuan',
             [
                'label'  => 'Satuan',
                'value' => function ($models) {
                    switch ($models->satuan) {
                        case 2:
                            $satuan = 'Unit';
                            break;

                        case 3:
                            $satuan = 'Botol';
                            break;
                        
                        case 4:
                            $satuan = 'Bungkus';
                            break;

                        case 5:
                            $satuan = 'Kotak';
                            break;

                        case 6:
                            $satuan = 'Peal';
                            break;

                        case 7:
                            $satuan = 'Bal';
                            break;
                    }
                    return $satuan;
                }
            ],
                    
            'keterangan',
//            'created_date',
//            'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>
    
</div>
