<?php

use yii\helpers\Html;
use frontend\models\TblRBarang;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTBukuInventory */

$aset = TblRBarang::findOne(['barang_id'=>$model->barang_id]);

$this->title = 'Laporkan Kerusakan Aset : ' . $aset->nama;
$this->params['breadcrumbs'][] = ['label' => 'Buku Inventory', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->buku_id, 'url' => ['view', 'id' => $model->buku_id]];
$this->params['breadcrumbs'][] = 'Lapor Kerusakan';
?>
<div class="tbl-tbuku-inventory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_1', [
        'model' => $model,
    ]) ?>

</div>
