<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPermintaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpermintaan-search" style="width:300px">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!--?= $form->field($model, 'pemohon')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'divisi')")->all(),'no', 'nama'),['prompt'=>'-Pilih divisi-', 'style'=>'width:300px'])?-->

    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>

    <!--?= $form->field($model, 'bulan_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'bulan')")->all(),'no', 'nama'),['prompt'=>'-Pilih Bulan-', 'style'=>'width:300px'])?-->

    <?php // echo $form->field($model, 'tahun_trans') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
