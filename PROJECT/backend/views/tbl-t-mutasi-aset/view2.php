<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblTMutasiAset;
use backend\models\TblTPencatatanAset;
use backend\models\TblRBarang;
use backend\models\TblRGedung;


$mei = new TblRBarang();


$PencatatanId = (new \yii\db\Query())
    ->select ('pencatatan_id')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$StatusMutasi = (new \yii\db\Query())
    ->select ('status_mutasi')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$BarangId = (new \yii\db\Query())
    ->select ('barang_id')
    ->from('tbl_t_pencatatan_aset')
    ->where(['pencatatan_id' => $PencatatanId])
    ->scalar();

$BarangName = (new \yii\db\Query())
    ->select ('nama')
    ->from('tbl_r_barang')
    ->where(['barang_id' => $BarangId])
    ->scalar();


// $gedungs = TblRGedung::find()->where(['gedung_id' => $gedungs->])

// var_dump($BarangName);die();
/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAset */

$this->title =$BarangName;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tmutasi Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'mutasi_id',

            // [
            //     'attribute' => 'Id Pencatatan',
            //     'value' => $model->pencatatan->pencatatan_id,
            // ],

            [
                'attribute' => 'Barang',
                'value' => $model->pencatatan->keterangan,
            ],

            'kode_trans',

            'tanggal_trans',

            //  [
            //     'attribute' => 'Lokasi Awal',
            //     'value' => $model->gedung->nama,
            // ],


            // 'lokasi_tujuan',
            [
                'attribute' => 'Lokasi Tujuan',
                'value' => $model->gedung2->nama,
            ],

            'jumlah',

            'alasan_mutasi',

          'status_mutasi',



            'status_pindah',

            'created_date',
            [
                'attribute' => 'Created By',
                'value' => $model->createdBy->nama,
            ],

            ],
    ])

    ?>
<br>
<br>
    <h4> Penanggung Jawab </h4>
<br>
<br>

<h4> (Suci Monika) <h4>
</div>
