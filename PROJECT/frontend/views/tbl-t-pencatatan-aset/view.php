<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPencatatanAset */

$this->title = $model->pencatatan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpencatatan Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pencatatan_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pencatatan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pencatatan_id',
            'barang_id',
            'gedung_id',
            'kode_trans',
            'tanggal_trans',
            'jumlah',
            'satuan',
            'keterangan:ntext',
            'sumber_perolehan',
            'kondisi',
            'created_date',
            'created_by',
            'created_ip',
            'modified_date',
            'modified_by',
            'modified_ip',
        ],
    ]) ?>

</div>
