<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $kode_status
 * @property string $detailStatus
 * @property string $keterangan
 *
 * @property Di[] $dis
 * @property Di[] $dis0
 * @property Di[] $dis1
 * @property Dii[] $diis
 * @property Dii[] $diis0
 * @property User[] $users
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_status'], 'integer'],
            [['nama', 'detailStatus', 'keterangan'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'kode_status' => 'Kode Status',
            'detailStatus' => 'Detail Status',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDis()
    {
        return $this->hasMany(Di::className(), ['status' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDis0()
    {
        return $this->hasMany(Di::className(), ['kondisi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDis1()
    {
        return $this->hasMany(Di::className(), ['lokasi' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiis()
    {
        return $this->hasMany(Dii::className(), ['kategori' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiis0()
    {
        return $this->hasMany(Dii::className(), ['jenis' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['divisi' => 'id']);
    }
}
