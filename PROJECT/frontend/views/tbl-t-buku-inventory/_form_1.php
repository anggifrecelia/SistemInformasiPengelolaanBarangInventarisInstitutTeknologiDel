<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TblRBarang;
use frontend\models\TblRGedung;
use frontend\models\TblRStatus;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblTBukuInventory */
/* @var $form yii\widgets\ActiveForm */

$barang = TblRBarang::findOne(['barang_id'=>$model->barang_id]);
$gedung = TblRGedung::findOne(['gedung_id'=>$model->gedung_id]);
?>

<div class="tbl-tbuku-inventory-form">
    <?= '<h4>Nama Barang        : '.$barang->nama .'</h4>'?>
    <?= '<h4>Gedung             : ' .$gedung->nama.'</h4>' ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rusak')->textInput() ?>

    <?= $form->field($model, 'deskripsi')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
