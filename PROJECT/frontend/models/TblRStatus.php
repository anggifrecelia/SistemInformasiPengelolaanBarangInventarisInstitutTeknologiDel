<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_r_status".
 *
 * @property integer $status_id
 * @property string $kode
 * @property integer $no
 * @property string $nama
 * @property string $grup
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 */
class TblRStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_r_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'no', 'nama'], 'required'],
            [['no', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['kode', 'nama', 'grup'], 'string', 'max' => 50],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'kode' => 'Kode',
            'no' => 'No',
            'nama' => 'Nama',
            'grup' => 'Grup',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }
}
