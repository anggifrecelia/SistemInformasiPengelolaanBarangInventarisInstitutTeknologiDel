<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTPeminjamanDetail;
use frontend\models\TblTPeminjamanSearch;
use frontend\models\TblTPeminjamanDetailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TblTPeminjamanDetailController implements the CRUD actions for TblTPeminjamanDetail model.
 */
class TblTPeminjamanDetailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionSerah($id)
    {
        $searchModel = new TblTPeminjamanDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id])->one();
        
//        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
//            'query'=>$models1TblPeminjamanDetail,
//        ]);
        
        $user = Yii::$app->user->id; 
//        $model = $this->findModel($id);
        $modelDetail = \frontend\models\TblTPeminjamanDetail::find()->where(['peminjaman_detail_id' => $id])->one();
        $modelDetail->status_kembali = 2;
        $modelDetail->status_kembali_by = $user;
        $modelDetail->save();        
        
        return $this->render('view', [
                'model' => $modelDetail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                //'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
            ]);
    }
    
    public function actionKembali($id)
    {
//        $searchModel = new TblTPeminjamanSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);
        
//        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
//            'query'=>$models1TblPeminjamanDetail,
//        ]);
        $user = Yii::$app->user->id; 
//        $model = $this->findModel($id);
        //$detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        //Yii::$app->db->createCommand("Update tbl_t_peminjaman set approval = 3, approval_by = $user where peminjaman_id = $id")->execute();
        
       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
        //$detail->status_by = Yii::$app->user->id;
        //$model->save();
        $modelDetail = \frontend\models\TblTPeminjamanDetail::find(['peminjaman_detail_id'=>$id])->one();
        $modelDetail->status_kembali = 1;
        $modelDetail->status_kembali_by = $user;
        $modelDetail->save();
        //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Disetujui'")->queryScalar();
//        foreach ($modelsDetail as $modelDetail) {
//                            //$modelDetail->peminjaman_id = $model->peminjaman_id;
//                            //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
//                            $modelDetail->status_pinjam = 3;
//                            $modelDetail->save();
//        }
        
        return $this->render('view', [
                'model' => $modelDetail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
            ]);
    }

    /**
     * Lists all TblTPeminjamanDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPeminjamanDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPeminjamanDetail model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblTPeminjamanDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTPeminjamanDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->peminjaman_detail_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTPeminjamanDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->peminjaman_detail_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPeminjamanDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTPeminjamanDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPeminjamanDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPeminjamanDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
