<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblRStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Status';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rstatus-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Status', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'status_id',
            'kode',
            'no',
            'nama',
//            [
//                'format' => 'raw',
//                'value' => function($model){
//                    return '<div><center>' . Html::a('Update', ['update', 'id' => $model->status_id], ['class' => 'btn btn-primary']).'&nbsp&nbsp'.
//                    Html::a('Delete', ['delete', 'id' => $model->status_id], ['class' => 'btn btn-danger', 'data' => [
//                        'confirm' => 'Are you sure you want to delete this item?',
//                        'method' => 'post',
//                    ],
//                    ]). '</center></div>';
//                },
//            ],
            // 'deskripsi',
            // 'deleted',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '50'],
                'template' => '{update} {delete}{link}',
            ],
        ],
    ]); ?>

</div>
