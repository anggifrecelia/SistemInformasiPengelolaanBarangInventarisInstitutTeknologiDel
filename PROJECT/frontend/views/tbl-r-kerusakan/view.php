<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblRKerusakan */

$this->title = $model->kerusakan_id;
$this->params['breadcrumbs'][] = ['label' => 'Kerusakan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rkerusakan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->kerusakan_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->kerusakan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kerusakan_id',
            'barang_id',
            'berita_acara_id',
            'no_laporan',
            'tanggal',
            'jumlah',
            'satuan',
            'keterangan:ntext',
            'created_date',
            'created_by',
            'created_ip',
            'modified_date',
            'modified_by',
            'modified_ip',
        ],
    ]) ?>

</div>
