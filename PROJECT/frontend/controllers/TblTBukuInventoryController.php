<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTBukuInventory;
use frontend\models\TblTBukuInventorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TblTBukuInventoryController implements the CRUD actions for TblTBukuInventory model.
 */
class TblTBukuInventoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTBukuInventory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTBukuInventorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single TblTBukuInventory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblTBukuInventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTBukuInventory();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        $model->deleted = '0';
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->jumlah = $model->baik + $model->rusak;
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTBukuInventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $kerusakan = new \frontend\models\TblRKerusakan();
        
        $jumlah = $model->rusak;
        if ($model->load(Yii::$app->request->post())) {
            
            //creating kerusakan instance
            $kerusakan->barang_id = $model->barang_id;
            $kerusakan->tanggal = date('Y-m-d');
            $kerusakan->jumlah = $model->rusak;
            $kerusakan->keterangan = $model->deskripsi;
            $kerusakan->satuan = $model->satuan;
            $kerusakan->created_by = Yii::$app->user->id;
            $kerusakan->created_date = date('Y-m-d');
            $kerusakan->created_ip = Yii::$app->getRequest()->getUserIP();
            $kerusakan->save();
            
            $model->baik -= $model->rusak;
            $model->rusak += $jumlah;
            $model->modified_by = Yii::$app->user->id;
            $model->modified_date = date('Y-m-d');
            $model->modified_ip = Yii::$app->getRequest()->getUserIP();
            $model->save();
            
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTBukuInventory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->deleted == '0'){
            $model->deleted = '1';
            $model->save();
        }
        
        return $this->redirect(['index']);
    }
    /**
     * Finds the TblTBukuInventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblTBukuInventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTBukuInventory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
