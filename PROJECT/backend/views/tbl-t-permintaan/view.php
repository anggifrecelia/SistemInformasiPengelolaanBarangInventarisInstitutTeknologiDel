<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\TblRBarang;
use backend\models\TblRStatus;
use backend\models\TblTPermintaanDetail;
use backend\models\User;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaan */

$this->title = $model->permintaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Permintaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$user = User::findOne(['id'=>$model->pemohon]);
?>

<h1>Peminjaman</h1>
<div class="tbl-tpermintaan-view">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

<!--    <p>
        <?= Html::a('Update', ['update', 'id' => $model->permintaan_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->permintaan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'permintaan_id',
//            'pemohon',
            [
                'label'  => 'Pemohon',
                'value' => $user->nama,
            ],
            'tanggal_trans',
           // 'bulan_trans',
           // 'tahun_trans',
            'keterangan:ntext',
           'created_date',
           // 'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>
    
    <h2 align="center">Detail Permintaan</h2>
    <?= GridView::widget([
        'dataProvider' => $dataProviderPermintaanDetail,
        //'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

           // 'permintaan_id',
           'barang.nama',
           // 'satuan',
           'jumlah',
           
//            [
//                'attribute' => 'Nama Barang',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                        $sq=mysql_query("select * from tbl_r_barang where barang_id = $data[barang_id]");
//                        while($kk=mysql_fetch_assoc($sq))
//                        {
//                            $listnya = $listnya . $kk['nama'] . "<br>";
//                        }
//                    }
//                    return $listnya;
//                },
//            ],
//            [
//                'attribute' => 'Nama Barang',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $detail = new TblTPermintaanDetail();
//                    $barang = \backend\models\TblRBarang::find()->where(['barang_id'=>$detail->barang_id])->one();
//                    return $barang->nama;
//                },
//            ],
              
               // 'jumlah',
//            [
//                'attribute' => 'Jumlah',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                            $listnya = $listnya . $data['jumlah'] . "<br>";
//                    }
//                    return $listnya;
//                },
//            ],
                        
            
                        
           
            'created_date',
            'keterangan:ntext',
             [
                'attribute' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_permintaan'])->andWhere(['no' => $detail['status']])->one();
                    return $status['nama'];
                },
            ],
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            
        ],
    ]); ?>



 <p>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
