<?php

namespace backend\controllers;

use Yii;
use backend\models\TblRStatus;
use backend\models\TblRStatusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TblRStatusController implements the CRUD actions for TblRStatus model.
 */
class TblRStatusController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblRStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblRStatusSearch(['deleted'=>0]);
        $query = TblRStatus::find()->where(['deleted'=>0]);
        $dataProvider = new \yii\data\ActiveDataProvider([
           'query'=>$query,
            'pagination'=> [
                'pageSize'=>false,
                ],
            'sort'=>[
                'defaultOrder'=>[
                    'kode'=> SORT_ASC,
                    'no' => SORT_ASC,
                ]
            ],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblRStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblRStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
   {
        $model = new TblRStatus();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d H:i:s');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        $model->deleted = '0';
        
        if ($model->load(Yii::$app->request->post())) {
            $tempStatus = TblRStatus::findOne(['kode' => $model->kode]);
            if($tempStatus){
                $noStatus = Yii::$app->db->createCommand("SELECT MAX(no) FROM `tbl_r_status` WHERE kode = '" . $model->kode ."'")->queryScalar();
                $noStatus++;
            } else {
                Yii::$app->session->setFlash('info' , 'Kode tidak terdaftar.');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->no = $noStatus;
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing TblRStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->modified_by = Yii::$app->user->id;
        $model->modified_date = date('Y-m-d H:i:s');
        $model->modified_ip = Yii::$app->getRequest()->getUserIP();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblRStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->deleted == '0'){
            $model->deleted = '1';
            $model->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblRStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblRStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblRStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
