<?php

namespace backend\controllers;

use Yii;
use backend\models\TblTStokbarang;
use backend\models\TblTStokbarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * TblTStokbarangController implements the CRUD actions for TblTStokbarang model.
 */
class TblTStokbarangController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTStokbarang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTStokbarangSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTStokbarang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionView2($id)
    {

        $modelel = new TblTStokbarang();
        $idBarang = (new \yii\db\Query())
            ->select ('barang_id')
            ->from('tbl_t_stokbarang')
            ->where(['stok_id' => $id])
            ->scalar();

        $idPermintaan = (new \yii\db\Query())
            ->select ('permintaan_id')
            ->from('tbl_t_permintaan_detail')
            ->where(['barang_id' => $idBarang])
            ->scalar();


        $permintaanJelas = (new \yii\db\Query())
            ->select ('pemohon')
            ->from('tbl_t_permintaan')
            ->where(['permintaan_id' => $idPermintaan])
            ->scalar();

        return $this->render('index2',[
            'model'=> $this->findModel($id),
            'idBarang' => $idBarang,
            'idPermintaan' => $idPermintaan,
            'permintaanJelas' => $permintaanJelas
        ]);
    }



// return $this->render('tbl-t-buku-inventory/index',[
//     'model'=> $this->findModel($id), 

//     'idBarang' => $idBarang,
//     'idPermintaan' => $idPermintaan,
//     'permintaanJelas' => $permintaanJelas
//     ]);
//     }



    /**
     * Creates a new TblTStokbarang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTStokbarang();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        $model->deleted = '0';
        if ($model->load(Yii::$app->request->post())) {
            $anggi = $model->jumlah;
            $model->available = $anggi;

            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTStokbarang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->modified_by = Yii::$app->user->id;
        $model->modified_date = date('Y-m-d');
        $model->modified_ip = Yii::$app->getRequest()->getUserIP();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->stok_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing TblTStokbarang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->deleted == '0'){
            $model->deleted = '1';
            $model->save();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTStokbarang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblTStokbarang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTStokbarang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
