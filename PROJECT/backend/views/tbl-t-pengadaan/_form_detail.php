<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use backend\models\TblTPengadaan;
use backend\models\TblTStokBarang;
// use backend\models\Barang;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaanDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpengadaan-detail-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

   

    <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'tanggal_trans')->widget(
            DatePicker::classname(), [
        'name' => 'dp_3',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => '23-Februari-1998',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ],
    ]);
    ?>

<!--    ?= $form->field($model, 'bulan_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'bulan')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Bulan-']) ?>

    ?= $form->field($model, 'tahun_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'tahun')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Tahun-']) ?>-->


    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Tambahkan Detail Barang</h4></div>
            <div class="panel-body">
                <?php
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsTblTPengadaanDetail[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'barang_id',
                        'jumlah',
                        'satuan',
                        'keterangan',
                        'created_date',
                        'created_by',
                        'created_ip',
                        'modified_date',
                        'modified_by',
                        'modified_ip',
                    ],
                ]);
                ?>

                <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelsTblTPengadaanDetail as $i => $modelsTblTPengadaanDetail): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Detail Barang</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (!$modelsTblTPengadaanDetail->isNewRecord) {
                                    echo Html::activeHiddenInput($modelsTblTPengadaanDetail, "[{$i}]id");
                                }
                                ?>

                            <div class="row">
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsTblTPengadaanDetail, "[{$i}]barang_id")->dropDownList(ArrayHelper::map(backend\models\TblTStokBarang::find()->all(), 'barang_id', 'deskripsi'), ['prompt' => '-- Select Barang --']) ?>                            
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsTblTPengadaanDetail, "[{$i}]jumlah")->textInput(['maxlength' => true]) ?>
                                    </div>  
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsTblTPengadaanDetail, "[{$i}]satuan")->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'satuan_barang')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Satuan-']) ?>
                                    </div> 
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsTblTPengadaanDetail, "[{$i}]keterangan")->textArea(['maxlength' => true]) ?>
                                    </div> 
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>



    </div>
