<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TblRBarang;
use frontend\models\TblRGedung;
use frontend\models\TblRStatus;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPencatatanAset */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpencatatan-aset-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'barang_id')->dropDownList(ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),['prompt'=>'-Pilih Nama Barang-']) ?-->

    <?= $form->field($model, 'barang_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Barang -'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    
    <!--?= $form->field($model, 'gedung_id')->dropDownList(ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),['prompt'=>'-Pilih Gedung-']) ?-->
    
    <?= $form->field($model, 'gedung_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Gedung -'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    
    <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'tanggal_trans')->textInput() ?-->
    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
    ]); ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'satuan')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'satuan_barang')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Satuan-']) ?>

    <?= $form->field($model, 'sumber_perolehan')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'sumber_perolehan')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Sumber Perolehan-']) ?>

    <?= $form->field($model, 'kondisi')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kondisi_barang')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Kondisi-']) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
    
    <!--?= $form->field($model, 'created_date')->textInput() ?>

    <!--?= $form->field($model, 'created_by')->textInput() ?>

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'modified_date')->textInput() ?>

    <!--?= $form->field($model, 'modified_by')->textInput() ?>

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
