<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\User;
use frontend\models\TblRStatus;
use yii\bootstrap\Modal;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTPeminjamanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peminjaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>

    <?= Html::button('Buat Peminjaman Baru', ['value'=>Url::to('index.php?r=tbl-t-peminjaman/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>

    <?php
        Modal::begin([
                'header'=>'<h4>Form Peminjaman Barang</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>


    

    <?= Html::a('Lihat Daftar Barang', ['tbl-t-buku-inventory/index'], ['class' => 'btn btn-success']) ?>     


   
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'peminjaman_id',
            // 'no_dokumen',
            [
                'attribute' => 'Pemohon',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    return $pemohon->nama;
                },
            ],
            [
                'attribute' => 'Divisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    $divisi = TblRStatus::find()->where(['kode'=>'divisi', 'no'=>$pemohon->divisi])->one();
                    return $divisi->nama;
                },
            ],
            'tanggal_trans',
            'tanggal_pinjam',
            'rencana_kembali',
            // 'rencana_waktu_kembali',
            // 'keterangan:ntext',
//            'approval',
            [
                'attribute' => 'Approval',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = TblRStatus::find()->where(['kode' => 'approval', 'no'=>$model->approval])->one();
                    return $status->nama;
                },
            ],
            
//            [
//                'attribute' => 'Action',
//                'format' => 'raw',
//                'value' => function($model){
//                    //$detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
//                    if($model['approval'] == 1){
//                    return '<div><center>' . Html::a('Approve', ['approve','id'=>$model->peminjaman_id], ['class' => 'btn btn-success', 'data' => [
//                        'confirm' => 'Are you sure you want to approve this request?',
//                        'method' => 'post',
//                    ],
//                    ]).'&nbsp;' . Html::a('Reject', ['reject', 'id' => $model->peminjaman_id], ['class' => 'btn btn-danger', 'data' => [
//                        'confirm' => 'Are you sure you want to reject this request?',
//                        'method' => 'post',
//                    ],
//                    ]). '</center></div>';
//                    }
//                    else{
//                        return "-";
//                    }
//                }
//            ],
            // 'approval_by',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

//            ['class' => 'yii\grid\ActionColumn',
//                'headerOptions' => ['width' => '50'],
//                'template' => '{view} {delete}{link}',
//            ],
            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'headerOptions' => ['width' => '50'],
            //     'template' => '{view} {link}',
            // ],






 [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                date_default_timezone_set("Asia/Jakarta");
                $end = $model->tanggal_pinjam;
                $current_date = date("Y-m-d");

                if($current_date > $end){                
                      return '<div>'.'<center>'.Html::a('Peminjaman Kadaluarsa', ['tbl-t-peminjaman/view','id'=>$model->peminjaman_id],['class'=>' btn btn-danger']).'</center>'.'</div>'
                    ;
                }

                else{

               return '<div>'.'<center>'.Html::a('Detail peminjaman', ['tbl-t-peminjaman/view2','id'=>$model->peminjaman_id],['class'=>' btn btn-success']).'</center>'.'</div>'
                    ;
            }
        },

            ],







        ],
    ]); ?>
</div>
