<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use frontend\models\RegistrationForm;
use common\models\AuthAssignment;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionRegister()
    {
        $model = new RegistrationForm();
        
        $model->scenario = 'register';
        if ($model->load(Yii::$app->request->post()))
        {
            if($user = $model->register()){
                
                return $this->redirect('index.php?r=site%2Flogin');
            }
        }
        return $this->render('register', [
                'model' => $model,
        ]);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'register'],
                'rules' => [
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) 
        {
              return $this->redirect(['login']);
        }
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
       if (!\Yii::$app->user->isGuest) 
        {
            return $this->goHome();
        }

        $model = new LoginForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) 
        {
            $auth = Yii::$app->getAuthManager();
            $curuser = Yii::$app->user->id;


 $namaBarang = (new \yii\db\Query())
                    ->select ('divisi')
                    ->from('user')
                    ->where(['id' => $curuser])
                    ->scalar();
// var_dump($namaBarang);die();


if($namaBarang != 1 && $namaBarang != 4){
      return $this->redirect(Url::to('http://localhost:8081/Sistem Informasi Pengelolaan Barang Inventaris/PROJECT/frontend/web/index.php'));
}

else{
    return $this->redirect(Url::to('http://localhost:8081/Sistem Informasi Pengelolaan Barang Inventaris/PROJECT/backend/web/index.php'));
}



            // if(!$auth->checkAccess($curuser, "staff_inventory"))
            // {
            //     return $this->redirect(Url::to('http://localhost:8089/inventorybaru/inventory/backend/web/index.php'));
            // }
            
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $curuser = Yii::$app->user->id;


 $namaBarang = (new \yii\db\Query())
                    ->select ('divisi')
                    ->from('user')
                    ->where(['id' => $curuser])
                    ->scalar();
// var_dump($namaBarang);die();





if($namaBarang != 1){
   Yii::$app->user->logout();
        return $this->redirect(Url::to('http://localhost:8081/Sistem Informasi Pengelolaan Barang Inventaris/PROJECT/frontend/web/index.php'));
}

else{
    Yii::$app->user->logout();
    return $this->redirect(Url::to('http://localhost:8081/Sistem Informasi Pengelolaan Barang Inventaris/PROJECT/frontend/web/index.php'));
}
    }



    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }





    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }







// public function actionSignup()
// {
// $model = new SignupForm();

// if ($model->load(Yii::$app->request->post())) {
//     if ($user = $model->signup()) {

        
//         $email = \Yii::$app->mailer->compose()
//         ->setTo($user->email)
//         ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
//         ->setSubject('Signup Confirmation')
//         ->setTextBody("Click this link ".\yii\helpers\Html::a('confirm',Yii::$app->urlManager->createAbsoluteUrl(['site/confirm','id'=>$user->id,'key'=>$user->auth_key])))->send();



//         if($email){
//         Yii::$app->getSession()->setFlash('success','Check Your email!');
//         }

//         else{
//         Yii::$app->getSession()->setFlash('warning','Failed, contact Admin!');
//         }

// return $this->goHome();
// }



// }
 
// return $this->render('signup', [
// 'model' => $model,
// ]);
// }







// public function actionConfirm($id, $key)
// {

// $user = \common\models\User::find()->where(['id'=>$id,'auth_key'=>$key,'status'=>0,])->one();

// if(!empty($user)){
// $user->status=10;
// $user->save();
// Yii::$app->getSession()->setFlash('success','Success!');
// }

// else{
// Yii::$app->getSession()->setFlash('warning','Failed!');
// }

// return $this->goHome();
// }




















    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
