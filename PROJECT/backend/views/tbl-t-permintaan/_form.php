<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblTPermintaanDetail;
use yii\helpers\ArrayHelper;
use backend\models\TblRBarang;
use backend\models\TblRStatus;
use kartik\date\DatePicker;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaan */
/* @var $form yii\widgets\ActiveForm */
$modelsDetail = [new TblTPermintaanDetail()];
$barang = new TblRBarang();
?>

<div class="tbl-tpermintaan-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?php if(Yii::$app->controller->action->id == 'update') { 
        echo $form->field($model, 'alasan_penolakan')->textarea(['rows' => 6]); 
    } ?>
    
    <?php if(Yii::$app->controller->action->id != 'update') { ?> 
        <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Tambahkan Detail Permintaan</h4></div>
            <div class="panel-body">
                <?php
                DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 999, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsDetail[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'barang_id',
                        'jumlah',
                        'satuan',
                        'keterangan',
                    ],
                ]);
                ?>

                <div class="container-items"><!-- widgetContainer -->
<?php foreach ($modelsDetail as $i => $modelsDetail): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Detail Permintaan</h3>
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (!$modelsDetail->isNewRecord) {
                                    echo Html::activeHiddenInput($modelsDetail, "[{$i}]id");
                                }
                                ?>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsDetail, "[{$i}]barang_id")->dropDownList(ArrayHelper::map(TblRBarang::find()->where("(kategori = '1')")->all(), 'barang_id', 'nama'), ['prompt' => '-Pilih Barang']) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsDetail, "[{$i}]jumlah")->textInput(['maxlength' => true]) ?>
                                    </div>  
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsDetail, "[{$i}]satuan")->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'satuan_barang')")->all(), 'no', 'nama'), ['prompt' => '-Pilih Satuan']) ?>
                                    </div>  
                                    <div class="col-sm-6">
                                        <?= $form->field($modelsDetail, "[{$i}]keterangan")->textInput(['maxlength' => true]) ?>
                                    </div>  
                                </div>
                            </div>
<?php endforeach; ?>
                    </div>
            <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>

    <?= $form->field($model, 'bulan_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'bulan')")->all(),'no', 'nama'),['prompt'=>'-Pilih Bulan-'])?>

    <?= $form->field($model, 'tahun_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'tahun')")->all(),'no', 'nama'),['prompt'=>'-Pilih Tahun-'])?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <!--?= $form->field($model, 'created_date')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <?php } ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
