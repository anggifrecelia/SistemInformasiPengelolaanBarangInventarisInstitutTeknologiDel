<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTBukuInventory */

$mei2=$model->buku_id;

// var_dump($mei2); die();

$mei = (new \yii\db\Query())
    ->select ('barang_id')
    ->from('tbl_t_bukuinventory')
    ->where(['buku_id' => $mei2])
    ->scalar();
// var_dump($mei);die();

$namaBarang = (new \yii\db\Query())
    ->select ('nama')
    ->from('tbl_r_barang')
    ->where(['barang_id' => $mei])
    ->scalar();

// var_dump($namaBarang);die();



$this->title = $namaBarang;


$this->params['breadcrumbs'][] = ['label' => 'Tbl Tbuku Inventories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tbuku-inventory-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->barang_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->barang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'buku_id',

            [
                'attribute' => 'barang_id',
                'value' => $model->barang->nama,
            ],


            [
                'attribute' => 'gedung_id',
                'value' => $model->gedung->nama,
            ],
            'baik',
            'rusak',
            'jumlah',
            'satuan',
            'deskripsi',
        ],
    ]) ?>






    <p  align="center">


        <?= Html::a('Lihat Riwayat Pencatatan Barang', ['tbl-t-pencatatan-aset/view-riwayat-pencatatan-barang', 'barang_id' => $model->barang_id], ['class' => 'btn btn-primary']) ?>
        
      <!--   <?= Html::a('Lihat Riwayat Mutasi Barang',['tbl-t-mutasi-aset/view-riwayat-mutasi-barang', 'barang_id' => $model->barang_id] , ['class' => 'btn btn-primary']) ?> -->

       <!--  <?= Html::a('Lihat Riwayat Peminjaman Barang', ['tbl-t-peminjaman-detail/view-riwayat-peminjaman-barang', 'barang_id' => $model->barang_id], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Lihat Riwayat Kerusakan Barang', ['update', 'id' => $model->barang_id], ['class' => 'btn btn-primary']) ?> -->

    </p>


</div>
