<?php

use yii\helpers\Html;
use backend\models\TblTPermintaanDetail;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaan */

$this->title = 'Create Permintaan';
$this->params['breadcrumbs'][] = ['label' => 'Permintaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpermintaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
    ]) ?>

</div>
