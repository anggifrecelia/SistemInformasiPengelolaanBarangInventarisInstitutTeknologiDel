<?php

namespace backend\controllers;

use Yii;
use yii\data\SqlDataProvider;
use backend\models\TblTPermintaan;
use backend\models\TblTPermintaanDetail;
use backend\models\TblTPermintaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\TblRBarang;
use backend\models\Model;
use backend\models\TblTStokbarang;
use kartik\mpdf\Pdf;

/**
 * TblTPermintaanController implements the CRUD actions for TblTPermintaan model.
 */
class TblTPermintaanController extends Controller
{
    public $dokumen = 274;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPermintaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPermintaan model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
//        $searchModel = new TblTPermintaanSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//            'dataProvider' => $dataProvider,
//        ]);
        $model = TblTPermintaan::find()->where(['permintaan_id'=>$id]);
        $dataProviderPermintaan = new \yii\data\ActiveDataProvider(
                [
                    'query'=>$model,
                ]
                );
        
        $models1TblPermintaanDetail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$id]);
        
        $dataProviderPermintaanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPermintaanDetail,
        ]);
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProviderPermintaanDetail'=>$dataProviderPermintaanDetail,
        ]);
    }

    public function actionCreate()
    {
        $model = new TblTPermintaan();
        $modelsDetail = [new TblTPermintaanDetail()];
        
        if ($model->load(Yii::$app->request->post())){
            if(!($model->tanggal_trans < date('Y-m-d')))
            {
                $modelsDetail = Model::createMultiple(TblTPermintaanDetail::classname());
                Model::loadMultiple($modelsDetail, Yii::$app->request->post());

                $model->pemohon = Yii::$app->user->id;
                $model->tipe = 1;
                $model->created_by = Yii::$app->user->id;
                $model->created_date = date('Y-m-d');
                $model->created_ip = Yii::$app->getRequest()->getUserIP();
                // validate all models  
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsDetail);

                if (!$valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                            foreach ($modelsDetail as $modelDetail) {
                                $modelDetail->permintaan_id = $model->permintaan_id;
                                $modelDetail->created_by = Yii::$app->user->id;
                                $modelDetail->created_date = date('Y-m-d');
                                $modelDetail->created_ip = Yii::$app->getRequest()->getUserIP();
                                $modelDetail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Requested'")->queryScalar();
                                if (! ($flag = $modelDetail->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash(
                    'error','Request Gagal ! Periksa tanggal request Anda!'
                );
                return $this->render('create', [
                    'model' => $model,
                    'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
                ]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
        ]);
        
    }
    /**
     * Updates an existing TblTPermintaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //$detail = new TblTPermintaanDetail();
        //$barang = new TblRBarang();
        $model = $this->findModel($id);
        //$model1 = $detail->findModel($id);        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->modified_by = Yii::$app->user->id;
            $model->modified_date = date('Y-m-d');
            $model->modified_ip = Yii::$app->getRequest()->getUserIP();
            $model->save();
            Yii::$app->db->createCommand("Update tbl_t_permintaan set modified_date= '$model->modified_date', modified_by = $model->modified_by, modified_ip = '$model->modified_ip', alasan_penolakan = '$model->alasan_penolakan' where permintaan_id = $id")->execute();
            
           
            //RejectRequest($id);
            $searchModel = new TblTPermintaanSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $user = Yii::$app->user->id; 
            //$model = $this->findModel($id);
            $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
            Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 3, status_by = $user where permintaan_id = $id AND status = 1")->execute();

           //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
            //$detail->status_by = Yii::$app->user->id;
            $detail->save();

            $date = date('H:i');
            echo $date;
            $sapa = '';
            if ($date < 12)
                $sapa = "Pagi";
            else if ($date < 14)
                $sapa = 'Siang';
            else if ($date < 18)
                $sapa = 'Sore';

            $message = 'Pemberitahuan Request Ditolak<br>'
                    . 'Selamat ' . $sapa . ' <br>'
                    .' Kami ingin pihak Staff Inventory memberitahukan bahwa Request yang dilakukan oleh Bapak/Ibu Telah kami tolak.'
                    .'Dikarenakan oleh '. $model->alasan_penolakan . ' <br> <br>Atas perhatian Bapak/Ibu Sekalian, Kami Ucapkan Terimakasih'
                    . '<br><br><br> <hr> Dikirim oleh : <br>'
                    . '<b>Sistem Informasi Inventory ( SII ) : by Staff Inventory <b>';

            $pemohon = \backend\models\User::findOne(['id'=>$model->pemohon]); 

            $tujuan = $pemohon->email;

            $email = Yii::$app->mailer->compose()
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'Sistem Informasi Inventory'])
                    ->setTo($tujuan)
                    ->setSubject("Notifikasi Request Ditolak")
                    ->setHtmlBody($message)
                    ->send();

            $connection = \Yii::$app->db;
            
            return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPermintaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        Yii::$app->db->createCommand("delete from tbl_t_permintaan_detail  where permintaan_id = $id")->execute();
        Yii::$app->db->createCommand("delete from tbl_t_permintaan where permintaan_id = $id")->execute();
        $detail->save();
        
        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'dataProvider' => $dataProvider,
            ]);
    }
    public function setDokumen()
    {
        $this->dokumen++;
    }







    public function actionApprove($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        
        $dokumenNo = 'DOCPERMINTAAN'.$this->dokumen;
        $this->setDokumen();



$nama = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('user')
                    ->where(['id' => $user])
                    ->scalar();

$mei = (new \yii\db\Query())
                    ->select ('permintaan_detail_id')
                    ->from('tbl_t_permintaan_detail')
                    ->where(['permintaan_id' => $model->permintaan_id])
                    ->scalar();
                    // var_dump($mei);die();

$jumlahminta = (new \yii\db\Query())
                    ->select ('jumlah')
                    ->from('tbl_t_permintaan_detail')
                    ->where(['permintaan_id' => $model->permintaan_id])
                    ->scalar();

$barangnya = (new \yii\db\Query())
                    ->select ('barang_id')
                    ->from('tbl_t_permintaan_detail')
                    ->where(['permintaan_id' => $model->permintaan_id])
                    ->scalar();

$jumlahada = (new \yii\db\Query())
                    ->select ('jumlah')
                    ->from('tbl_t_stokbarang')
                    ->where(['barang_id' => $barangnya])
                    ->scalar();
                    // var_dump($baiknya);die();


$sisanya = $jumlahada-$jumlahminta;


Yii::$app->db->createCommand("Update tbl_t_stokbarang set available = '$sisanya' where barang_id = '$barangnya' ")->execute();

         Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 2, status_by = $user where permintaan_id = $id AND status = 1")->execute();


        Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 2, status_by = $user where permintaan_id = $id AND status = 1")->execute();

        Yii::$app->db->createCommand("Update tbl_t_permintaan set no_dokumen = '$dokumenNo' where permintaan_id = $id ")->execute();
        
        $detail->save();

        $connection = \Yii::$app->db;  
        
        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }
    






  //fungsi reject
    public function actionReject($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();

        // Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 3, approval_by= $user where permintaan_id = $id AND approval = 1")->execute();
        // $model->save();
$modele = new TblTPermintaanDetail();
$mei = (new \yii\db\Query())
                    ->select ('permintaan_detail_id')
                    ->from('tbl_t_permintaan_detail')
                    ->where(['permintaan_id' => $model->permintaan_id])
                    ->scalar();

Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = '3', status_by = $user where permintaan_detail_id = $mei")->execute();
$modele->save();
        $connection = \Yii::$app->db;  
        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }




    /**
     * Finds the TblTPermintaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPermintaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPermintaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


 public function actionLaporanIndex()
    {

        if(($bulan = Yii::$app->request->post('bulan')) && ($tahun = Yii::$app->request->post('tahun'))) {//($request) {//

        } else {
            $timeZone = 'Asia/Jakarta';
            $timestamp = time();
            $dt = new \DateTime("now", new \DateTimeZone($timeZone));
            $dt->setTimestamp($timestamp);
            $bulan = $dt->format('m');
            $tahun = $dt->format('Y');
        }

        // Mengambil nama bulan
        switch ($bulan) {
            case 1 :
                $namaBulan = 'Januari';
                break;
            case 2 :
                $namaBulan = 'Februari';
                break;
            case 3 :
                $namaBulan = 'Maret';
                break;
            case 4 :
                $namaBulan = 'April';
                break;
            case 5 :
                $namaBulan = 'Mei';
                break;
            case 6 :
                $namaBulan = 'Juni';
                break;
            case 7 :
                $namaBulan = 'Juli';
                break;
            case 8 :
                $namaBulan = 'Agustus';
                break;
            case 9 :
                $namaBulan = 'September';
                break;
            case 10 :
                $namaBulan = 'Oktober';
                break;
            case 11 :
                $namaBulan = 'November';
                break;
            case 12 :
                $namaBulan = 'Desember';
                break;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT t.permintaan_id,  t.no_dokumen, t.pemohon, t.tanggal_trans, t.keterangan, td.barang_id FROM tbl_t_permintaan as t INNER JOIN tbl_t_permintaan_detail as td ON t.permintaan_id=td.permintaan_id  WHERE MONTH(t.tanggal_trans)=:bulan AND YEAR(t.tanggal_trans)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        return $this->render('index-laporan', [
            'dataProvider' => $dataProvider,
            'namaBulan' => $namaBulan,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);
    }



  public function actionGetPdf($bulan, $tahun, $namaBulan) {

        $dataProvider = new SqlDataProvider([
          'sql' => 'SELECT t.permintaan_id,  t.no_dokumen, t.pemohon, t.tanggal_trans, t.keterangan, td.barang_id FROM tbl_t_permintaan as t INNER JOIN tbl_t_permintaan_detail as td ON t.permintaan_id=td.permintaan_id  WHERE MONTH(t.tanggal_trans)=:bulan AND YEAR(t.tanggal_trans)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);
        $pdf = new Pdf([
            'filename' => 'Laporan' . $bulan . $tahun,
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('laporan', ['dataProvider' => $dataProvider, 'namaBulan' => $namaBulan, 'tahun' => $tahun]),
            'options' => [
                'title' => 'Laporan Bulanan - PDF' ,
                'subject' => 'Laporan Bulan ' . $bulan . ' ' . $tahun
            ],
            'methods' => [
                'SetHeader' => ['Laporan Permintaan||' . $bulan . ' ' . $tahun],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }
}