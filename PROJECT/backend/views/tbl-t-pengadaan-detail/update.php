<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaanDetail */

$this->title = 'Update Tbl Tpengadaan Detail: ' . ' ' . $model->pengadaan_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpengadaan Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pengadaan_detail_id, 'url' => ['view', 'id' => $model->pengadaan_detail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpengadaan-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
