<?php

use yii\grid\GridView;
use backend\models\User;
use yii\helpers\Html;
use backend\models\TblRBarang;
?>

<h2 class="text-center"><u><b>Laporan Bulan <?=$namaBulan . ' ' . $tahun?></b></u></h2>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => 'No.'],

        // 'no_dokumen',
         // 'pemohon',
        [
            'attribute' => 'Pemohon',
            'format' => 'raw',
            'value' => function ($model) {
                $pemohon = User::find()->where(['id'=>$model['pemohon']])->one();
                return $pemohon->nama;
            },
        ],

         [
                    'attribute' => 'Barang',
          'format' => 'raw',
          'value' => function ($model) {
              $barang = TblRBarang::find()->where(['barang_id' => $model['barang_id']])->one();
              return $barang->nama;
           },
                ],
        'tanggal_trans',
        // 'bulan_trans',
        // 'tahun_trans',
        'keterangan',
        // [
        //             'attribute' => 'Action',
        //             'format' => 'raw',
        //             'value' => function($model) {
        //                 // return '<div><center>' . Html::a('Detail Permintaan', ['view', 'id' => $model['permintaan_id']], ['class' => 'btn btn-success', 'data'
        //                 // ]);
        //             }
        //         ],
 
    ],
    'summary' => '',
]); ?>
