<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPeminjamanDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Tpeminjaman Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Tpeminjaman Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'peminjaman_detail_id',
            'peminjaman_id',
            'barang_id',
            'jumlah',
            'satuan',
            // 'keterangan',
            // 'status_pinjam',
            // 'status_kembali',
            // 'status_kembali_by',
            // 'tanggal_kembali',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
