<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TblRBarang;
use yii\helpers\ArrayHelper;
use frontend\models\TblRGedung;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblTStokbarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tstokbarang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'barang_id')->dropDownList(ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),['prompt'=>'-Pilih Nama Barang-'])?>

    <?= $form->field($model, 'gedung_id')->dropDownList(ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),['prompt'=>'-Pilih Gedung-'])?>

    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'satuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'deleted')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'created_date')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
