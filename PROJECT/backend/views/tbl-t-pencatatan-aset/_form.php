<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use backend\models\TblTBukuInventory;
use backend\models\TblRGedung;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTPencatatanAset */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="tbl-tpencatatan-aset-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'buku_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(TblTBukuInventory::find()->all(),'buku_id', 'deskripsi'),
            'language' => 'en',
            'options' => ['placeholder' => '- Pilih Buku -', 'id'=>'bukuId'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>


        <?= $form->field($model, 'barang_id')->textInput(['readonly'=>true]) ?>

        <?= $form->field($model, 'gedung_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),
            'language' => 'en',
            'options' => ['placeholder' => '- Pilih Gedung -', 'id'=>'pencatatanId'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

        <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true, 'value'=>"ITD/PA/"]) ?>

        <?= $form->field($model, 'tanggal_trans')->widget(
            DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'readonly'=>true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>


        <?= $form->field($model, 'jumlah')->textInput() ?>

        <?= $form->field($model, 'satuan')->textInput(['readonly'=>true]) ?>

        <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'created_date')->textInput(['readonly'=>true]) ?>

        <?= $form->field($model, 'created_by')->textInput(['readonly'=>true]) ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


<?php
$script = <<< JS

//here you right all you javascript stuff

$('#bukuId').change(function(){

    var bukuCode = $(this).val();
    
    //alert(pencatatanId);
    
    $.get('index.php?r=tbl-t-buku-inventory/get',{ bukuCode : bukuCode }, function(data){
    var data = $.parseJSON(data);

        // $('#tbltpencatatanaset-gedung_id').attr('value',data.gedung_id);

        $('#tbltpencatatanaset-barang_id').attr('value',data.barang_id);
        $('#tbltpencatatanaset-jumlah').attr('value',data.baik);
        $('#tbltpencatatanaset-satuan').attr('value',data.satuan);

        });

    });

JS;
$this->registerJS($script);

?>