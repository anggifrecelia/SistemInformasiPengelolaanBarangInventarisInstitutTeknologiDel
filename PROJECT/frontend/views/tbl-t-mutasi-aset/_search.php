<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTMutasiAsetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tmutasi-aset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mutasi_id') ?>

    <?= $form->field($model, 'buku_id') ?>

    <?= $form->field($model, 'kode_trans') ?>

    <?= $form->field($model, 'tanggal_trans') ?>

    <?= $form->field($model, 'lokasi') ?>

    <?php // echo $form->field($model, 'baik') ?>

    <?php // echo $form->field($model, 'rusak') ?>

    <?php // echo $form->field($model, 'alasan_mutasi') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
