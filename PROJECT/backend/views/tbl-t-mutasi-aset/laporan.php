<?php
use yii\grid\GridView;
use backend\models\User;
use yii\widgets\DetailView;
use backend\models\TblTMutasiAset;
use backend\models\TblTPencatatanAset;
use backend\models\TblRBarang;
use backend\models\TblRGedung;

$mei = new TblRBarang();


$PencatatanId = (new \yii\db\Query())
    ->select ('pencatatan_id')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$StatusMutasi = (new \yii\db\Query())
    ->select ('status_mutasi')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$BarangId = (new \yii\db\Query())
    ->select ('barang_id')
    ->from('tbl_t_pencatatan_aset')
    ->where(['pencatatan_id' => $PencatatanId])
    ->scalar();

$BarangName = (new \yii\db\Query())
    ->select ('nama')
    ->from('tbl_r_barang')
    ->where(['barang_id' => $BarangId])
    ->scalar();


// $gedungs = TblRGedung::find()->where(['gedung_id' => $gedungs->])

// var_dump($BarangName);die();
/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAset */
?>

<h2 class="text-center"><u><b>Laporan Bulan <?= $namaBulan . ' ' . $tahun ?></b></u></h2>

  

    <?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => 'No.'],
        
        'attributes' => [
            'mutasi_id',

            [
                'attribute' => 'Id Pencatatan',
                'value' => $model->pencatatan->pencatatan_id,
            ],

            [
                'attribute' => 'Barang',
                'value' => $model->pencatatan->kode_trans,
            ],

            'kode_trans',

            'tanggal_trans',

             [
                'attribute' => 'Lokasi Tujuan',
                'value' => $model->gedung->nama,
            ],


            // 'lokasi_tujuan',
            [
                'attribute' => 'Lokasi Tujuan',
                'value' => $model->gedung2->nama,
            ],

            'jumlah',

            'alasan_mutasi',

            
            [
                'attribute' => 'Created By',
                'value' => $model->createdBy->nama,
            ],

    

   ]);
                ?>