<?php

namespace backend\controllers;

use Yii;
use backend\models\TblTMutasiAset;
use backend\models\TblTMutasiAsetSearch;
use backend\models\TblRGedung;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\TblTPencatatanAset;
use kartik\mpdf\Pdf;
/**
 * TblTMutasiAsetController implements the CRUD actions for TblTMutasiAset model.
 */
class TblTMutasiAsetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTMutasiAset models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTMutasiAsetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTMutasiAset model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
 

    public function actionGetPdf($id) {
        
    

        $pdf = new Pdf([
            'filename' => 'bukti_mutasi.pdf',
            'mode' => Pdf::MODE_UTF8,
                'content' => $this->renderPartial('view2', [
            'model' => $this->findModel($id)]),
                'options' => [
                'title' => 'Cetak Bukti Mutasi - PDF' ,
                'subject' => 'Cetak Bukti Mutasi'
            ],
            'methods' => [
                'SetHeader' => ['Cetak Bukti'],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }



 public function actionViewRiwayatMutasiBarang($barang_id)
    {
        $searchModel = new TblTMutasiAsetSearch(['barang_id'=>$barang_id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view_riwayat_mutasi_barang', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new TblTMutasiAset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new TblTMutasiAset();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        // $model->deleted = '0';
        if ($model->load(Yii::$app->request->post())) {
            $model_tmp = TblRGedung::find()->where(["nama"=>$model->lokasi])->one();
            $model->lokasi = $model_tmp->gedung_id; 
            $model->save();
            $jumlahmutasi = $model->jumlah;
            // $jumlahmutasi = (new \yii\db\Query())
            //     ->select ('jumlah')
            //     ->from('tbl_t_mutasi_aset')
            //     ->where(['mutasi_id' => $model->mutasi_id])
            //     ->scalar();


            $jumlahpencatatan = (new \yii\db\Query())
                ->select ('jumlah')
                ->from('tbl_t_pencatatan_aset')
                ->where(['pencatatan_id' => $model->pencatatan_id])
                ->scalar();


            $gedungpencatatan = (new \yii\db\Query())
                ->select ('gedung_id')
                ->from('tbl_t_pencatatan_aset')
                ->where(['pencatatan_id' => $model->pencatatan_id])
                ->scalar();
// var_dump($gedungpencatatan);die();
                $gedungmutasi = $model->lokasi_tujuan;
            // $gedungmutasi = (new \yii\db\Query())
            //     ->select ('lokasi_tujuan')
            //     ->from('tbl_t_mutasi_aset')
            //     ->where(['mutasi_id' => $model->mutasi_id])
            //     ->scalar();

            $TransDate = (new \yii\db\Query())
                ->select ('tanggal_trans')
                ->from('tbl_t_pencatatan_aset')
                ->where(['pencatatan_id' => $model->pencatatan_id])
                ->scalar();


            // var_dump($gedungmutasi);die();
            // var_dump($gedungpencatatan);die();

            $jumlahAkhir = $jumlahpencatatan-$jumlahmutasi;

            // echo $model->jumlah;
            // echo '\n';
            // echo $jumlahpencatatan;
            // echo '\n';
            // echo $gedungpencatatan;
            // echo '\n';
           //  echo $TransDate;
          //   echo "\n";
            // echo  $model->tanggal_trans;
            if( ($jumlahmutasi <= $jumlahpencatatan) && ($gedungpencatatan!=$gedungmutasi)  ){

                Yii::$app->session->setFlash('success', "Proses Mutasi Barang berhasil dilakukan");

                Yii::$app->db->createCommand("Update tbl_t_pencatatan_aset set jumlah = $jumlahAkhir where pencatatan_id = $model->pencatatan_id")->execute();


                Yii::$app->db->createCommand("Update tbl_t_mutasi_aset set status_mutasi = '1'where pencatatan_id = $model->pencatatan_id")->execute();

                Yii::$app->db->createCommand("Update tbl_t_mutasi_aset set status_pindah = '1' where pencatatan_id = $model->pencatatan_id")->execute();

            }

            else{

                Yii::$app->session->setFlash('error', "Jumlah barang yang Anda input salah!");

                Yii::$app->db->createCommand("Update tbl_t_mutasi_aset set status_mutasi = '2' where pencatatan_id = $model->pencatatan_id")->execute();

            }




           return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing TblTMutasiAset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->mutasi_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTMutasiAset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }




    public function actionPindah($id)
    {
        // var_dump($id);die();

        Yii::$app->db->createCommand("Update tbl_t_mutasi_aset set status_pindah = '2' where mutasi_id = $id")->execute();


        $this->findModel($id);
        return $this->redirect(['view']);
    }



// public function actionPindah($id){
//     $searchModel = new TblTMutasiAsetSear();
//         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//         $user = Yii::$app->user->id;
//         $model = $this->findModel($id);

//         Yii::$app->db->createCommand("Update tbl_t_mutasi_aset set status_pindah = 2 where mutasi_id = $id")->execute();

//         $model->save();
// return $this->render('view', [
//                 'searchModel' => $searchModel,
//                 'dataProvider' => $dataProvider,
//             ]);
// }


    /**
     * Finds the TblTMutasiAset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTMutasiAset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTMutasiAset::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
