<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model backend\models\TblRKerusakanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rkerusakan-search">

    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!--?= $form->field($model, 'kerusakan_id') ?-->
    
    <?= $form->field($model, 'barang_id')->textInput(['style'=>'width:300px'])?>

    <!--?= $form->field($model, 'berita_acara_id') ?>

    <!--?= $form->field($model, 'no_laporan') ?>

    <!--?= $form->field($model, 'tanggal') ?-->

    <?php // echo $form->field($model, 'jumlah') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

     <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
