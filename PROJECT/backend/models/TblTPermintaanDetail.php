<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_permintaan_detail".
 *
 * @property string $permintaan_detail_id
 * @property string $permintaan_id
 * @property integer $barang_id
 * @property integer $jumlah
 * @property string $satuan
 * @property string $keterangan
 * @property integer $status
 * @property integer $status_by
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTPermintaan $permintaan
 * @property TblRBarang $barang
 */
class TblTPermintaanDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_permintaan_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permintaan_id', 'barang_id'], 'required'],
            [['permintaan_id', 'barang_id', 'jumlah', 'status', 'status_by', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['keterangan'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'permintaan_detail_id' => 'Permintaan Detail ID',
            'permintaan_id' => 'Permintaan ID',
            'barang_id' => 'Nama Barang',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'status' => 'Status',
            'status_by' => 'Status By',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermintaan()
    {
        return $this->hasOne(TblTPermintaan::className(), ['permintaan_id' => 'permintaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }
}
