<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use frontend\models\TblRBarang;
use frontend\models\TblRGedung;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTBukuInventorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tbuku-inventory-search">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    

<?= $form->field($model, 'barang_id')->widget(Select2::classname(),[
    'data' => ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),
    'language' => 'en',
    'options' => ['placeholder' => '-Pilih Barang-'],
    'pluginOptions' => [
        'allowClear' => true
        ],
]);?>


   
<?= $form->field($model, 'gedung_id')->widget(Select2::classname(),[
    'data' => ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),
    'language' => 'en',
    'options' => ['placeholder' => '-Pilih Gedung-'],
    'pluginOptions' => [
        'allowClear' => true
        ],
]);?>



    <?php // echo $form->field($model, 'rusak') ?>

    <?php // echo $form->field($model, 'jumlah') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
