<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblTMutasiAset;
use backend\models\TblTPencatatanAset;
use backend\models\TblRBarang;
use backend\models\TblRGedung;

$mei = new TblRBarang();


$PencatatanId = (new \yii\db\Query())
    ->select ('pencatatan_id')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$StatusMutasi = (new \yii\db\Query())
    ->select ('status_mutasi')
    ->from('tbl_t_mutasi_aset')
    ->where(['mutasi_id' => $model->mutasi_id])
    ->scalar();

$BarangId = (new \yii\db\Query())
    ->select ('barang_id')
    ->from('tbl_t_pencatatan_aset')
    ->where(['pencatatan_id' => $PencatatanId])
    ->scalar();

$BarangName = (new \yii\db\Query())
    ->select ('nama')
    ->from('tbl_r_barang')
    ->where(['barang_id' => $BarangId])
    ->scalar();


// $gedungs = TblRGedung::find()->where(['gedung_id' => $gedungs->])

// var_dump($BarangName);die();
/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAset */

$this->title =$BarangName;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tmutasi Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mutasi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mutasi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'mutasi_id',

            // [
            //     'attribute' => 'Id Pencatatan',
            //     'value' => $model->pencatatan->pencatatan_id,
            // ],

            [
                'attribute' => 'Barang',
                'value' => $model->pencatatan->keterangan,
            ],

            'kode_trans',

            'tanggal_trans',

             [
                'attribute' => 'Lokasi Awal',
                'value' => $model->gedung3->nama,
            ],


            // 'lokasi_tujuan',  
            [
                'attribute' => 'Lokasi Tujuan',
                'value' => $model->gedung2->nama,
            ],

            'jumlah',

            'alasan_mutasi',

             [
                'attribute' => 'Status Mutasi',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status_mutasi == 1)
                        return "Berhasil";
                    else
                        return "Tidak Berhasil";
                }
            ],
            [
                'attribute' => 'Status Pindah',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status_pindah != 1)
                        return "Sudah dipindahkan";
                    else
                        return "Belum Dipindahkan";
                }
            ],

            'created_date',
            [
                'attribute' => 'Created By',
                'value' => $model->createdBy->nama,
            ],

            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){

                    $StatusMutasi = (new \yii\db\Query())
                        ->select ('status_mutasi')
                        ->from('tbl_t_mutasi_aset')
                        ->where(['mutasi_id' => $model->mutasi_id])
                        ->scalar();


// var_dump($model->mutasi_id);die();
                    // $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();;

                    if($StatusMutasi != 2){ 

                        return '<div><left>' . Html::a('Cetak Bukti Mutasi', ['tbl-t-mutasi-aset/get-pdf','id'=>$model->mutasi_id], ['class' => 'btn btn-success', 'data' => [
                                'confirm' => 'Kamu yakin barang sudah ditangan peminjam?',
                                'method' => 'post',],
                            ]).'&nbsp;

                    ' . Html::a('Update Status Pindah', ['tbl-t-mutasi-aset/pindah', 'id' => $model->mutasi_id], ['class' => 'btn btn-success', 'data' => ['confirm' => 'Kamu yakin barang sudah dipindahkan?',
                                'method' => 'post',]  ]).
                            '</center></div>';
                    }
                    else
                    {
                        return '-';
                    }
                }
            ],
        ],
    ])

    ?>

</div>
