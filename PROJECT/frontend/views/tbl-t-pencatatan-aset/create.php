<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPencatatanAset */

$this->title = 'Tambahkan Pencatatan Aset';
$this->params['breadcrumbs'][] = ['label' => 'Pencatatan Aset', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
