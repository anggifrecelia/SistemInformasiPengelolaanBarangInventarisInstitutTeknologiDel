<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use yii\widgets\Pjax;
use backend\models\TblRGedung;
use backend\models\TblTPencatatanAset;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAset */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tmutasi-aset-form">

    <?php $form = ActiveForm::begin(); ?>



        <?= $form->field($model, 'pencatatan_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(TblTPencatatanAset::find()->all(),'pencatatan_id', 'keterangan'),
            'language' => 'en',
            'options' => ['placeholder' => '- Pilih Barang -', 'id'=>'pencatatanId'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>




    <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true, 'value'=> "ITD/MA/"]) ?>

    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
        'name' => 'dp_3',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => '23-Februari-1998',
        'readonly'=>true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ],
    ]); ?>



    <?= $form->field($model, 'lokasi')->textInput(['readOnly'=>true]) ?>




    <?= $form->field($model, 'lokasi_tujuan')->dropDownList(ArrayHelper::map(TblRGedung::find('gedung_id')->all(),'gedung_id', 'nama'),['prompt'=>'-Pilih Gedung Tujuan-']) ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <?= $form->field($model, 'alasan_mutasi')->textArea(['maxlength' => true]) ?>


   <!--  <?= $form->field($model, 'created_date')->widget(
        DatePicker::classname(), [
        'name' => 'dp_3',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => '23-Februari-1998',
        'readonly'=>true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ],
    ]); ?> -->



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




<?php
$script = <<< JS


//here you right all you javascript stuff

$('#pencatatanId').change(function(){

    var pencatatanCode = $(this).val();
    
    //alert(pencatatanId);
    
    $.get('index.php?r=tbl-t-pencatatan-aset/get',{ pencatatanCode : pencatatanCode }, function(data){
    var data = $.parseJSON(data);

    //alert(data.barang_id);
        $('#tbltmutasiaset-lokasi').attr('value',data.kode_trans);
        $('#tbltmutasiaset-jumlah').attr('value',data.jumlah);
        });


    });



JS;
$this->registerJS($script);

?>
