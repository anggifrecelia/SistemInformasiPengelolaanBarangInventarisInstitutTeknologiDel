
<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use backend\models\TblRStatus;
use backend\models\TblTPeminjaman;

$this->title = 'Peminjaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-index">

    <h1><?= Html::encode($this->title) ?></h1>
   
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'peminjaman_id',
            'no_dokumen',
            [
                'attribute' => 'Pemohon',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    return $pemohon->nama;
                },
            ],

            [
                'attribute' => 'Divisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    $divisi = TblRStatus::find()->where(['kode'=>'divisi', 'no'=>$pemohon->divisi])->one();
                    return $divisi->nama;
                },
            ],

            'tanggal_trans',
            'tanggal_pinjam',
            // 'rencana_kembali',
            // 'rencana_waktu_kembali',
            // 'keterangan:ntext',
//            'approval',
            [
                'attribute' => 'Approval',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = TblRStatus::find()->where(['kode' => 'approval', 'no'=>$model->approval])->one();
                    return $status->nama;
                },
            ],

            'alasan_penolakan',
            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){
                    //$detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    if($model['approval'] == 1){
                  
                    return '<div><center>' . Html::a('Approve', ['approve','id'=>$model->peminjaman_id], 
                        ['class' => 'btn btn-success', 'data' => [
                        'confirm' => 'Are you sure you want to approve this request?',
                        'method' => 'post',
                        ],

                    ]).'&nbsp;' . Html::a('Reject', ['update', 'id' => $model->peminjaman_id], ['class' => 'btn btn-danger', 'data' => [
                        //'confirm' => 'Are you sure you want to reject this request?',
                        'method' => 'post',
                    ],
                    ]). '</center></div>';
                    }

                    else{
                        return "-";
                    }
                }
            ],

        //       [ 'attribute' => 'Keterangan',
        //         'format' => 'raw',
        //         'value' => function ($model){
        //         date_default_timezone_set("Asia/Jakarta");
        //         $end = $model->tanggal_pinjam;
        //         $current_date = date("Y-m-d");

        //         //jika tanggal hari ini lebih besar dari tanggal pengembalian
        //         if($current_date > $end){                
        //               return '<div>'.'<center>'.Html::a('Peminjaman Kadaluarsa', ['tbl-t-peminjaman/view2','id'=>$model->peminjaman_id],['class'=>' btn btn-danger']).'</center>'.'</div>'
        //             ;
        //         }

        //         else{

        //        return '<div>'.'<center>'.Html::a('Detail Peminjaman', ['tbl-t-peminjaman/view2','id'=>$model->peminjaman_id],['class'=>' btn btn-success']).'</center>'.'</div>'
        //             ;
        //     }
        // },

        //     ],


        ],
    ]); ?>
</div>
