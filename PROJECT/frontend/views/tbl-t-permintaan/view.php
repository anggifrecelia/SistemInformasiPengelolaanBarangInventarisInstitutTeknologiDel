<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\TblRBarang;
use backend\models\TblRStatus;
use backend\models\TblTPermintaanDetail;
use backend\models\User;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaan */

$this->title = $model->permintaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Permintaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$user = User::findOne(['id'=>$model->pemohon]);
?>

<h1>Permintaan Anda</h1>
<div class="tbl-tpermintaan-view">

<!--    <h1><?= Html::encode($this->title) ?></h1>-->

<!--    <p>
        <?= Html::a('Update', ['update', 'id' => $model->permintaan_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->permintaan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'permintaan_id',
//            'pemohon',
            [
                'label'  => 'Pemohon',
                'value' => $user->nama,
            ],
            'tanggal_trans',
           // 'bulan_trans',
           // 'tahun_trans',
            'keterangan:ntext',
           'created_date',
           // 'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>
    
    <h2 align="center">Detail Permintaan</h2>
    <?= GridView::widget([
        'dataProvider' => $dataProviderPermintaanDetail,
        //'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

           // 'permintaan_id',
           'barang.nama',
           'satuan',
           'jumlah',
           
//         
            
                        
           
            'created_date',
            'keterangan:ntext',
             [
                'attribute' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_permintaan'])->andWhere(['no' => $detail['status']])->one();
                    return $status['nama'];
                },
            ],
        
            
        ],
    ]); ?>



 

</div>
<p>
        <?= Html::a('Cetak Bukti Permintaan', ['tbl-t-permintaan/get-pdf','id'=>$model->permintaan_id], ['class' => 'btn btn-success']) ?>
    </p>