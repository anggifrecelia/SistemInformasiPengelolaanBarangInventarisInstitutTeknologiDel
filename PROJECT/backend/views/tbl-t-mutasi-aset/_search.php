<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use yii\widgets\Pjax;
use backend\models\TblRGedung;
use kartik\select2\Select2;
// use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTMutasiAsetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tmutasi-aset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

   <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>


<?= $form->field($model, 'lokasi')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Barang -', 'id'=>'pencatatanId'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
