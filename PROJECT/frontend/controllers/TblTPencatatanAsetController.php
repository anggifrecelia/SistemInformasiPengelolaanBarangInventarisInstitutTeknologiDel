<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTPencatatanAset;
use frontend\models\TblTPencatatanAsetSearch;
use frontend\models\TblTBukuInventory;
use frontend\models\TblTMutasiAset;
use frontend\models\TblRKerusakan;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TblTPencatatanAsetController implements the CRUD actions for TblTPencatatanAset model.
 */
class TblTPencatatanAsetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPencatatanAset models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPencatatanAsetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPencatatanAset model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblTPencatatanAset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTPencatatanAset();
        $bukuinventory = new TblTBukuInventory();
        $mutasi = new TblTMutasiAset();
        $kerusakan = new TblRKerusakan();
        
        //$bukuinventory->save();
        if ($model->load(Yii::$app->request->post())) 
        {
            if(TblTBukuInventory::findOne(['barang_id'=>$model->barang_id, 'gedung_id' => $model->gedung_id]))
            {
                $model->created_by = Yii::$app->user->id;
                $model->created_date = date('Y-m-d');
                $model->created_ip = Yii::$app->getRequest()->getUserIP();
                $model->save();
                
                $bukuinventory = TblTBukuInventory::findOne(['barang_id'=>$model->barang_id, 'gedung_id' => $model->gedung_id]);
                $bukuinventory->modified_by = Yii::$app->user->id;
                $bukuinventory->modified_date = date('Y-m-d');
                $bukuinventory->modified_ip = Yii::$app->getRequest()->getUserIP();
                if($model->kondisi == 1)
                {
                    $bukuinventory->baik += $model->jumlah;
                }
                else if($model->kondisi == 2)
                {
                    $bukuinventory->rusak += $model->jumlah;
                }
                $bukuinventory->jumlah += $model->jumlah;
            }
            else
            {
                $model->save();
                $bukuinventory->barang_id = $model->barang_id;
                $bukuinventory->gedung_id = $model->gedung_id;
                $bukuinventory->created_by = Yii::$app->user->id;
                $bukuinventory->created_date = date('Y-m-d');
                $bukuinventory->created_ip = Yii::$app->getRequest()->getUserIP();
                $bukuinventory->deskripsi = $model->keterangan;
                if($model->kondisi == 1)
                {
                    $bukuinventory->baik = $model->jumlah;
                    $bukuinventory->jumlah += $model->jumlah;
                }
                else if($model->kondisi == 2)
                {
                    $bukuinventory->rusak = $model->jumlah;
                    $bukuinventory->jumlah += $model->jumlah;
                }
                $bukuinventory->satuan = $model->satuan;
                $bukuinventory->deleted = '0';
            }
            
            if($bukuinventory->save(true))
            {
                $mutasi->buku_id = $bukuinventory->buku_id;
                $mutasi->kode_trans = $model->kode_trans;
                $mutasi->lokasi = $bukuinventory->gedung_id;
                $mutasi->tanggal_trans = $model->tanggal_trans;
                if($model->kondisi == 1)
                {
                    $mutasi->baik = $model->jumlah;
                }
                else if($model->kondisi == 2)
                {
                    $mutasi->rusak = $model->jumlah;
                }
                //$mutasi->jumlah += $model->jumlah;
                
                //create kerusakan instance
                if($model->kondisi == 2)
                {
                    //$kerusakan->no_laporan = $model->kode_trans;
                    $kerusakan->barang_id = $model->barang_id;
                    //$kerusakan->bulan = date('m');
                    //$kerusakan->tahun = date('Y');
                    $kerusakan->jumlah = $model->jumlah;
                    $kerusakan->satuan = $model->satuan;
                    //$kerusakan->keterangan = $model->keterangan;
                    //$kerusakan->tanggal = $model->tanggal_trans;
                    $kerusakan->created_by = Yii::$app->user->id;
                    $kerusakan->created_date = date('Y-m-d');
                    $kerusakan->created_ip = Yii::$app->getRequest()->getUserIP();
                    
                    $kerusakan->save();
                }
                    
                $mutasi->alasan_mutasi = $model->keterangan;
                $mutasi->created_date = date('Y-m-d');
                $mutasi->created_by = Yii::$app->user->id;;
                $mutasi->created_ip =  Yii::$app->getRequest()->getUserIP();
                if($mutasi->save(true))
                {
                    return $this->redirect(['index']);
                }
            }
            if($model->save(false)){
                Yii::$app->getSession()->setFlash(
                    'error','Gagal mencatat aset!'
                );
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTPencatatanAset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pencatatan_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPencatatanAset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTPencatatanAset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblTPencatatanAset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPencatatanAset::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
