<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTPencatatanAset */

// $this->title = 'Create Tbl Tpencatatan Aset';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpencatatan Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
