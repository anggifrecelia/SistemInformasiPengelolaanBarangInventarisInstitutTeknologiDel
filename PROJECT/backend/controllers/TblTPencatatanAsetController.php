<?php

namespace backend\controllers;

use Yii;
use backend\models\TblTPencatatanAset;
use backend\models\TblTPencatatanAsetSearch;
use backend\models\TblTBukuInventory;
use backend\models\TblTMutasiAset;
use backend\models\TblRKerusakan;
use backend\models\TblRGedung;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// use yii\helpers\Json;
use yii\helpers\Json;


/**
 * TblTPencatatanAsetController implements the CRUD actions for TblTPencatatanAset model.
 */
class TblTPencatatanAsetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPencatatanAset models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPencatatanAsetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPencatatanAset model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblTPencatatanAset model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionViewRiwayatPencatatanBarang($barang_id)
    {
        $searchModel = new TblTPencatatanAsetSearch(['barang_id'=>$barang_id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view_riwayat_pencatatan_barang', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new TblTPencatatanAset();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();

        if ($model->load(Yii::$app->request->post())) {
            echo $model->pencatatan_id;
           $model->save();

        $idBuku = (new \yii\db\Query())
               ->select ('buku_id')
                ->from('tbl_t_pencatatan_aset')
                ->where(['pencatatan_id' => $model->pencatatan_id])
                ->scalar();


             $idBarang = (new \yii\db\Query())
                ->select ('barang_id')
                ->from('tbl_t_bukuinventory')
                ->where(['buku_id' => $idBuku])
                ->scalar();


            $jumlahBuku = (new \yii\db\Query())
                ->select ('baik')
                ->from('tbl_t_bukuinventory')
                ->where(['buku_id' => $idBuku])
                ->scalar();

            $int1 = (int) $jumlahBuku;

// // var_dump($int1);die();

            $jumlahCatat = (new \yii\db\Query())
                ->select ('jumlah')
                ->from('tbl_t_pencatatan_aset')
                ->where(['pencatatan_id' => $model->pencatatan_id])
                ->scalar();

            $int2 = (int) $jumlahCatat;

            $sisanya = $int1-$int2;

            if(($int2<0) || ($int2>$int1) || ($model->tanggal_trans < $model->created_date)){

                Yii::$app->session->setFlash('error', "Proses Pencatatan Barang gagal dilakukan");

                Yii::$app->db->createCommand("Update tbl_t_pencatatan_aset set status_pencatatan = '2' where pencatatan_id = $model->pencatatan_id")->execute();

            }

            else{
                Yii::$app->session->setFlash('success', "Proses Pencatatan Barang berhasil dilakukan");

                Yii::$app->db->createCommand("Update tbl_t_bukuinventory set available = $sisanya where barang_id = $idBarang")->execute();

                Yii::$app->db->createCommand("Update tbl_t_pencatatan_aset set status_pencatatan = '1' where pencatatan_id = $model->pencatatan_id")->execute();
            }

            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTPencatatanAset model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pencatatan_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPencatatanAset model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTPencatatanAset model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblTPencatatanAset the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPencatatanAset::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionGet($pencatatanCode){

        $pencatatan = TblTPencatatanAset::findOne($pencatatanCode);
        $gedung = TblRGedung::findOne($pencatatan->gedung_id);
        $pencatatan->kode_trans = $gedung->nama;
        echo Json::encode($pencatatan);
        exit;

    }







}
