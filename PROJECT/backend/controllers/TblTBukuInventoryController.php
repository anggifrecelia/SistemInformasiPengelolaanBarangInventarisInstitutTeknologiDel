<?php

namespace backend\controllers;

use Yii;
use backend\models\TblTBukuInventory;
use backend\models\TblTBukuInventorySearch;
use backend\controllers\TblRKerusakanController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * TblTBukuInventoryController implements the CRUD actions for TblTBukuInventory model.
 */
class TblTBukuInventoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTBukuInventory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTBukuInventorySearch(['deleted'=>'0']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTBukuInventory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblTBukuInventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTBukuInventory();
        $model->created_by = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        $model->deleted = '0';

        if ($model->load(Yii::$app->request->post())) {
            $model->jumlah = $model->baik + $model->rusak;
            $mei = $model->baik;
            $model->available = $mei;
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblTBukuInventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $rusak = TblTBukuInventory::findOne(['barang_id' => $model->barang_id]);
        $rus = $rusak->rusak;
        $kerusakan = new \backend\models\TblRKerusakan();
        if ($model->load(Yii::$app->request->post())) {

            $a = $model->rusak;
            //die($a);
            $model->baik = $model->baik - $a;
            $model->rusak = $a + $rus;
            $model->save(false);

            $kerusakan->barang_id = $model->barang_id;
            $kerusakan->gedung_id = $model->gedung_id;
            $kerusakan->no_laporan = 1;
            $date = date('Y-m-d');
            $kerusakan->tanggal = $date;
            $kerusakan->jumlah = $model->rusak;
            $kerusakan->satuan = $model->satuan;
            $kerusakan->keterangan = $model->deskripsi;
            $kerusakan->save(false);
            return $this->redirect(['index']);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }




    /**
     * Updates an existing TblTBukuInventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate2($id)
    {
        $model = $this->findModel($id);
        $rusak = TblTBukuInventory::findOne(['barang_id' => $model->barang_id]);
        $rus = $rusak->rusak;
        $kerusakan = new \backend\models\TblRKerusakan();
        if ($model->load(Yii::$app->request->post())) {

            $a = $model->rusak;
            //die($a);
            $model->baik = $model->baik - $a;
            $model->rusak = $a + $rus;
            $model->save(false);

            $kerusakan->barang_id = $model->barang_id;
            $kerusakan->gedung_id = $model->gedung_id;
            $kerusakan->no_laporan = 1;
            $date = date('Y-m-d');
            $kerusakan->tanggal = $date;
            $kerusakan->jumlah = $model->rusak;
            $kerusakan->satuan = $model->satuan;
            $kerusakan->keterangan = $model->deskripsi;
            $kerusakan->save(false);
            return $this->redirect(['index']);
        }
        else {
            return $this->render('update2', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Deletes an existing TblTBukuInventory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->deleted == '0'){
            $model->deleted = '1';
            $model->save();
        }

        return $this->redirect(['index']);
    }
    /**
     * Finds the TblTBukuInventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblTBukuInventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTBukuInventory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionGet($bukuCode){

        $buku = TblTBukuInventory::findOne($bukuCode);
        echo Json::encode($buku);
        exit;

    }



}
