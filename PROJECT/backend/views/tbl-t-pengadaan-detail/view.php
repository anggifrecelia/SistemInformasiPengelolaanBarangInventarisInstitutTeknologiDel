<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaanDetail */

$this->title = $model->pengadaan_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpengadaan Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pengadaan_detail_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pengadaan_detail_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pengadaan_detail_id',
            'pengadaan_id',
            'barang_id',
            'jumlah',
            'satuan',
            'keterangan',
            'created_date',
            'created_by',
            'created_ip',
            'modified_date',
            'modified_by',
            'modified_ip',
        ],
    ]) ?>

     <p><h2>Detail Pengadaan Barang</h2></p>
    <?php $model = new \backend\models\TblTPengadaan()?>
    <?= $this->render('_form_detail', [
        'model' => $model,
    ]) ?>
</div>
