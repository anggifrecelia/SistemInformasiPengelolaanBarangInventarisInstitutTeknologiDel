<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTPencatatanAset;

/**
 * TblTPencatatanAsetSearch represents the model behind the search form about `backend\models\TblTPencatatanAset`.
 */
class TblTPencatatanAsetSearch extends TblTPencatatanAset
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pencatatan_id', 'barang_id', 'gedung_id', 'jumlah', 'satuan', 'created_by', 'modified_by', 'buku_id'], 'integer'],
            [['kode_trans', 'tanggal_trans', 'keterangan', 'sumber_perolehan', 'kondisi', 'created_date', 'created_ip', 'modified_date', 'modified_ip', 'status_pencatatan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPencatatanAset::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pencatatan_id' => $this->pencatatan_id,
            'barang_id' => $this->barang_id,
            'gedung_id' => $this->gedung_id,
            'tanggal_trans' => $this->tanggal_trans,
            'jumlah' => $this->jumlah,
            'satuan' => $this->satuan,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
            'buku_id' => $this->buku_id,
        ]);

        $query->andFilterWhere(['like', 'kode_trans', $this->kode_trans])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'sumber_perolehan', $this->sumber_perolehan])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip])
            ->andFilterWhere(['like', 'status_pencatatan', $this->status_pencatatan]);

        return $dataProvider;
    }
}
