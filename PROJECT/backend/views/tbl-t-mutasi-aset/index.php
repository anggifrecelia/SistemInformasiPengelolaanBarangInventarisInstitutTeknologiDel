<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTMutasiAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mutasi Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>


<?= $this->render('/tbl-t-mutasi-aset/_menu') ?>
    
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Daftarkan Mutasi Aset Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'mutasi_id',
            // 'pencatatan_id',
            'kode_trans',
            'tanggal_trans',



            [
                'label' => 'Lokasi Awal',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung3->nama;
                },
            ],

            [
                'label' => 'Lokasi Tujuan',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung2->nama;
                },
            ],



            'jumlah',
            [
                'attribute' => 'Status Mutasi',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status_mutasi == 1)
                        return "Berhasil";
                    else
                        return "Tidak Berhasil";
                }
            ],
            [
                'attribute' => 'Status Pindah',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status_pindah != 1)
                        return "Sudah dipindahkan";
                    else
                        return "Belum Dipindahkan";
                }
            ],


            [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                    return '<div>'.'<center>'.Html::a('Detail', ['tbl-t-mutasi-aset/view','id'=>$model->mutasi_id],['class'=>'  btn btn-success']).'</center>'.'</div>';
                },
            ],
            // 'alasan_mutasi',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            
        ],
    ]); ?>
</div>
