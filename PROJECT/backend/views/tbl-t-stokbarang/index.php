    <?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTStokbarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stok Inventory';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tstokbarang-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('/tbl-r-barang/_menu') ?>
    
   

    

    <p>
<?= Html::button('Daftarkan Barang Habis Pakai', ['value'=>Url::to('index.php?r=tbl-t-stokbarang/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>



<?php
        Modal::begin([
                'header'=>'<h4>Form Pendaftaran Barang Habis</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>

    </p>







    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'stok_id',
            

            [
                'attribute'=>'barang_id',
		'value'=>'barang.nama',

            ],


            // [
            //     'attribute' => 'Merk Barang',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         $barang = backend\models\TblRBarang::find()->where(['barang_id' => $model->barang_id])->one();
            //         if($barang !=Null){
            //             return $barang->deskripsi;
            //         }else{
            //             return Null;
            //         }
                    
            //     },
            // ],

            'jumlah',
            'available',
            'kode',
            [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                    $satuan = backend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
                    if($satuan !=Null){
                        return $satuan->nama;
                    }else{
                        return Null;
                    }
                    
                },
            ],

     [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                

                //jika tanggal hari ini lebih besar dari tanggal pengembalian
                             
                      return '<div>'.'<center>'.Html::a('Detail', ['tbl-t-stokbarang/view','id'=>$model->stok_id],['class'=>'  btn btn-success']).'</center>'.'</div>'
                    ;
                
        },

            ],


//            [
//                'attribute'=>'gedung_id',
//		'value'=>'gedung.nama',
//            ],
            
            
            // 'satuan',
            // 'deskripsi',
            // 'deleted',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            // ['class' => 'yii\grid\ActionColumn',
            //     'headerOptions' => ['width' => '30'],
            //     'template' => '{update}  {delete}{link}',

            // ],
        ],
    ]); ?>

</div>
