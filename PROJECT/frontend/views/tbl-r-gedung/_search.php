<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblRGedungSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rgedung-search">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <!--?= $form->field($model, 'gedung_id') ?-->

    <?= $form->field($model, 'nama')->textInput(['style'=>'width:300px']); ?>
    
    <?= $form->field($model, 'tipe')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'tipe_gedung')")->all(),'no', 'nama'),['prompt'=>'-Pilih Tipe Gedung-', 'style'=>'width:300px'])?>

    <?= $form->field($model, 'kelompok')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kelompok_gedung')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kelompok Gedung-', 'style'=>'width:300px'])?>

    <!--?= $form->field($model, 'deskripsi') ?-->

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
