<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TblTBukuInventory;

/**
 * TblTBukuInventorySearch represents the model behind the search form about `frontend\models\TblTBukuInventory`.
 */
class TblTBukuInventorySearch extends TblTBukuInventory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buku_id',  'lokasi', 'baik', 'rusak', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['satuan','barang_id', 'gedung_id', 'deskripsi', 'deleted', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTBukuInventory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('barang');
        $query->joinWith('gedung');
        
        // grid filtering conditions
        $query->andFilterWhere([
            'buku_id' => $this->buku_id,
            //'barang_id' => $this->barang_id,
            //'gedung_id' => $this->gedung_id,
            'lokasi' => $this->lokasi,
            'baik' => $this->baik,
            'rusak' => $this->rusak,
            'jumlah' => $this->jumlah,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'tbl_t_bukuinventory.deleted', 0])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip])
            ->andFilterWhere(['like', 'tbl_r_barang.nama', $this->barang_id])
            ->andFilterWhere(['like', 'tbl_r_gedung.nama', $this->gedung_id]);

        return $dataProvider;
    }
}
