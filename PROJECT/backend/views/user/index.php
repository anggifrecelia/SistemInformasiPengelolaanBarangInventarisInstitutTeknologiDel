<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Akun Sistem';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


     <p>
 <?= Html::button('Daftarkan User Baru', ['value'=>Url::to('index.php?r=user/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>

    <?php
        Modal::begin([
                'header'=>'<h4>Form Tambah Data User</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>

    </p>

   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',


  [
                'attribute' => 'Divisi',
                
                'value' => function($model){
                    if($model->divisi == 1){

                        return "Staf Inventory";
                    }
                    else if(($model->divisi) == 2){
                        return "Mahasiswa";
                    } 
                    else if(($model->divisi) == 3){
                        return "Dosen/Staf";
                    }
                    else{
                        return "Administrator";
                    }
                }

              ],

            'email:email',
            'username',
            'password',
            // 'divisi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
