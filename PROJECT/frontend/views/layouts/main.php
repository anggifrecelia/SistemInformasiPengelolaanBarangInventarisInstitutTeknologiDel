<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style>
    .navbar {
        background-color : #265a88;
    }

    element.style {
    }
    .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus {
        color: #fff;
        background-color: #080808;
    }
    .navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus {
        color: #fff;
        background-color: transparent;
    }
    .nav .open > a, .nav .open > a:hover, .nav .open > a:focus {
        background-color: #eee;
        border-color: #337ab7;
    }
    .navbar-inverse .navbar-nav > li > a {
        color: #9d9d9d;
    }
    .nav > li > a:hover, .nav > li > a:focus {
        text-decoration: none;
        background-color: #eee;
    }
    .dropdown-toggle:focus {
        outline: 0;
    }
    .navbar-nav > li > a {
        padding-top: 10px;
        padding-bottom: 10px;
        line-height: 20px;
    }
    .nav > li > a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }
    a:hover {
        color: #0056b3;
        text-decoration: underline;
    }
    .open > a {
        outline: 0;
    }
    a:focus {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px;
    }
    a:hover, a:focus {
        color: #23527c;
        text-decoration: underline;
    }
    a:active, a:hover {
        outline: 0;
    }
    a {
        color: #007bff;
        text-decoration: none;
        background-color: transparent;
        -webkit-text-decoration-skip: objects;
    }
    a {
        color: #337ab7;
        text-decoration: none;
    }
    a {
        background-color: transparent;
    }
    *, *::before, *::after {
        box-sizing: border-box;
    }
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    user agent stylesheet
    a:-webkit-any-link {
        color: -webkit-link;
        cursor: pointer;
        text-decoration: underline;
    }
    user agent stylesheet
    :focus {
        outline: -webkit-focus-ring-color auto 5px;
    }
    user agent stylesheet
    li {
        display: list-item;
        text-align: -webkit-match-parent;
    }
    .nav {
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    user agent stylesheet
    ul, menu, dir {
        display: block;
        list-style-type: disc;
        -webkit-margin-before: 1em;
        -webkit-margin-after: 1em;
        -webkit-margin-start: 0px;
        -webkit-margin-end: 0px;
        -webkit-padding-start: 40px;
    }
    body {
        font-family: Lato,'Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
    }
    body {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
        background-color: #fff;
    }

    .dropdown-toggle::after {

    }

    *, *::before, *::after {
        box-sizing: border-box;
    }


</style>
<body style="background-color: mintcream">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Sistem Informasi Pengelolaan Barang Inventaris',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        // ['label' => 'Home', 'url' => ['/site/index']],
        // ['label' => 'About', 'url' => ['/site/about']],
        // ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        // $menuItems[] = ['label' => 'Register', 'url' => ['/site/register']];
    } else {
        $auth = Yii::$app->getAuthManager();
        $curuser = Yii::$app->user->id;
        $menuItems[] = ['label' => 'Permintaan', 'url' => ['/tbl-t-permintaan/index']];
        $menuItems[] = ['label' => 'Peminjaman', 'url' => ['/tbl-t-peminjaman/index']];
        $menuItems[] = ['label' => 'Hi ('.Yii::$app->user->identity->username.')', 'items' => [
            ['label'=> 'Change Password', 'url' => ['/user/changepass']],
            ['label' => 'Logout',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']],
        ]];
//        }
//        $menuItems = [
//                ['label' => 'Home', 'url' => ['/site/index']],
//                ['label' => 'Admin', 'items' => [
//                    ['label' => 'Status', 'url' => ['/tbl-r-status/index']],
//                    ['label' => 'Barang', 'url' => ['/tbl-r-barang/index']],
//                    ['label' => 'Gedung', 'url' => ['/tbl-r-gedung/index']],
//                    ['label' => 'Buku Inventory', 'url' => ['/tbl-t-buku-inventory/index']],
//                ]],
//                ['label' => 'Permintaan', 'url' => ['/tbl-t-permintaan/index']],
//                ['label' => 'Peminjaman', 'url' => ['/tbl-t-peminjaman/index']],
//                ['label' => 'Hi ('.Yii::$app->user->identity->username.')', 'items' => [
//                        ['label'=> 'Change Password', 'url' => ['/user/changepass']],
//                        ['label' => 'Logout',
//                        'url' => ['/site/logout'],
//                        'linkOptions' => ['data-method' => 'post']],
//                ]],
//            ];

    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

    <!--notifikasi-->
    <?php
    if(Yii::$app->session->hasFlash('success')){
        echo '
            <div class="alert alert-success">
                <button type="button" class="close" 
                    data-dismiss="alert" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                '.Yii::$app->session->getFlash('success').'
            </div>
        ';
    }

    if(Yii::$app->session->hasFlash('info')){
        echo '
            <div class="alert alert-info">
                <button type="button" class="close" 
                    data-dismiss="alert" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                '.Yii::$app->session->getFlash('info').'
            </div>
        ';
    }

    if(Yii::$app->session->hasFlash('warning')){
        echo '
           <div class="alert alert-warning">
                <button type="button" class="close" 
                    data-dismiss="alert" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                '.Yii::$app->session->getFlash('warning').'
            </div>
        ';
    }

    if(Yii::$app->session->hasFlash('error')){
        echo '
            <div class="alert alert-error">
                <button type="button" class="close" 
                    data-dismiss="alert" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                '.Yii::$app->session->getFlash('error').'
            </div>
        ';
    }?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy;  <?= date('Y') ?></p>
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
                <li class="list-inline-item">
                    <a href="#">Team 12</a>
                </li>
                <li class="list-inline-item">&sdot;</li>
                <li class="list-inline-item">
                    <a href="#">For Institut Teknologi Del</a>
                </li>


            </ul>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
