    <?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;



/* @var $this yii\web\View */
/* @var $model frontend\models\TblTBukuInventory */


$mei2=$model->buku_id;


 $mei = (new \yii\db\Query())
                    ->select ('barang_id')
                    ->from('tbl_t_bukuinventory')
                    ->where(['buku_id' => $mei2])
                    ->scalar();

 $namaBarang = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('tbl_r_barang')
                    ->where(['barang_id' => $mei])
                    ->scalar();


                    // var_dump($namaBarang); die();


$this->title = $namaBarang;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tbuku Inventories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tbuku-inventory-view">

    <h1><?= Html::encode($this->title) ?></h1>




 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'buku_id',
            'barang_id',
            'gedung_id',
            'lokasi',
            'baik',
            'rusak',
            'jumlah',
            'satuan',
            // 'deskripsi',
            // 'deleted',
            // 'created_date',
            // 'created_by',
           
        ],
        
    ]) ?>


<p>
    
    <?= Html::button('Pinjam Sekarang', ['value'=>Url::to('index.php?r=tbl-t-peminjaman/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>


<?php
        Modal::begin([
                'header'=>'<h4>Permintaan Barang Habis</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>
</p>


</div>
