<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblRBarang;
use backend\models\TblRGedung;
use backend\models\TblRStatus;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\TblRKerusakan */
/* @var $form yii\widgets\ActiveForm */
$barang = TblRBarang::findOne(['barang_id'=>$id]);
?>

<div class="tbl-rkerusakan-form">

    <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'create-kerusakan-form'
                ]
    ]); ?>
    
    <?= '<b>Nama Barang : <b>'.$barang->nama.'</b><br></br>' ?>
    
    <?= $form->field($model, 'barang_id')->textInput(['readonly'=>true]);/* textInput(['readOnly'=>true])*/ ?>
    
    <?php $model->barang_id = $id; ?>

    <?= $form->field($model, 'berita_acara_id')->textInput() ?>

    <?= $form->field($model, 'no_laporan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>
    <?= $form->field($model, 'tanggal')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
    ]); ?>

    <?= $form->field($model, 'jumlah')->textInput() ?>

    <!--?= $form->field($model, 'satuan')->textInput(['maxlength' => true]) ?-->

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <!--?= $form->field($model, 'created_date')->textInput() ?>

    <!--?= $form->field($model, 'created_by')->textInput() ?>

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'modified_date')->textInput() ?>

    <!--?= $form->field($model, 'modified_by')->textInput() ?>

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <!--?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['id'=>$id, 'gedung_id'=>$gedung_id,'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?-->
        <?= Html::submitButton('Create',['id'=>$id, 'gedung_id'=>$gedung_id,'class' => 'btn btn-success']) ?>
        <!--?= Html::submi?-->
    </div>

    <?php ActiveForm::end(); ?>

</div>
