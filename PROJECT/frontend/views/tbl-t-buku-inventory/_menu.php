<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;

?>

<?= Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
    ],
    'items' => [
        [
            'label'   => Yii::t('app', 'Buku Inventory'),
            'url'     => ['/tbl-t-buku-inventory/index'],
        ],
        [
            'label'   => Yii::t('app', 'Pencatatan Aset'),
            'url'     => ['/tbl-t-pencatatan-aset/index'],
        ],
        [
            'label'   => Yii::t('app', 'Pelepasan Aset'),
            'url'     => ['/tbl-t-pelepasan-aset/index'],
        ],
        [
            'label' => Yii::t('app','Mutasi Aset'),
            'url'     => ['/tbl-t-mutasi-aset/index'],
        ],
        [
            'label' => Yii::t('app','Kerusakan'),
            'url'     => ['/tbl-r-kerusakan/index'],
        ],
    ],
]) ?>
