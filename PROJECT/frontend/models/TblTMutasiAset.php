<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_mutasi_aset".
 *
 * @property string $mutasi_id
 * @property integer $buku_id
 * @property string $kode_trans
 * @property string $tanggal_trans
 * @property integer $lokasi
 * @property integer $baik
 * @property integer $rusak
 * @property string $alasan_mutasi
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblTBukuinventory $buku
 */
class TblTMutasiAset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_mutasi_aset';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buku_id', 'kode_trans'], 'required'],
            [['buku_id', 'lokasi', 'baik', 'rusak', 'created_by', 'modified_by'], 'integer'],
            [['tanggal_trans', 'created_date', 'modified_date'], 'safe'],
            [['kode_trans'], 'string', 'max' => 30],
            [['alasan_mutasi'], 'string', 'max' => 255],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['buku_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTBukuinventory::className(), 'targetAttribute' => ['buku_id' => 'buku_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mutasi_id' => 'ID',
            'buku_id' => 'Buku ID',
            'kode_trans' => 'Kode Transaksi',
            'tanggal_trans' => 'Tanggal Transaksi',
            'lokasi' => 'Lokasi',
            'baik' => 'Baik',
            'rusak' => 'Rusak',
            'alasan_mutasi' => 'Alasan Mutasi',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuku()
    {
        return $this->hasOne(TblTBukuinventory::className(), ['buku_id' => 'buku_id']);
    }
}
