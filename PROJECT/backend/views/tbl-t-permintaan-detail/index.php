<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPermintaanDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Tpermintaan Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpermintaan-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Tpermintaan Detail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'permintaan_detail_id',
            'permintaan_id',
            'barang_id',
            'jumlah',
            'satuan',
             'keterangan',
             'status',
            // 'status_by',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
