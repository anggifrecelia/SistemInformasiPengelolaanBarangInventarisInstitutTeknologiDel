<?php

namespace backend\controllers;

use Yii;
use yii\data\SqlDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use kartik\mpdf\Pdf;

class LaporanBulananController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-pdf' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        if(($bulan = Yii::$app->request->post('bulan')) && ($tahun = Yii::$app->request->post('tahun'))) {//($request) {//

        } else {
            $timeZone = 'Asia/Jakarta';
            $timestamp = time();
            $dt = new \DateTime("now", new \DateTimeZone($timeZone));
            $dt->setTimestamp($timestamp);
            $bulan = $dt->format('m');
            $tahun = $dt->format('Y');
        }

        // Mengambil nama bulan
        switch ($bulan) {
            case 1 :
                $namaBulan = 'Januari';
                break;
            case 2 :
                $namaBulan = 'Februari';
                break;
            case 3 :
                $namaBulan = 'Maret';
                break;
            case 4 :
                $namaBulan = 'April';
                break;
            case 5 :
                $namaBulan = 'Mei';
                break;
            case 6 :
                $namaBulan = 'Juni';
                break;
            case 7 :
                $namaBulan = 'Juli';
                break;
            case 8 :
                $namaBulan = 'Agustus';
                break;
            case 9 :
                $namaBulan = 'September';
                break;
            case 10 :
                $namaBulan = 'Oktober';
                break;
            case 11 :
                $namaBulan = 'November';
                break;
            case 12 :
                $namaBulan = 'Desember';
                break;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT no_dokumen, pemohon, tanggal_trans, tanggal_pinjam, approval FROM tbl_t_peminjaman WHERE MONTH(tanggal_trans)=:bulan AND YEAR(tanggal_trans)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        return $this->render('index-laporan', [
            'dataProvider' => $dataProvider,
            'namaBulan' => $namaBulan,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);
    }

    public function actionGetPdf($bulan, $tahun, $namaBulan) {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT no_dokumen, pemohon, tanggal_trans, tanggal_pinjam, approval FROM tbl_t_peminjaman WHERE MONTH(tanggal_trans)=:bulan AND YEAR(tanggal_trans)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        $pdf = new Pdf([
            'filename' => 'Laporan' . $bulan . $tahun,
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('laporan', ['dataProvider' => $dataProvider, 'namaBulan' => $namaBulan, 'tahun' => $tahun]),
            'options' => [
                'title' => 'Laporan Bulanan - PDF' ,
                'subject' => 'Laporan Bulan ' . $bulan . ' ' . $tahun
            ],
            'methods' => [
                'SetHeader' => ['Bang Edu||' . $bulan . ' ' . $tahun],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }
}
