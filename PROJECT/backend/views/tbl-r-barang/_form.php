<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\TblRStatus;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRBarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rbarang-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kategori')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kategori_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kategori Barang-'])?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_register')->textInput(['maxlength' => true, 'value'=>"ITD/TRB/"]) ?>

    <?= $form->field($model, 'jenis')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'jenis_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Jenis Barang-'])?>

    
     <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>


    <!--?= $form->field($model, 'deleted')->textInput(['maxlength' => true]) ?-->



 <?= $form->field($model, 'created_date')->textInput(['maxlength' => true, 'readonly'=>true]) ?>


    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
