<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTBukuInventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Barang Pinjaman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tbuku-inventory-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
   
    
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Nama Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $barang = \backend\models\TblRBarang::find()->where(['barang_id'=>$model->barang_id])->one();
                    return $barang->nama;
                },
            ],
            [
                'attribute' => 'Gedung',
                //'format' => 'raw',
                'value' => function ($model) {
                    $gedung = backend\models\TblRGedung::find()->where(['gedung_id'=>$model->gedung_id])->one();
                    return $gedung->nama;
                },
            ],

            [
                'attribute' => 'Available',
                'format' => 'raw',
                'value' => 'available',
            ],

            [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                    $satuan = backend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
                    if($satuan !=Null){
                        return $satuan->nama;
                    }else{
                        return Null;
                    }
                    
                },
            ],

        //   [ 'attribute' => 'Keterangan',
        //         'format' => 'raw',
        //         'value' => function ($model){                             
        //               return '<div>'.'<center>'.Html::a('Pinjam Sekarang', ['tbl-t-peminjaman/index'],['class'=>'  btn btn-success']).'</center>'.'</div>'
        //             ;
                
        // },
        //     ],
        ],
    ]);
     ?>

      <div class="form-group">
        <?= Html::a('Kembali', ['tbl-t-peminjaman/index'], ['class' => 'btn btn-success']) ?>
    </div>
</div>
