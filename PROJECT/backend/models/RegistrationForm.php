<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use backend\models\User;
// use backend\models\AuthAssignment;

class RegistrationForm extends Model
{
    /**
     * inheritdoc
     */
    public $nama;
    public $email;
    public $username;
    public $password;
    public $repassword;
    public $divisi;
    
    public function rules(){
        return [
            [['username','password','repassword','nama','email','divisi'], 'required', 'on' => 'register'],

            ['username', 'filter', 'filter' => 'trim'],
            [['username'], 'string', 'max' => 16],
            [['username'], 'match','pattern' => '/^[a-z]*$/'],
            ['username', 'unique', 'targetClass' => '\backend\models\User', 'message' => 'This username has already been taken.'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\backend\models\User', 'message' => 'This email address has already been taken.'],
            
            [['password'], 'string', 'min' => 8, 'max' => 16],
            [['repassword'], 'compare', 'compareAttribute' => 'password','message' => Yii::t('app','Password must be match.')]
        ];
    }
    
    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['register'] = ['username','password','repassword','email','nama','email','divisi'];
        return $scenarios;
    }
    
    /**;
     * @inheritdoc
     */
   public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'repassword' => 'Repassword',
            'divisi' => 'Divisi',
        ];
    }
    
    public static function hashPassword($_password)
    {
        return (Yii::$app->getSecurity()->generatePasswordHash($_password));
    }
    
    public function register(){
        if($this->validate()) //rules harus dijalankan
        {
            $user = new User();
            $user->username = $this->username;
            $user->password = self::hashPassword($this->password);
            $user->nama = $this->nama;
            $user->email = $this->email;
            $user->divisi = $this->divisi;
            $user->save();
            // if($account->save()){
            //     //transaction begin
            //     $member = new Members();
            //     $member->account_id = $account->id;
            //     $member->first_name = $this->first_name;
            //     $member->last_name = $this->last_name;
            //     $member->email = $this->email;
            //     $member->address = $this->address;
            //     // $auth_assignment = new AuthAssignment();
            //     // $auth_assignment->item_name = 'member';
            //     // $auth_assignment->user_id = $account->id;
            //     if($member->save()){
            //         return $account;    //return user yang berhasil register
            //     } else {
            //         print_r($member->errors);
            //     }
            // } else {
            //     print_r($account->errors);
            return true;
            }
            return false; //null;
        }
}
?>