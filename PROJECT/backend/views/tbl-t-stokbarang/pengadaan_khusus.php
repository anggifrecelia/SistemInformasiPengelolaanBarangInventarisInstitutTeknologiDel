<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPengadaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'pengadaan_id',
            [
                'attribute' => 'Tipe Pengadaan',
                'format' => 'raw',
                'value' => function ($model) {
                    $tipe  = backend\models\TblRStatus::find()->where(['kode'=>'tipe_pengadaan_barang', 'no'=>$model->tipe])->one();
                    if($tipe != NULL){
                        return $tipe->nama;
                    }else{
                        return NULL;
                    }
                    
                },
            ],
            
            'kode_trans',
            'tanggal_trans',
              
            [
                
                'class' => 'yii\grid\ActionColumn'
            ],
        ],
    ]); ?>
    

</div>
