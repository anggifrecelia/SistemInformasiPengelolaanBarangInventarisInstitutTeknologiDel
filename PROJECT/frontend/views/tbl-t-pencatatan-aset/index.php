<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTPencatatanAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pencatatan Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('/tbl-t-buku-inventory/_menu') ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <p>
        <?= Html::a('Catat Aset', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pencatatan_id',
            [
                'attribute' => 'Nama Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $barang = \frontend\models\TblRBarang::find()->where(['barang_id'=>$model->barang_id])->one();
                    return $barang->nama;
                },
            ],
            [
                'attribute' => 'Gedung',
                'format' => 'raw',
                'value' => function ($model) {
                    $gedung = frontend\models\TblRGedung::find()->where(['gedung_id'=>$model->gedung_id])->one();
                    return $gedung->nama;
                },
            ],
            //'barang_id',
            //'gedung_id',
            'kode_trans',
            'tanggal_trans',
            'jumlah',
            //'satuan',
            [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                    $satuan = frontend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
                    return $satuan->nama;
                },
            ],
            //'sumber_perolehan',
            [
                'attribute' => 'Sumber Perolehan',
                'format' => 'raw',
                'value' => function ($model) {
                    $sumber = frontend\models\TblRStatus::find()->where(['kode'=>'sumber_perolehan', 'no' => $model->sumber_perolehan])->one();
                    return $sumber->nama;
                },
            ],
            //'kondisi',
            [
                'attribute' => 'Kondisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $kondisi = frontend\models\TblRStatus::find()->where(['kode'=>'kondisi_barang', 'no' => $model->kondisi])->one();
                    return $kondisi->nama;
                },
            ],
            'keterangan:ntext',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
