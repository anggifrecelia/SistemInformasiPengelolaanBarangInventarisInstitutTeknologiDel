<?php
$server="localhost";
$username="root";
$password="";
$database="inventory"; 
$mei = mysqli_connect($server,$username,$password)or die('tidak terkoneksi dengan db');
// mysqli_select_db($database)or die('tidak terkoneksi dengan db');
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblTPermintaanDetail;
use backend\models\TblRBarang;
use backend\models\User;
use backend\models\TblRStatus;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPermintaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$detail = new TblTPermintaanDetail();
$this->title = 'Permintaan';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="tbl-tpermintaan-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'permintaan_id',
            // 'no_dokumen',
            [
                'attribute' => 'Pemohon',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    return $pemohon->nama;
                },
            ],
            [
                'attribute' => 'Divisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    $divisi = TblRStatus::find()->where(['kode'=>'divisi', 'no'=>$pemohon->divisi])->one();
                    return $divisi->nama;
                },
            ],
//            [
//                'attribute' => 'Tipe Permintaan',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    if($model->tipe==1){
//                        return "Permintaan";
//                    }
//                    else{
//                        return "-";
//                    }
//                    return;
//                },
//            ],
//            'jumlah',
//            'satuan',
//            [
//                'attribute' => 'Nama Barang',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                        $sq=mysql_query("select * from tbl_r_barang where barang_id = $data[barang_id]");
//                        while($kk=mysql_fetch_assoc($sq))
//                        {
//                            $listnya = $listnya . $kk['nama'] . "<br>";
//                        }
//                    }
//                    return $listnya;
//                },
//            ],
            
//            [
//                'attribute' => 'Jumlah',
//                'format' => 'raw',
//                'value' => function ($model) 
//                {
//                    $listnya = "";
//                    $detail = $model->permintaan_id;
//                    $select_query=mysql_query("select * from tbl_t_permintaan_detail where permintaan_id = $detail");
//                    while($data=mysql_fetch_assoc($select_query))
//                    {
//                            $listnya = $listnya . $data['jumlah'] . "<br>";
//                    }
//                    return $listnya;
//                },
//            ],
                        
            'tanggal_trans',
                        
//            [
//                'attribute' => 'Bulan Trans',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $status = TblRStatus::find()->where(['kode' => 'bulan'])->andWhere(['no' => $model->bulan_trans])->one();
//                    return $status['nama'];
//                },
//            ],
//            [
//                'attribute' => 'Tahun Trans',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $status = TblRStatus::find()->where(['kode' => 'tahun'])->andWhere(['no' => $model->tahun_trans])->one();
//                    return $status['nama'];
//                },
//            ],
            'keterangan:ntext',
                        
            [
                'attribute' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_permintaan'])->andWhere(['no' => $detail['status']])->one();
                    return $status['nama'];
                },
            ],
            'alasan_penolakan',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
             
           

            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    
                    if($detail['status'] == 1){
                    return '<div><center>' . 

                    Html::a('Approve', ['approve','id'=>$model->permintaan_id], ['class' => 'btn btn-success', 'data' => [
                        'confirm' => 'Are you sure you want to approve this request?',
                        'method' => 'post',
                    ],
                    ])


                    .'&nbsp;' . 


                    Html::a('Reject', ['update', 'id' => $model->permintaan_id], ['class' => 'btn btn-danger', 'data' => [
                       'confirm' => 'Are you sure you want to Rejectct this request?',
                        'method' => 'post',
                    ],


                    ]). '</center></div>';
                    }


                    else{
                        return "-";
                    }
                }
            ], 
            
//            [
//                'format' => 'raw',
//                'value' => function($model){
//                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
//                    if($detail['status'] == 1){
//                    return '<div><center>' . Html::a('Delete', ['delete', 'id' => $model->permintaan_id], ['class' => 'btn btn-danger', 'data' => [
//                        'confirm' => 'Are you sure you want to delete this item?',
//                        'method' => 'post',
//                    ],
//                    ]). '</center></div>';
//                    }else{
//                        return "-";
//                    }
//                },
//            ],
            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '50'],
                'template' => '{view}{link}',
            ],
        ],
    ]); ?>

</div>
