<?php

namespace backend\controllers;

use Yii;
use backend\models\TblRBarang;
use backend\models\TblRBarangSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
/**
 * TblRBarangController implements the CRUD actions for TblRBarang model.
 */
class TblRBarangController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblRBarang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblRBarangSearch(['deleted'=>0]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
//    public function actionHabis()
//    {
//        $searchModel = new TblRBarangSearch();
//        $model = TblRBarang::find()->where(['kategori'=>1]);
//        $dataProvider = new ActiveDataProvider([
//            'query'=>$model,
//        ]);
//        
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
//    
//    public function actionAsset()
//    {
//        $searchModel = new TblRBarangSearch();
//        $model = TblRBarang::find()->where(['kategori'=>"2"]);
//        $model = TblRBarang::find()->where(['deleted'=>"0"]);
//        $dataProvider = new ActiveDataProvider([
//            'query'=>$model,
//        ]);
//        
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
    /**
     * Displays a single TblRBarang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblRBarang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
        $model = new TblRBarang();
        $model->created_by   = Yii::$app->user->id;
        $model->created_date = date('Y-m-d');
        $model->created_ip = Yii::$app->getRequest()->getUserIP();
        $model->deleted = '0';
        $model->jenis = '0';
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing TblRBarang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->modified_by = Yii::$app->user->id;
        $model->modified_date = date('Y-m-d');
        $model->modified_ip = Yii::$app->getRequest()->getUserIP();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->barang_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblRBarang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->deleted == '0'){
            $model->deleted = '1';
            $model->save();
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the TblRBarang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblRBarang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblRBarang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
