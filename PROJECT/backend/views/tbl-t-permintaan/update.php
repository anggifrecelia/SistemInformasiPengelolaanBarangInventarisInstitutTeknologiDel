<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaan */

$this->title = 'Update Permintaan: ' . ' ' . $model->permintaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpermintaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->permintaan_id, 'url' => ['view', 'id' => $model->permintaan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpermintaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
