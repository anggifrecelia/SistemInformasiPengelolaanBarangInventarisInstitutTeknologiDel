<?php
$server="localhost";
$username="root";
$password="";
$database="inventory"; 
$mei = mysqli_connect($server,$username,$password)or die('tidak terkoneksi dengan db');
// mysqli_select_db($database)or die('tidak terkoneksi dengan db');
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\TblTPermintaanDetail;
use backend\models\TblRBarang;
use backend\models\User;
use backend\models\TblRStatus;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPermintaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$detail = new TblTPermintaanDetail();
$this->title = 'Permintaan';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tbl-tpermintaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'permintaan_id',
            // 'no_dokumen',
            [
                'attribute' => 'Pemohon',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    return $pemohon->nama;
                },
            ],
            [
                'attribute' => 'Divisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    $divisi = TblRStatus::find()->where(['kode'=>'divisi', 'no'=>$pemohon->divisi])->one();
                    return $divisi->nama;
                },
            ],    
            'tanggal_trans',

            'keterangan:ntext',
                        
            [
                'attribute' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_permintaan'])->andWhere(['no' => $detail['status']])->one();
                    return $status['nama'];
                },
            ],
            'alasan_penolakan',    
            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    
                    if($detail['status'] == 1){
                    return '<div><center>' . 

                    Html::a('Terima', ['approve','id'=>$model->permintaan_id], ['class' => 'btn btn-success', 'data' => [
                        'confirm' => 'Are you sure you want to approve this request?',
                        'method' => 'post',
                    ],
                    ])


                    .'&nbsp;' . 


                    Html::a('Tolak', ['reject', 'id' => $model->permintaan_id], ['class' => 'btn btn-danger', 'data' => [
                       'confirm' => 'Are you sure you want to Rejectct this request?',
                        'method' => 'post',
                    ],


                    ]). '</center></div>';
                    }


                    else{
                        return "-";
                    }
                }
            ], 

              [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                date_default_timezone_set("Asia/Jakarta");
                $end = $model->tanggal_trans;
                $current_date = date("Y-m-d");

                //jika tanggal hari ini lebih besar dari tanggal pengembalian
                if($current_date > $end){                
                      return '<div>'.'<center>'.Html::a('Permintaan Kadaluarsa', ['tbl-t-permintaan/view','id'=>$model->permintaan_id],['class'=>' btn btn-danger']).'</center>'.'</div>'
                    ;
                }

                else{

               return '<div>'.'<center>'.Html::a('Detail Permintaan', ['tbl-t-permintaan/view','id'=>$model->permintaan_id],['class'=>' btn btn-success']).'</center>'.'</div>'
                    ;
            }
        },

            ],
        ],
    ]); ?>

</div>
