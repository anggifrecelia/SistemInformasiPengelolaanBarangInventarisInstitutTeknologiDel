<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblRBarang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rbarang-form">
    
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kategori')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kategori_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kategori Barang-'])?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_register')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'jenis_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Jenis Barang-'])?>

    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'deleted')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'created_date')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
