<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPengadaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpengadaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipe')->textInput() ?>

    <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_trans')->textInput() ?>

    <?= $form->field($model, 'bulan_trans')->textInput() ?>

    <?= $form->field($model, 'tahun_trans')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modified_date')->textInput() ?>

    <?= $form->field($model, 'modified_by')->textInput() ?>

    <?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
