<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\AuthAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Member';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--?= Html::a('Register Member', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'item_name',
            [
                'label' => 'Nama Member',
                'value' => function($data){
                            
                            return Yii::$app->db->createCommand('SELECT nama FROM user where id = ' . $data['user_id'])
                            ->queryScalar();
                }
            ],
                    [
                'label'  => 'Divisi',
                'value' => function ($data) {
                            $user = backend\models\User::findOne(['id' => $data['user_id']]);

                            $tempStatus = \backend\models\TblRStatus::findOne(['no' => $user->divisi , 'kode' => 'divisi']);

                            return $tempStatus->nama;
                }
            ],
//            'user_id',
//            'created_at',

//            ['class' => 'yii\grid\ActionColumn'],
              ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '50'],
                'template' => '{delete}{link}',
            ],
        ],
    ]); ?>
</div>
