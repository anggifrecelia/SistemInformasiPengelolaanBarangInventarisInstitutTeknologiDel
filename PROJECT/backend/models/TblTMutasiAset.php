<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_mutasi_aset".
 *
 * @property string $mutasi_id
 * @property integer $pencatatan_id
 * @property string $kode_trans
 * @property string $tanggal_trans
 * @property string $lokasi
 * @property string $lokasi_tujuan
 * @property integer $jumlah
 * @property string $alasan_mutasi
 * @property string $status_mutasi
 * @property string $status_pindah
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 * @property integer $barang_id
 *
 * @property TblTPencatatanAset $pencatatan
 * @property User $createdBy
 */
class TblTMutasiAset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_mutasi_aset';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pencatatan_id', 'kode_trans', 'tanggal_trans', 'lokasi', 'lokasi_tujuan', 'jumlah', 'alasan_mutasi', 'created_date', 'created_by'], 'required'],
            [['pencatatan_id', 'jumlah', 'created_by', 'modified_by', 'barang_id'], 'integer'],
            [['tanggal_trans', 'created_date', 'modified_date'], 'safe'],
            [['kode_trans'], 'string', 'max' => 30],
            [['lokasi', 'lokasi_tujuan', 'alasan_mutasi', 'status_mutasi', 'status_pindah'], 'string', 'max' => 255],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['pencatatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTPencatatanAset::className(), 'targetAttribute' => ['pencatatan_id' => 'pencatatan_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mutasi_id' => 'Mutasi ID',
            'pencatatan_id' => 'Pencatatan Aset',
            'kode_trans' => 'Kode Mutasi',
            'tanggal_trans' => 'Tanggal Mutasi',
            'lokasi' => 'Lokasi Awal',
            'lokasi_tujuan' => 'Lokasi Tujuan',
            'jumlah' => 'Jumlah',
            'alasan_mutasi' => 'Alasan Mutasi',
            'status_mutasi' => 'Status Mutasi',
            'status_pindah' => 'Status Pindah',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
            'barang_id' => 'Barang ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPencatatan()
    {
        return $this->hasOne(TblTPencatatanAset::className(), ['pencatatan_id' => 'pencatatan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getGedung3()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'lokasi']);
    }

    public function getGedung2()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'lokasi_tujuan']);
    }
}
