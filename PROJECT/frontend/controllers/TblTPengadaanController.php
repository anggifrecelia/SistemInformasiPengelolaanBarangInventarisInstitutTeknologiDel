<?php

namespace frontend\controllers;
 
use Yii;
use frontend\models\TblTPengadaan;
use frontend\models\TblTPengadaanSearch;
use yii\web\Controller;
use frontend\models\Model;
use frontend\models\TblTPengadaanDetail;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use frontend\models\TblTStokbarang;
use kartik\typeahead\Typeahead;

/**
 * TblTPengadaanController implements the CRUD actions for TblTPengadaan model.
 */
class TblTPengadaanController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPengadaan models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TblTPengadaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPengadaan model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $model = TblTPengadaan::find()->where(['pengadaan_id'=>$id]);
        $dataProviderPengadaan = new \yii\data\ActiveDataProvider(
                [
                    'query'=>$model,
                ]
                );
        
        $models1TblPengadaanDetail = TblTPengadaanDetail::find()->where(['pengadaan_id'=>$id]);
        
        $dataProviderPengadaanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPengadaanDetail,
        ]);
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProviderPengadaanDetail'=>$dataProviderPengadaanDetail,
        ]);
    }

    /**
     * Creates a new TblTPengadaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTPengadaan();
        $models1TblPengadaanDetail = [new TblTPengadaanDetail];
        $searchModel = new TblTPengadaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $modelsTblPengadaanDetail= Model::createMultiple(TblTPengadaanDetail::classname());
            Model::loadMultiple($modelsTblPengadaanDetail, Yii::$app->request->post());
        
        if ($model->load(Yii::$app->request->post()))
            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsTblPengadaanDetail);

            if (!$valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsTblPengadaanDetail as $modelTblTPengadaanDetail) {
                            $modelTblTPengadaanDetail->pengadaan_id = $model->pengadaan_id;                           
                            $barang = TblTStokbarang::find()->where(['barang_id' => $modelTblTPengadaanDetail->barang_id] )->one();////                         
                            $barang->jumlah = $barang->jumlah + $modelTblTPengadaanDetail->jumlah;////                          
                            $barang->save();
                            
                            if (! ($flag = $modelTblTPengadaanDetail->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }                     
                    }
                    if ($flag) {
                        $transaction->commit();
                             return $this->redirect(['tbl-t-pengadaan/index']);              
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
        } else {
            return $this->render('create', [
                'model' => $model,
                 'models1TblTPengadaanDetail'=> (empty($models1TblTPengadaanDetail)) ? [new TblTPengadaanDetail] : $models1TblTPengadaanDetail
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pengadaan_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPengadaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblTPengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = TblTPengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
      public function actionApprove($id_barang, $id_jumlah) {
        //die($id_barang. $id_jumlah);
        // $stock_barang = Yii::$app->db->createCommand('Select jumlah from tbl_t_stokbarang where barang_id=' . $barang_id)->queryScalar();
        $stock_barang = \frontend\models\Stokbarang::findOne(['barang_id' => $id_barang]);
        $new_stock = $stock_barang->jumlah + $id_jumlah;
        $stock_barang->jumlah = $new_stock;
        $stock_barang->save();
        if ($stock_barang->save()) {
            return $this->redirect(Url::to(['pengadaan/index', 'status' => 'success']));
        } else {
            return $this->redirect(Url::to(['pengadaan/index', 'status' => 'gagal']));
        }
    }


}
