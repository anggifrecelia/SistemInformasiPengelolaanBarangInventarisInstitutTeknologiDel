<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRBarang */



$mei2=$model->barang_id;

// var_dump($mei2); die();

//  $mei = (new \yii\db\Query())
//                     ->select ('barang_id')
//                     ->from('tbl_t_stokbarang')
//                     ->where(['stok_id' => $mei2])
//                     ->scalar();

 $namaBarang = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('tbl_r_barang')
                    ->where(['barang_id' => $mei2])
                    ->scalar();
// var_dump($namaBarang);die();




$this->title = $namaBarang;
$this->params['breadcrumbs'][] = ['label' => 'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rbarang-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->barang_id], ['class' => 'btn btn-primary']) ?>

 <?= Html::button('Daftarkan Barang Baru', ['value'=>Url::to('index.php?r=tbl-r-barang/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>

 <?php
        Modal::begin([
                'header'=>'<h4>Edit Data Master Barang</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>





        <?= Html::a('Delete', ['delete', 'id' => $model->barang_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'barang_id',
            'kategori',
            'nama',
            'no_register',
            'jenis',
            'Merek',
            'deskripsi',
            // 'deleted',
            'created_date',
            'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
        ],
    ]) ?>

</div>
