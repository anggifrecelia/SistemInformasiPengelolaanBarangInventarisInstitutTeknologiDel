<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblRBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rbarang-index">

    <h1><?= Html::encode($this->title) ?></h1>  
        
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>



 <?= Html::button('Daftarkan Barang Baru', ['value'=>Url::to('index.php?r=tbl-r-barang/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>

    <?php
        Modal::begin([
                'header'=>'<h4>Pendaftaran Barang Baru</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>








    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],
//          'barang_id',
            'nama',

            [
                'attribute' => 'Kategori Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $kategori = \backend\models\TblRStatus::find()->where(['kode'=>'kategori_barang', 'no'=>$model->kategori])->one();
                    return $kategori->nama;
                },
            ],

            'no_register',

            // [
            //     'attribute' => 'Jenis Barang Habis',
            //     'format' => 'raw',
            //     'value' => function ($model) {
            //         $jenis = \backend\models\TblRStatus::find()->where(['kode'=>'jenis_barang', 'no'=>$model->jenis])->one();
            //         return $jenis->nama;
            //     },
            // ],

             'deskripsi',
//            [
//                'format' => 'raw',
//                'value' => function($model){
//                    return '<div><center>' . Html::a('Delete', ['delete', 'id' => $model->barang_id], ['class' => 'btn btn-danger', 'data' => [
//                        'confirm' => 'Are you sure you want to delete this item?',
//                        'method' => 'post',
//                    ],
//                    ]). '</center></div>';
//                },
//            ],
            // 'deleted',
             'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            //['class' => 'yii\grid\ActionColumn'],


            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '30'],
                'template' => '{update}  {view}  {delete}{link}',
            ],






        ],
    ]);
    ?>

</div>
