<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TblRStatus;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRGedung */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-rgedung-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipe')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'tipe_gedung')")->all(),'no', 'nama'),['prompt'=>'-Pilih Tipe Gedung-'])?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kelompok')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kelompok_gedung')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kelompok Gedung-'])?>

    <?= $form->field($model, 'deskripsi')->textarea(['maxlength' => true]) ?>

    <!--?= $form->field($model, 'deleted')->textInput(['maxlength' => true]) ?-->

    <?= $form->field($model, 'created_date')->textInput(['maxlength' => true, 'readonly'=>true]) ?>


<!-- 
    <?= $form->field($model, 'created_date')->widget(
            DatePicker::classname(), [
        'name' => 'dp_3',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => '23-Februari-1998',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ],
    ]);
    ?> -->


    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
 