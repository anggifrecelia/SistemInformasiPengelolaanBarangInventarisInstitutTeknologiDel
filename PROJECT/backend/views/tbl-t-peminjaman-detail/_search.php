<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPeminjamanDetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpeminjaman-detail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'peminjaman_detail_id') ?>

    <?= $form->field($model, 'peminjaman_id') ?>

    <?= $form->field($model, 'barang_id') ?>

    <?= $form->field($model, 'jumlah') ?>

    <?= $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'status_pinjam') ?>

    <?php // echo $form->field($model, 'status_kembali') ?>

    <?php // echo $form->field($model, 'status_kembali_by') ?>

    <?php // echo $form->field($model, 'tanggal_kembali') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
