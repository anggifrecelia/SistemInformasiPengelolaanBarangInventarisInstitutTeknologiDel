<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRGedung */

$this->title = 'Edit Gedung: ' . ' ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Gedung', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gedung_id, 'url' => ['view', 'id' => $model->gedung_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-rgedung-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
