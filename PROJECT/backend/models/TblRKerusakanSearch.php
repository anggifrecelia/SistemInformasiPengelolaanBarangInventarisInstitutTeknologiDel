<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblRKerusakan;

/**
 * TblRKerusakanSearch represents the model behind the search form about `backend\models\TblRKerusakan`.
 */
class TblRKerusakanSearch extends TblRKerusakan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kerusakan_id', 'berita_acara_id', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['no_laporan', 'barang_id','tanggal', 'satuan', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblRKerusakan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('barang');

        $query->andFilterWhere([
            'kerusakan_id' => $this->kerusakan_id,
//            'barang_id' => $this->barang_id,
            'berita_acara_id' => $this->berita_acara_id,
            'tanggal' => $this->tanggal,
            'jumlah' => $this->jumlah,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'no_laporan', $this->no_laporan])
            ->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip])
            ->andFilterWhere(['like', 'tbl_r_barang.nama', $this->barang_id]);

        return $dataProvider;
    }
}
