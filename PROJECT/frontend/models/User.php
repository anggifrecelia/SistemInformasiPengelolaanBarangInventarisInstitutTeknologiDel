<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $nama
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $divisi
 *
 * @property Dii[] $diis
 * @property Dii[] $diis0
 * @property Status $divisi0
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    //Validates password
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        //return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['divisi'], 'integer'],
            [['nama', 'email', 'username', 'password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'divisi' => 'Divisi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiis()
    {
        return $this->hasMany(Dii::className(), ['createdBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiis0()
    {
        return $this->hasMany(Dii::className(), ['modifiedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivisi0()
    {
        return $this->hasOne(Status::className(), ['id' => 'divisi']);
    }
}
