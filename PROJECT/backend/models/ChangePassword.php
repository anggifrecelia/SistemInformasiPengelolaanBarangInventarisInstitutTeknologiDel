<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use backend\models\User;

class ChangePassword extends Model {
	/**
     * @inheritdoc
     */
    public $current_password;
    public $new_password;
    public $retype_password;

    public function rules() {
        return[
            //untuk validasi field tidak bisa kosong
            [['current_password', 'new_password', 'retype_password'], 'required'],
            [['retype_password'], 'compare', 'compareAttribute' => 'new_password', 'message' => Yii::t('app', 'Password must be match')],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['register'] = ['current_password', 'new_password', 'retype_password'];
        return $scenarios;
    }

    public function attributeLabels() {
        return[
            'current_password' => 'Current Password',
            'new_password' => 'New Password',
            'retype_password' => 'Retype New Password',
        ];
    }
}

?>