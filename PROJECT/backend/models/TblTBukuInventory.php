<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_bukuinventory".
 *
 * @property integer $buku_id
 * @property integer $barang_id
 * @property integer $gedung_id
 * @property integer $lokasi
 * @property string $no_register
 * @property integer $baik
 * @property integer $rusak
 * @property integer $jumlah
 * @property integer $satuan
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 * @property string $merk
 * @property integer $pencatatan_id
 * @property integer $available
 *
 * @property TblRBarang $barang
 * @property TblRGedung $gedung
 * @property TblTPencatatanAset $pencatatan
 */
class TblTBukuinventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_bukuinventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang_id', 'gedung_id'], 'required'],
            [['barang_id', 'gedung_id', 'lokasi', 'baik', 'rusak', 'jumlah', 'satuan', 'created_by', 'modified_by', 'pencatatan_id', 'available'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['no_register', 'merk'], 'string', 'max' => 50],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
            [['gedung_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRGedung::className(), 'targetAttribute' => ['gedung_id' => 'gedung_id']],
            [['pencatatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblTPencatatanAset::className(), 'targetAttribute' => ['pencatatan_id' => 'pencatatan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'buku_id' => 'Buku ID',
            'barang_id' => 'Barang ID',
            'gedung_id' => 'Gedung ID',
            'lokasi' => 'Lokasi',
            'no_register' => 'No Register',
            'baik' => 'Baik',
            'rusak' => 'Rusak',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
            'merk' => 'Merk',
            'pencatatan_id' => 'Pencatatan ID',
            'available' => 'Available',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGedung()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'gedung_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPencatatan()
    {
        return $this->hasOne(TblTPencatatanAset::className(), ['pencatatan_id' => 'pencatatan_id']);
    }
}
