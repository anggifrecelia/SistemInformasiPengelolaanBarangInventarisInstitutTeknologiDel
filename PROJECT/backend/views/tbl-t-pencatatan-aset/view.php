<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPencatatanAset */

$this->title = $model->pencatatan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpencatatan Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
          <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Update', ['update', 'pencatatan_id' => $model->pencatatan_id, 'created_date' => $model->created_date, 'created_by' => $model->created_by], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::a('Delete', ['delete', 'pencatatan_id' => $model->pencatatan_id, 'created_date' => $model->created_date, 'created_by' => $model->created_by], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pencatatan_id',
            [
                'attribute' => 'barang_id',
                'value' => $model->barang->nama,
            ],


            [
                'attribute' => 'gedung_id',
                'value' => $model->gedung->nama,
            ],

            'kode_trans',
            'tanggal_trans',
            'jumlah',
            // 'satuan',
            'keterangan:ntext',
            // 'sumber_perolehan',
            // 'kondisi',
            'created_date',
            'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            // 'buku_id',
            'status_pencatatan',
        ],
    ]) ?>

</div>
