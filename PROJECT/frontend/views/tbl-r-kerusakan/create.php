<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TblRKerusakan */

$this->title = 'Kerusakan';
$this->params['breadcrumbs'][] = ['label' => 'Laporkan Kerusakan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rkerusakan-create">

    <!--h1><!--?= Html::encode($this->title) ?></h1-->

    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
        'gedung_id' => $gedung_id,
    ]) ?>

</div>
