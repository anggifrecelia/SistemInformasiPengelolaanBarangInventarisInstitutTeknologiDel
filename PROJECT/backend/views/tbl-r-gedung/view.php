<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TblRGedung */




$mei2=$model->gedung_id;

// var_dump($mei2); die();

//  $mei = (new \yii\db\Query())
//                     ->select ('barang_id')
//                     ->from('tbl_t_stokbarang')
//                     ->where(['stok_id' => $mei2])
//                     ->scalar();

 $namaBarang = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('tbl_r_gedung')
                    ->where(['gedung_id' => $mei2])
                    ->scalar();
// var_dump($namaBarang);die();


$this->title = $namaBarang;
$this->params['breadcrumbs'][] = ['label' => 'Gedung', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-rgedung-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->gedung_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->gedung_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('Lihat Barang Inventaris', ['create'], ['class' => 'btn btn-success']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'gedung_id',
            'tipe',
            'nama',
            'kelompok',
            'deskripsi',
            // 'deleted',
            'created_date',
            'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
        ],
    ]) ?>

</div>
