<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_stokbarang".
 *
 * @property integer $stok_id
 * @property integer $barang_id
 * @property integer $gedung_id
 * @property string $kode
 * @property integer $jumlah
 * @property string $satuan
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRBarang $barang
 * @property TblRGedung $gedung
 */
class TblTStokbarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_stokbarang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang_id', 'gedung_id', 'jumlah', 'satuan'], 'required'],
            [['barang_id', 'gedung_id', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['kode'], 'string', 'max' => 50],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stok_id' => 'Stok ID',
            'barang_id' => 'Nama Barang',
            'gedung_id' => 'Nama Gedung',
            'kode' => 'Kode',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGedung()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'gedung_id']);
    }
}
