<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_t_bukuinventory".
 *
 * @property integer $buku_id
 * @property integer $barang_id
 * @property integer $gedung_id
 * @property integer $lokasi
 * @property integer $baik
 * @property integer $rusak
 * @property integer $jumlah
 * @property string $satuan
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRBarang $barang
 * @property TblRGedung $gedung
 * @property TblTMutasiAset[] $tblTMutasiAsets
 */
class TblTBukuInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_t_bukuinventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang_id', 'gedung_id'], 'required'],
            [['barang_id', 'gedung_id', 'lokasi', 'baik', 'rusak', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
            [['gedung_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRGedung::className(), 'targetAttribute' => ['gedung_id' => 'gedung_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'buku_id' => 'Buku ID',
            'barang_id' => 'Nama Barang',
            'gedung_id' => 'Gedung',
            'lokasi' => 'Lokasi',
            'baik' => 'Baik',
            'rusak' => 'Rusak',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGedung()
    {
        return $this->hasOne(TblRGedung::className(), ['gedung_id' => 'gedung_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTMutasiAsets()
    {
        return $this->hasMany(TblTMutasiAset::className(), ['buku_id' => 'buku_id']);
    }
}
