<?php 

use yii\helpers\Html;
use yii\bootstrap\ActiveRecord;
use yii\widgets\ActiveForm;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="site-signup">
	<h1> <?= Html::encode($this->title) ?> </h1>
	<?php
		if ($status == 'success'){
			echo "<div class='alert alert-success'>Your password has been changed</div>";
		} else if ($status == 'failed'){
			echo "<div class='alert alert-danger'>Change password is failed</div>";
		}
	?>

	<p> Please fill out the following fields to change password </p>

	<div class="row">
		<div class="col-lg-5">

			<?php $form = ActiveForm::begin(); ?>

			<?= $form->field($model, 'current_password')->passwordInput() ?>

			<?= $form->field($model, 'new_password')->passwordInput() ?>

			<?= $form->field($model, 'retype_password')->passwordInput() ?>

			<div class="form-group">
				<?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
			</div>

		<?php ActiveForm::end(); ?>

		</div>
	</div>
</div>