<?php

namespace backend\controllers;

use Yii;
use yii\data\SqlDataProvider;
use backend\models\TblRKerusakan;
use backend\models\TblRKerusakanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
/**
 * TblRKerusakanController implements the CRUD actions for TblRKerusakan model.
 */
class TblRKerusakanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblRKerusakan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblRKerusakanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblRKerusakan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblRKerusakan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblRKerusakan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kerusakan_id]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
                'id' => $id,
                'gedung_id' => $gedung_id,
            ]);
        }
    }
    
//    public function actionCreate()
//    {
//        $model = new TblRKerusakan();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->kerusakan_id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//                'id' => $id,
//                'gedung_id' => $gedung_id,
//            ]);
//        }
//    }
    
    public function actionCreate_Kerusakan($id)
    {
        $model = new TblRKerusakan();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->kerusakan_id]);
        } 
        else {
            return $this->render('create', [
                'model' => $model,
                'id' => $id,
            ]);
        }
    }
    
    /**
     * Updates an existing TblRKerusakan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kerusakan_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblRKerusakan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblRKerusakan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblRKerusakan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblRKerusakan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     public function actionLaporanIndex()
    {

        if(($bulan = Yii::$app->request->post('bulan')) && ($tahun = Yii::$app->request->post('tahun'))) {//($request) {//

        } else {
            $timeZone = 'Asia/Jakarta';
            $timestamp = time();
            $dt = new \DateTime("now", new \DateTimeZone($timeZone));
            $dt->setTimestamp($timestamp);
            $bulan = $dt->format('m');
            $tahun = $dt->format('Y');
        }

        // Mengambil nama bulan
        switch ($bulan) {
            case 1 :
                $namaBulan = 'Januari';
                break;
            case 2 :
                $namaBulan = 'Februari';
                break;
            case 3 :
                $namaBulan = 'Maret';
                break;
            case 4 :
                $namaBulan = 'April';
                break;
            case 5 :
                $namaBulan = 'Mei';
                break;
            case 6 :
                $namaBulan = 'Juni';
                break;
            case 7 :
                $namaBulan = 'Juli';
                break;
            case 8 :
                $namaBulan = 'Agustus';
                break;
            case 9 :
                $namaBulan = 'September';
                break;
            case 10 :
                $namaBulan = 'Oktober';
                break;
            case 11 :
                $namaBulan = 'November';
                break;
            case 12 :
                $namaBulan = 'Desember';
                break;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT barang_id, no_laporan, tanggal, jumlah, satuan, keterangan FROM tbl_r_kerusakan WHERE MONTH(tanggal)=:bulan AND YEAR(tanggal)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        return $this->render('index-laporan', [
            'dataProvider' => $dataProvider,
            'namaBulan' => $namaBulan,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);
    }


    public function actionGetPdf($bulan, $tahun, $namaBulan) {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT barang_id, no_laporan, tanggal,jumlah, satuan, keterangan  FROM tbl_r_kerusakan WHERE MONTH(tanggal)=:bulan AND YEAR(tanggal)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        $pdf = new Pdf([
            'filename' => 'Laporan' . $bulan . $tahun,
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('laporan', ['dataProvider' => $dataProvider, 'namaBulan' => $namaBulan, 'tahun' => $tahun]),
            'options' => [
                'title' => 'Laporan Bulanan - PDF' ,
                'subject' => 'Laporan Bulan ' . $bulan . ' ' . $tahun
            ],
            'methods' => [
                'SetHeader' => ['Laporan Permintaan||' . $bulan . ' ' . $tahun],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }
}
