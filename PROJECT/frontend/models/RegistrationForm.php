<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\User;
use common\models\AuthAssignment;
// use frontend\models\AuthAssignment;

class RegistrationForm extends Model
{
    /**
     * inheritdoc
     */
    public $nama;
    public $email;
    public $username;
    public $password;
    public $repassword;
    public $divisi;
    public $verifyCode;
    
    public function rules(){
        return [
            [['username','password','repassword','nama','email','divisi'], 'required', 'on' => 'register'],

            ['username', 'filter', 'filter' => 'trim'],
            [['username'], 'string', 'max' => 16],
            [['username'], 'match','pattern' => '/^[a-z]*$/'],
            ['username', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This username has already been taken.'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This email address has already been taken.'],
            ['verifyCode','captcha'],
            
            [['password'], 'string', 'min' => 8, 'max' => 16],
            [['repassword'], 'compare', 'compareAttribute' => 'password','message' => Yii::t('app','Password must be match.')]
        ];
    }
    
    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['register'] = ['username','password','repassword','email','nama','email','divisi','verifyCode'];
        return $scenarios;
    }
    
    /**;
     * @inheritdoc
     */
   public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'repassword' => 'Repassword',
            'divisi' => 'Divisi',
        ];
    }
    
    public static function hashPassword($_password)
    {
        return (Yii::$app->getSecurity()->generatePasswordHash($_password));
    }
    
    public function register(){
        if($this->validate()) //rules harus dijalankan
        {
            $user = new User();
            $user->username = $this->username;
            $user->password = self::hashPassword($this->password);
            $user->nama = $this->nama;
            $user->email = $this->email;
            $user->divisi = $this->divisi;
            $user->save();
            if($user->save(true))
            {
                $auth = Yii::$app->authManager;
                if($user->divisi == 1)
                    $authorRole = $auth->getRole('staff_inventory');
                else
                    $authorRole = $auth->getRole('staff');
                
                $auth->assign($authorRole, $user->getId());
            }
            return true;
            }
            return false; //null;
        }
}
?>