<?php

namespace frontend\models;

use Yii;
/**
 * This is the model class for table "tbl_r_barang".
 *
 * @property integer $barang_id
 * @property integer $kategori
 * @property string $nama
 * @property string $no_register
 * @property string $jenis
 * @property string $deskripsi
 * @property string $deleted
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 *
 * @property TblRKerusakan[] $tblRKerusakans
 * @property TblTPengadaanDetail[] $tblTPengadaanDetails
 * @property TblTPengeluaranDetail[] $tblTPengeluaranDetails
 * @property TblTPermintaanDetail[] $tblTPermintaanDetails
 * @property TblTStokbarang[] $tblTStokbarangs
 */
class TblRBarang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_r_barang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori', 'nama'], 'required'],
            [['kategori', 'created_by', 'modified_by'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['nama', 'no_register'], 'string', 'max' => 50],
            [['jenis'], 'string', 'max' => 30],
            [['deskripsi'], 'string', 'max' => 255],
            [['deleted'], 'string', 'max' => 1],
            [['created_ip', 'modified_ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'barang_id' => 'Barang ID',
            'kategori' => 'Kategori',
            'nama' => 'Nama',
            'no_register' => 'No Register',
            'jenis' => 'Jenis',
            'deskripsi' => 'Deskripsi',
            'deleted' => 'Deleted',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRKerusakans()
    {
        return $this->hasMany(TblRKerusakan::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPengadaanDetails()
    {
        return $this->hasMany(TblTPengadaanDetail::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPengeluaranDetails()
    {
        return $this->hasMany(TblTPengeluaranDetail::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTPermintaanDetails()
    {
        return $this->hasMany(TblTPermintaanDetail::className(), ['barang_id' => 'barang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTStokbarangs()
    {
        return $this->hasMany(TblTStokbarang::className(), ['barang_id' => 'barang_id']);
    }
    
}
