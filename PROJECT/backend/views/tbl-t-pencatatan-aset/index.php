<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\bootstrap\Modal;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPencatatanAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pencatatan Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('/tbl-t-pencatatan-aset/_menu') ?>

  

      <p>
<?= Html::button('Daftarkan Barang Aset Baru', ['value'=>Url::to('index.php?r=tbl-t-pencatatan-aset/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>



<?php
        Modal::begin([
                'header'=>'<h4>Form Pencatatan Barang Aset</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pencatatan_id',
             [
                'attribute' => 'barang_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->barang->nama;
                },
            ],

             [
                'attribute' => 'gedung_id',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung->nama;
                },
            ],
      
            'kode_trans',
            'tanggal_trans',
            'jumlah',
            // 'satuan',


  [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                    return '<div>'.'<center>'.Html::a('Detail', ['tbl-t-pencatatan-aset/view','id'=>$model->pencatatan_id],['class'=>'  btn btn-success']).'</center>'.'</div>';
                },
            ],


            // 'keterangan:ntext',
            // 'sumber_perolehan',
            // 'kondisi',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            // 'buku_id',
            // 'status_pencatatan',

            
        ],
    ]); ?>
</div>
