<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTPermintaan;

/**
 * TblTPermintaanSearch represents the model behind the search form about `backend\models\TblTPermintaan`.
 */
class TblTPermintaanSearch extends TblTPermintaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permintaan_id', 'tipe', 'pemohon', 'bulan_trans', 'created_by', 'modified_by'], 'integer'],
            [['tanggal_trans', 'tahun_trans', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPermintaan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'permintaan_id' => SORT_DESC, 
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'permintaan_id' => $this->permintaan_id,
            'tipe' => $this->tipe,
            'pemohon' => $this->pemohon,
            'tanggal_trans' => $this->tanggal_trans,
            'bulan_trans' => $this->bulan_trans,
            'tahun_trans' => $this->tahun_trans,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
            'modified_ip' => $this->modified_ip,
        ]);

        $query->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip]);

        return $dataProvider;
    }
}
