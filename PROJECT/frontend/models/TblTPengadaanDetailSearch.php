<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TblTPengadaanDetail;

/**
 * TblTPengadaanDetailSearch represents the model behind the search form about `s\models\TblTPengadaanDetail`.
 */
class TblTPengadaanDetailSearch extends TblTPengadaanDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengadaan_detail_id', 'pengadaan_id', 'barang_id', 'jumlah', 'created_by', 'modified_by'], 'integer'],
            [['satuan', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPengadaanDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'pengadaan_detail_id' => $this->pengadaan_detail_id,
            'pengadaan_id' => $this->pengadaan_id,
            'barang_id' => $this->barang_id,
            'jumlah' => $this->jumlah,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
