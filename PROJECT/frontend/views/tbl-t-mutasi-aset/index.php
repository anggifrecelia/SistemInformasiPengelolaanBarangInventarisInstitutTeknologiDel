<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTMutasiAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mutasi Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('/tbl-t-buku-inventory/_menu') ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--?= Html::a('Tambahkan Mutasi Aset', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'mutasi_id',
            //'buku_id',
            'kode_trans',
            'tanggal_trans',
            //'lokasi',
//            [
//                'attribute' => 'Lokasi',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $gedung = frontend\models\TblRGedung::find()->where(['gedung_id'=>$model->lokasi])->one();
//                    return $gedung->nama;
//                },
//            ],
            'baik',
            'rusak',
            'alasan_mutasi',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
