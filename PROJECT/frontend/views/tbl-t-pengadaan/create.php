<?php

use yii\helpers\Html;
use frontend\models\TblTPengadaanDetail;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPengadaan */
$modelsTblTPengadaanDetail = [new TblTPengadaanDetail];
// $this->title = 'Create Pengadaan Barang ';
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpengadaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_detail', [
        'model' => $model,
        'modelsTblTPengadaanDetail' => (empty($modelsTblTPengadaanDetail)) ? [new TblTPengadaanDetail] : $modelsTblTPengadaanDetail
    ]) ?>

</div>
