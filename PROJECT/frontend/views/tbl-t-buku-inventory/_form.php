<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\TblRBarang;
use frontend\models\TblRGedung;
use frontend\models\TblRStatus;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblTBukuInventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tbuku-inventory-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--?= $form->field($model, 'barang_id')->dropDownList(ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),['prompt'=>'-Pilih Nama Barang-']) ?-->

    <?= $form->field($model, 'barang_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblRBarang::find()->all(),'barang_id', 'nama'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Barang -'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>
    
    <?= $form->field($model, 'gedung_id')->dropDownList(ArrayHelper::map(TblRGedung::find()->all(),'gedung_id', 'nama'),['prompt'=>'-Pilih Gedung-']) ?>

    <!--?= $form->field($model, 'lokasi')->textInput() ?-->

    <?= $form->field($model, 'baik')->textInput() ?>

    <?= $form->field($model, 'rusak')->textInput() ?>

    <!--?= $form->field($model, 'jumlah')->textInput() ?-->

    <?= $form->field($model, 'satuan')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'satuan_barang')")->all(), 'nama', 'nama'), ['prompt' => '-Pilih Satuan']) ?>

    <?= $form->field($model, 'deskripsi')->textarea() ?>

    <!--?= $form->field($model, 'deleted')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'created_date')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput() ?-->

    <!--?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'modified_date')->textInput() ?-->

    <!--?= $form->field($model, 'modified_by')->textInput() ?-->

    <!--?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
