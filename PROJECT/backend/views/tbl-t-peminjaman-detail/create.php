<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTPeminjamanDetail */

$this->title = 'Create Tbl Tpeminjaman Detail';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpeminjaman Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpeminjaman-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
