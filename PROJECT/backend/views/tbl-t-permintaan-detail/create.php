<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblTPermintaanDetail */

$this->title = 'Create Tbl Tpermintaan Detail';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpermintaan Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpermintaan-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
