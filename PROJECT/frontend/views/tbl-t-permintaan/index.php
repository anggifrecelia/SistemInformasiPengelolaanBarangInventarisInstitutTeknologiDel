<?php
$server="localhost";
$username="root";
$password="";
$database="inventory"; 
$mei = mysqli_connect($server,$username,$password)or die('tidak terkoneksi dengan db');
// mysql_select_db($database)or die('tidak terkoneksi dengan db');
use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\TblTPermintaanDetail;
use frontend\models\TblRBarang;
use frontend\models\User;
use frontend\models\TblRStatus;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\models\TblTStokBarang;


/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTPermintaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$detail = new TblTPermintaanDetail();
$this->title = 'Permintaan';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tbl-tpermintaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>


 <?= Html::button('Buat Permintaan Baru ', ['value'=>Url::to('index.php?r=tbl-t-permintaan/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>

    <?php
        Modal::begin([
                'header'=>'<h4>Form Permintaan Barang Habis</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>
<p>
 <?= Html::a('Lihat Daftar Barang Habis', ['tbl-t-stokbarang/index'], ['class' => 'btn btn-success'])  ?>   
</p>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Pemohon',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    return $pemohon->nama;
                },
            ],
            [
                'attribute' => 'Divisi',
                'format' => 'raw',
                'value' => function ($model) {
                    $pemohon = User::find()->where(['id'=>$model->pemohon])->one();
                    $divisi = TblRStatus::find()->where(['kode'=>'divisi', 'no'=>$pemohon->divisi])->one();
                    return $divisi->nama;
                },
            ],
            'tanggal_trans',
            'keterangan:ntext',
                        
            [
                'attribute' => 'Status',
                'format' => 'raw',
                'value' => function ($model) {
                    $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
                    $status = TblRStatus::find()->where(['kode' => 'status_permintaan'])->andWhere(['no' => $detail['status']])->one();
                    return $status['nama'];
                },
            ],
 [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                date_default_timezone_set("Asia/Jakarta");
                $end = $model->tanggal_trans;
                $current_date = date("Y-m-d");

                if($current_date > $end){                
                      return '<div>'.'<center>'.Html::a('Peminjaman Kadaluarsa', ['tbl-t-permintaan/view','id'=>$model->permintaan_id],['class'=>' btn btn-danger']).'</center>'.'</div>'
                    ;
                }

                else{

               return '<div>'.'<center>'.Html::a('Detail Permintaan', ['tbl-t-permintaan/view','id'=>$model->permintaan_id],['class'=>' btn btn-success']).'</center>'.'</div>'
                    ;
            }
        },

            ],
            
        ],
    ]); ?>

</div>
