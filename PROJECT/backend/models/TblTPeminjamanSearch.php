<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTPeminjaman;

/**
 * TblTPeminjamanSearch represents the model behind the search form about `backend\models\TblTPeminjaman`.
 */
class TblTPeminjamanSearch extends TblTPeminjaman
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['peminjaman_id', 'pemohon', 'approval', 'approval_by', 'created_by', 'modified_by'], 'integer'],
            [['no_dokumen', 'tanggal_trans', 'tanggal_pinjam', 'rencana_kembali', 'rencana_waktu_kembali', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPeminjaman::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'peminjaman_id' => SORT_DESC, 
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'peminjaman_id' => $this->peminjaman_id,
            'pemohon' => $this->pemohon,
            'tanggal_trans' => $this->tanggal_trans,
            'tanggal_pinjam' => $this->tanggal_pinjam,
            'rencana_kembali' => $this->rencana_kembali,
            'rencana_waktu_kembali' => $this->rencana_waktu_kembali,
            'approval' => $this->approval,
            'approval_by' => $this->approval_by,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'no_dokumen', $this->no_dokumen])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
