<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTMutasiAset */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tmutasi-aset-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'buku_id')->textInput() ?>

    <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_trans')->textInput() ?>

    <?= $form->field($model, 'lokasi')->textInput() ?>

    <?= $form->field($model, 'baik')->textInput() ?>

    <?= $form->field($model, 'rusak')->textInput() ?>

    <?= $form->field($model, 'alasan_mutasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modified_date')->textInput() ?>

    <?= $form->field($model, 'modified_by')->textInput() ?>

    <?= $form->field($model, 'modified_ip')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
