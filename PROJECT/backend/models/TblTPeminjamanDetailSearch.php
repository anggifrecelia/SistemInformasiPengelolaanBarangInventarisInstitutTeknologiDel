<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TblTPeminjamanDetail;

/**
 * TblTPeminjamanDetailSearch represents the model behind the search form about `backend\models\TblTPeminjamanDetail`.
 */
class TblTPeminjamanDetailSearch extends TblTPeminjamanDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['peminjaman_detail_id', 'peminjaman_id', 'barang_id', 'jumlah', 'status_pinjam', 'status_kembali', 'status_kembali_by', 'created_by', 'modified_by'], 'integer'],
            [['satuan', 'keterangan', 'tanggal_kembali', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPeminjamanDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'peminjaman_detail_id' => $this->peminjaman_detail_id,
            'peminjaman_id' => $this->peminjaman_id,
            'barang_id' => $this->barang_id,
            'jumlah' => $this->jumlah,
            'status_pinjam' => $this->status_pinjam,
            'status_kembali' => $this->status_kembali,
            'status_kembali_by' => $this->status_kembali_by,
            'tanggal_kembali' => $this->tanggal_kembali,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
