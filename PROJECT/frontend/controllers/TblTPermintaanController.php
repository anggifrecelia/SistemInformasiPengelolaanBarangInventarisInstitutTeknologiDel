<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTPermintaan;
use frontend\models\TblTPermintaanDetail;
use frontend\models\TblTPermintaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\TblRBarang;
use frontend\models\Model;
use frontend\models\TblTStokbarang;
use kartik\mpdf\Pdf;

class TblTPermintaanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TblTPermintaanSearch(['pemohon'=>Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
//        $searchModel = new TblTPermintaanSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//            'dataProvider' => $dataProvider,
//        ]);
        $model = TblTPermintaan::find()->where(['permintaan_id'=>$id]);
        $dataProviderPermintaan = new \yii\data\ActiveDataProvider(
                [
                    'query'=>$model,
                ]
                );
        
        $models1TblPermintaanDetail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$id]);
        
        $dataProviderPermintaanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPermintaanDetail,
        ]);
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProviderPermintaanDetail'=>$dataProviderPermintaanDetail,
        ]);
    }

    public function actionGetPdf($id) {
        $model = TblTPermintaan::find()->where(['permintaan_id'=>$id]);
        $dataProviderPermintaan = new \yii\data\ActiveDataProvider(
            [
                'query'=>$model,
            ]
        );

        $models1TblPermintaanDetail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$id]);

        $dataProviderPermintaanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPermintaanDetail,
        ]);

        $pdf = new Pdf([
            'filename' => 'bukti_permintaan.pdf',
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('view2', ['dataProviderPermintaan' => $dataProviderPermintaan, 'dataProviderPermintaanDetail'=>$dataProviderPermintaanDetail, 'model' => $this->findModel($id)]),
            'options' => [
                'title' => 'Cetak Bukti Peminjaman - PDF' ,
                'subject' => 'Cetak Bukti Permintaan'
            ],
            'methods' => [
                'SetHeader' => ['Bukti Permintaan'],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }

    /**
     * Creates a new TblTPermintaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new TblTPermintaan();
//        $detail = [new TblTPermintaanDetail()];
//      
//
//        if ($model->load(Yii::$app->request->post()) && $detail->load(Yii::$app->request->post())) {
//            $model->pemohon = Yii::$app->user->id;
//            $model->created_by = Yii::$app->user->id;
//            $model->created_date = date('Y-m-d');
//            $model->created_ip = Yii::$app->getRequest()->getUserIP();
//            // $model->dateCreated = date('Y-m-d');
//            $model->save();
//            $detail->permintaan_id=$model->permintaan_id;
//            $detail->created_by = Yii::$app->user->id;
//            $detail->created_date = date('Y-m-d');
//            $detail->created_ip = Yii::$app->getRequest()->getUserIP();
//            $detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Requested'")->queryScalar();
//            // $dr->idBarang=$dii->id;
//            // $di->id = $model->id;
//            // $di->lokasi = $this->lokasi;
//            // $dr->jumlah = $this->jumlah;
//            // $dr->deskripsi = $this->deskripsi;
//            $detail->save();
//            try{
//                if($model->save() && $detail->save()){
//                Yii::$app->getSession()->setFlash(
//                    'success','Request Berhasil Dilakukan!'
//                );
//                return $this->redirect(['index']);
//            }
//            }catch(Exception $e){
//                Yii::$app->getSession()->setFlash(
//                    'error',"{$e->getMessage()}"
//                );
//            }
//            return $this->redirect(['index', 'id' => $model->id]);
//        } 
//        else 
//        {
//            return $this->render('create', [
//                'model' => $model,
//                'detail' => (empty($detail)) ? [new TbtTPermintaanDetail] : $detail,
//            ]);
//        }
//    }

    public function actionCreate()
    {
        $model = new TblTPermintaan();
        $modelsDetail = [new TblTPermintaanDetail()];
        
        $mei= date('Y-m-d');





        if ($model->load(Yii::$app->request->post())){

            // var_dump($model->tanggal_trans); die();


            $diff= strtotime($model->tanggal_trans) - strtotime($mei);
            $days = $diff / 60 / 60 / 24;



            // var_dump($days);die();

            if(!($model->tanggal_trans < date('Y-m-d') ) && $days >= 2)
            {
                $modelsDetail = Model::createMultiple(TblTPermintaanDetail::classname());
                Model::loadMultiple($modelsDetail, Yii::$app->request->post());

// var_dump($model->tanggal_trans);die();
$month = date('m', strtotime($model->tanggal_trans));
$year = date('Y', strtotime($model->tanggal_trans));
// var_dump($month);die();

                $model->bulan_trans= $month;
                $model->tahun_trans= $year;
                $model->pemohon = Yii::$app->user->id;
                $model->tipe = 1;
                $model->created_by = Yii::$app->user->id;
                $model->created_date = date('Y-m-d');
                $model->created_ip = Yii::$app->getRequest()->getUserIP();
                // validate all models  
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsDetail);

                if (!$valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                            foreach ($modelsDetail as $modelDetail) {
                                $modelDetail->permintaan_id = $model->permintaan_id;
                                $modelDetail->created_by = Yii::$app->user->id;
                                $modelDetail->created_date = date('Y-m-d');
                                $modelDetail->created_ip = Yii::$app->getRequest()->getUserIP();
                                $modelDetail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Requested'")->queryScalar();
                                if (! ($flag = $modelDetail->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash(
                    'error','Request Gagal ! Periksa tanggal request Anda!'
                );
                return $this->renderAjax('create', [
                    'model' => $model,
                    'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
                ]);
            }
        }
        
        return $this->renderAjax('create', [
            'model' => $model,
            'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
        ]);
        
    }
    
    /**
     * Updates an existing TblTPermintaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        //$detail = new TblTPermintaanDetail();
        //$barang = new TblRBarang();
        
        $model = $this->findModel($id);
        //$model1 = $detail->findModel($id);        
        if ($model->load(Yii::$app->request->post())) {
            $model->modified_by = Yii::$app->user->id;
            $model->modified_date = date('Y-m-d');
            $model->modified_ip = Yii::$app->getRequest()->getUserIP();
            
            $model->save();
            
            return $this->redirect(['view', 'id' => $model->permintaan_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPermintaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        Yii::$app->db->createCommand("delete from tbl_t_permintaan_detail  where permintaan_id = $id")->execute();
        Yii::$app->db->createCommand("delete from tbl_t_permintaan where permintaan_id = $id")->execute();
        $detail->save();
        
        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionApprove($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Approved'")->execute();
        Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 2, status_by = $user where permintaan_id = $id AND status = 1")->execute();
        $detail->save();
        
        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReject($id)
    {
        $searchModel = new TblTPermintaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        Yii::$app->db->createCommand("Update tbl_t_permintaan_detail set status = 3, status_by = $user where permintaan_id = $id AND status = 1")->execute();
        
       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
        //$detail->status_by = Yii::$app->user->id;
        $detail->save();
        
        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'dataProvider' => $dataProvider,
            ]);
    }
    /**
     * Finds the TblTPermintaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPermintaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPermintaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
