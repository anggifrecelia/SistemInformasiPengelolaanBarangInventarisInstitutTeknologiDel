<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\TblRStatus;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaan */

$this->title = 'Detail Pengadaan Barang'; //$model->pengadaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
   // $model = new backend\models\TblTPengadaan();
    $tipe = TblRStatus::findOne(['kode'=>'tipe_pengadaan_barang', 'no'=>$model->tipe]);
?>
<div class="tbl-tpengadaan-view">

    

<!--    <p>
        ?= Html::a('Update', ['update', 'id' => $model->pengadaan_id], ['class' => 'btn btn-primary']) ?>
        ?= Html::a('Delete', ['delete', 'id' => $model->pengadaan_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //pengadaan_id',
           // 'tipe',
            
            // [
            //     'attribute' => 'Tipe Pengadaan',
            //     'value' => $tipe->nama,
            // ],
            
            'kode_trans',
            'tanggal_trans',
//            'satuan'
//            'bulan_trans',
//            'tahun_trans',
//            'keterangan:ntext',
//            'created_date',
//            'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>

    <p><h2>Detail Pengadaan Barang</h2></p>
  <?=\yii\grid\GridView::widget([
        'dataProvider' => $dataProviderPengadaanDetail,
        'columns' => [
//            'pengadaan_detail_id',
//            'pengadaan_id',
//             [
//                'attribute' => 'Nama Barang',
//                'value' => function($model) {
//                    $id_bar= Yii::$app->db->createCommand('SELECT pengadaan_id FROM tbl_t_pengadaan_detail where pengadaan_id=' . $model->pengadaan_id)->queryScalar();
//
//                    $id_barang = Yii::$app->db->createCommand('SELECT barang_id FROM tbl_t_pengadaan_detail where pengadaan_id = ' . $id_bar)->queryScalar();
//
//                    return Yii::$app->db->createCommand('SELECT nama FROM tbl_r_barang where barang_id = ' . $id_barang)->queryScalar();
//                }
//            ],
            'barang.nama',
            // 'barang.deskripsi',
            'jumlah',
//            'satuan',
            [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                    $satuan = backend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
                    if($satuan !=Null){
                        return $satuan->nama;
                    }else{
                        return Null;
                    }
                    
                },
            ],
            'keterangan',
//            'created_date',
//            'created_by',
//            'created_ip',
//            'modified_date',
//            'modified_by',
//            'modified_ip',
        ],
    ]) ?>
    
</div>
