<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPeminjamanDetail */

$this->title = 'Update Tbl Tpeminjaman Detail: ' . $model->peminjaman_detail_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpeminjaman Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->peminjaman_detail_id, 'url' => ['view', 'id' => $model->peminjaman_detail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpeminjaman-detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
