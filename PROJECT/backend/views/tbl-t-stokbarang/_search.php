<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use backend\models\TblRBarang;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTStokbarangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tstokbarang-search">

    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

 <?= $form->field($model, 'barang_id')->dropDownList(ArrayHelper::map(TblRBarang::find()->where("(kategori = 1)")->all(),'barang_id', 'nama'),['prompt'=>'-Pilih Barang-', 'style'=>'width:300px'])?>

    <!--?= $form->field($model, 'gedung_id')->textInput(['style'=>'width:300px']); ?-->

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'modified_by') ?>

    <?php // echo $form->field($model, 'modified_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset',['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
