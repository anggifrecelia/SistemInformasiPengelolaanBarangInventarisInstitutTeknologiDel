<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;


use backend\models\TblTPermintaanDetail;
use backend\models\TblTPengadaanDetail;
use backend\models\TblRStatus;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTStokbarang */

$mei2=$model->stok_id;

// var_dump($mei2); die();

 $mei = (new \yii\db\Query())
                    ->select ('barang_id')
                    ->from('tbl_t_stokbarang')
                    ->where(['stok_id' => $mei2])
                    ->scalar();
// var_dump($mei);die();

 $namaBarang = (new \yii\db\Query())
                    ->select ('nama')
                    ->from('tbl_r_barang')
                    ->where(['barang_id' => $mei])
                    ->scalar();

// var_dump($namaBarang);die();



$this->title = $namaBarang;



$this->params['breadcrumbs'][] = ['label' => 'Stok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="tbl-tstokbarang-view">


 <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->stok_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->stok_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'stok_id',
            

              [
                'attribute' => 'barang_id',
                'value' => $model->barang->nama,
            ],


             [
                'attribute' => 'gedung_id',
                'value' => $model->gedung->nama,
            ],

            'kode',
            'jumlah',
            
            'satuan',

            'deskripsi',
            'deleted',
            'created_date',
            'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
        ],
    ]) ?>




<p align="center">
    

<!-- <?= Html::a('Lihat Permintaan Barang', ['tbl-t-stokbarang/view2', 'id' => $model->stok_id], ['class' => 'btn btn-primary']) ?>


<?= Html::a('Lihat Pengadaan Barang', ['update', 'id' => $model->stok_id], ['class' => 'btn btn-primary']) ?> -->

</p>

</div>
