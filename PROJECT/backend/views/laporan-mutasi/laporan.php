<?php


use yii\grid\GridView;
use backend\models\User;
use yii\helpers\Html;
use backend\models\TblRBarang;

?>

<h2 class="text-center"><u><b>Laporan Bulan <?= $namaBulan . ' ' . $tahun ?></b></u></h2>

<!--<h3><b>Pelanggaran Piket Makan</b></h3>

<h4><b>Daftar Pelanggar</b></h4>-->
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => 'No.'],
//        'peminjaman_id',

        
        'pencatatan_id',
 //       'pemohon',
        // [
        //     'attribute' => 'Pemohon',
        //   'format' => 'raw',
        //   'value' => function ($model) {
        //       $pemohon = User::find()->where(['id' => $model['pemohon']])->one();
        //       return $pemohon->nama;
        //    },

        'kode_trans',
        'tanggal_trans',
        'lokasi',
        'lokasi_tujuan',
        'jumlah',
        'alasan_mutasi',
             
                ]
            ])
                ?>
