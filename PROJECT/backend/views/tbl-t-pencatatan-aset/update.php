<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPencatatanAset */

$this->title = 'Update Tbl Tpencatatan Aset: ' . $model->pencatatan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpencatatan Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pencatatan_id, 'url' => ['view', 'pencatatan_id' => $model->pencatatan_id, 'created_date' => $model->created_date, 'created_by' => $model->created_by]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpencatatan-aset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
