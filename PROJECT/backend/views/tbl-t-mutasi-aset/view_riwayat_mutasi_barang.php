<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTMutasiAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Riwayat Mutasi Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'mutasi_id',
            // 'pencatatan_id',
            'kode_trans',
            'tanggal_trans',
            [
                'label' => 'Lokasi',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung->nama;
                },
            ],
            [
                'label' => 'Lokasi Tujuan',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung2->nama;
                },
            ],
            // 'lokasi',
            // 'lokasi_tujuan',
            'jumlah',
            'status_mutasi',
            'status_pindah',
            // 'alasan_mutasi',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
