<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\bootstrap\Modal;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTPencatatanAsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Riwayat Pencatatan Aset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpencatatan-aset-index">

    <h1><?= Html::encode($this->title) ?></h1>

 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
   
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pencatatan_id',
             [
                'label' => 'Barang',
                'format' => 'raw',
                'value' => function($model){
                    return $model->barang->nama;
                },
            ],

             [
                'label' => 'Gedung',
                'format' => 'raw',
                'value' => function($model){
                    return $model->gedung->nama;
                },
            ],
      
            'kode_trans',
            'tanggal_trans',
            'jumlah',
           'satuan',
            // 'keterangan:ntext',
            // 'sumber_perolehan',
            // 'kondisi',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',
            // 'buku_id',
            // 'status_pencatatan',

            
        ],
    ]); ?>
</div>
