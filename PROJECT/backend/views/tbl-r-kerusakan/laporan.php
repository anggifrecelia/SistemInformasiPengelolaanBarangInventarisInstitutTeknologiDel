<?php

use yii\grid\GridView;
use backend\models\User;
use yii\helpers\Html;
?>

<h2 class="text-center"><u><b>Laporan Bulan <?=$namaBulan . ' ' . $tahun?></b></u></h2>

<!--<h3><b>Pelanggaran Piket Makan</b></h3>

<h4><b>Daftar Pelanggar</b></h4>-->
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', 'header' => 'No.'],

//        'barang_id',
        [
                'attribute' => 'Nama Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $barang = \backend\models\TblRBarang::find()->where(['barang_id'=> $model['barang_id']])->one();
                    return $barang->nama;
                },
            ],
//        'no_laporan',
//        [
//            'attribute' => 'Pemohon',
//            'format' => 'raw',
//            'value' => function ($model) {
//                $pemohon = User::find()->where(['id'=>$model['pemohon']])->one();
//                return $pemohon->nama;
//            },
//        ],
        'tanggal',
//        'bulan',
//        'tahun',
        'jumlah',            
//        'satuan',   
         [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                     $satuan = \backend\models\TblRStatus::find()->where(['kode' => 'satuan_barang' ,'no'=> $model['satuan']])->one();                    
                        return $satuan->nama;                 
  
                },
            ],                
        'keterangan',
        
//        barang_id, no_laporan, pemohon, tanggal, bulan, tahun,satuan, keterangan 
    ],
    'summary' => '',
]); ?>
