<?php

use yii\helpers\Html;
use frontend\models\TblTPermintaanDetail;


/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPermintaan */

// $this->title = 'Form Permintaan Barang Habis';
$this->params['breadcrumbs'][] = ['label' => 'Permintaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tpermintaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
    ]) ?>

</div>
