<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblRKerusakan */

$this->title = 'Update Kerusakan: ' . ' ' . $model->kerusakan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Rkerusakans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kerusakan_id, 'url' => ['view', 'id' => $model->kerusakan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-rkerusakan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
