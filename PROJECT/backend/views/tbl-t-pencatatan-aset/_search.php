<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\TblTBukuInventory;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\TblTPencatatanAsetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpencatatan-aset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <?= $form->field($model, 'barang_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(TblTBukuInventory::find()->all(),'buku_id', 'deskripsi'),
        'language' => 'en',
        'options' => ['placeholder' => '- Pilih Barang -','id' => 'barangId'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
        'name' => 'dp_3',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'value' => '23-Februari-1998',
        'readonly'=>true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ],
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
