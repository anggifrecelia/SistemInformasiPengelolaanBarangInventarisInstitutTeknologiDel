Topik		: Sistem Informasi Pengelolaan Barang Inventaris Institut Teknologi Del
Kelompok	: PSI 12
Anggota		: 1. Arsantya Saragih (12S15020)
		  2. Riahta Mei (12S15038)
		  3. Widya Simbolon (12S15042)
		  4. Anggi Frecellia (12S15048)

Pembimbing	: Samuel I.G. Situmeang S.Ti., M.Sc.
Penguji 1	: Mario Simare-mare S.Kom., M.Sc.

Seminar
Hari/Tanggal	: Rabu, 06 Juni 2018
Pukul		: 17.30-19.00 WIB
Tempat		: GD516