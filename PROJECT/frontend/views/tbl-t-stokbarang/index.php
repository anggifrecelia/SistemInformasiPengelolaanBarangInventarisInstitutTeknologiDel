<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblTStokbarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stok Barang Habis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tstokbarang-index">

    <h1><?= Html::encode($this->title) ?></h1>
  

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'stok_id',
            [
                'attribute'=>'barang_id',
		          'value'=>'barang.nama',
            ],
            [
                'attribute'=>'gedung_id',
		          'value'=>'gedung.nama',
            ],
 [
                'attribute' => 'Available',
                'format' => 'raw',
                'value' => 'available',
            ],
            
            'kode',




            // 'satuan',
            // 'deskripsi',
            // 'deleted',
            // 'created_date',
            // 'created_by',
            // 'created_ip',
            // 'modified_date',
            // 'modified_by',
            // 'modified_ip',

            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '30'],
                'template' => '{view}{link}',
            ],
        ],
    ]);




     ?>
 <div class="form-group">
        <?= Html::a('Kembali', ['tbl-t-permintaan/index'], ['class' => 'btn btn-success']) ?>
    </div>


</div>
