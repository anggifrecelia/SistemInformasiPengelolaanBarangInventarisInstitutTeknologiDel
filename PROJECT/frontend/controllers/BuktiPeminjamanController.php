<?php
/**
 * Created by PhpStorm.
 * User: stu
 * Date: 02/06/2018
 * Time: 11:39
 */

class BuktiPeminjamanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-pdf' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT , pemohon, tanggal_trans, tanggal_pinjam, rencana_kembali, rencana_waktu_kembali,keterangan, approval FROM tbl_t_peminjaman WHERE MONTH(',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        return $this->render('index-laporan', [
            'dataProvider' => $dataProvider,
            'namaBulan' => $namaBulan,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);
    }

    public function actionGetPdf($bulan, $tahun, $namaBulan) {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT no_dokumen, pemohon, tanggal_trans, tanggal_pinjam, approval FROM tbl_t_peminjaman WHERE MONTH(tanggal_trans)=:bulan AND YEAR(tanggal_trans)=:tahun',
            'params' => [':bulan' => $bulan, ':tahun' => $tahun],
            'pagination' => false,
        ]);

        $pdf = new Pdf([
            'filename' => 'Laporan' . $bulan . $tahun,
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('laporan', ['dataProvider' => $dataProvider, 'namaBulan' => $namaBulan, 'tahun' => $tahun]),
            'options' => [
                'title' => 'Laporan Bulanan - PDF' ,
                'subject' => 'Laporan Bulan ' . $bulan . ' ' . $tahun
            ],
            'methods' => [
                'SetHeader' => ['Bang Edu||' . $bulan . ' ' . $tahun],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }
}

}