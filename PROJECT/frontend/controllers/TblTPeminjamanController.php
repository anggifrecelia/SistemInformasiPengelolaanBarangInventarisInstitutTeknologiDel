<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TblTPeminjaman;
use frontend\models\TblTPeminjamanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Model;
use frontend\models\TblTPeminjamanDetail;
use kartik\mpdf\Pdf;

/**
 * TblTPeminjamanController implements the CRUD actions for TblTPeminjaman model.
 */
class TblTPeminjamanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblTPeminjaman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblTPeminjamanSearch(['pemohon'=>Yii::$app->user->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblTPeminjaman model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = TblTPeminjaman::find()->where(['peminjaman_id'=>$id]);
        $dataProviderPeminjaman = new \yii\data\ActiveDataProvider(
                [
                    'query'=>$model,
                ]
                );
        
        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);
        
        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPeminjamanDetail,
        ]);
        
        return $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                    'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
        ]);
    }


public function actionView2($id)
    {
        $model = TblTPeminjaman::find()->where(['peminjaman_id'=>$id]);

        $dataProviderPeminjaman = new \yii\data\ActiveDataProvider(
                [
                    'query'=>$model,
                ]
                );
        
        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);
        
        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPeminjamanDetail,
        ]);
        
        return $this->render('view2', [
                    'model' => $this->findModel($id),
                    'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
        ]);
    }


    public function actionGetPdf($id) {
        $model = TblTPeminjaman::find()->where(['peminjaman_id'=>$id]);
        $dataProviderPeminjaman = new \yii\data\ActiveDataProvider(
            [
                'query'=>$model,
            ]
        );

        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);

        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPeminjamanDetail,
        ]);

        $pdf = new Pdf([
            'filename' => 'bukti_peminjaman.pdf',
            'mode' => Pdf::MODE_UTF8,
            'content' => $this->renderPartial('view3', ['dataProviderPeminjaman' => $dataProviderPeminjaman, 'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail, 'model' => $this->findModel($id)]),
            'options' => [
                'title' => 'Cetak Bukti Peminjaman - PDF' ,
                'subject' => 'Cetak Bukti Peminjaman'
            ],
            'methods' => [
                'SetHeader' => ['Cetak Bukti Peminjaman'],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }









    /**
     * Creates a new TblTPeminjaman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblTPeminjaman();
        $modelsDetail = [new TblTPeminjamanDetail()];
        

            $model->tanggal_trans= date('Y-m-d');
            // var_dump($model->tanggal_pinjam);die();


        if ($model->load(Yii::$app->request->post()))
        
        {

            $diff= strtotime($model->tanggal_pinjam) - strtotime($model->tanggal_trans);
            $days = $diff / 60 / 60 / 24;
            // var_dump($days);die();

            $diff2 = strtotime($model->rencana_kembali) - strtotime($model->tanggal_pinjam);
            $days2 = $diff2 / 60 / 60 / 24;



            if(!($model->tanggal_trans < date('Y-m-d')) && !($model->tanggal_pinjam < $model->tanggal_trans) && !($model->rencana_kembali < $model->tanggal_pinjam) && $days>=2 && $days2<=5)
            {
                $modelsDetail = Model::createMultiple(TblTPeminjamanDetail::classname());
                Model::loadMultiple($modelsDetail, Yii::$app->request->post());

                $model->pemohon = Yii::$app->user->id;
                $model->created_by = Yii::$app->user->id;
                $model->approval = 1;
                $model->created_date = date('Y-m-d');
                $model->created_ip = Yii::$app->getRequest()->getUserIP();
                // validate all models  
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsDetail);

                if (!$valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                            foreach ($modelsDetail as $modelDetail) {
                                $modelDetail->peminjaman_id = $model->peminjaman_id;
                                $modelDetail->created_by = Yii::$app->user->id;
                                $modelDetail->created_date = date('Y-m-d');
                                $modelDetail->created_ip = Yii::$app->getRequest()->getUserIP();
                                $modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
                                if (! ($flag = $modelDetail->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash(
                    'error','Request Gagal ! Periksa tanggal request Anda!'
                );
                return $this->renderAjax('create', [
                    'model' => $model,
                    'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
                ]);
            }
        }
        
        return $this->renderAjax('create', [
            'model' => $model,
            'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPeminjamanDetail] : $modelsDetail
        ]);
    }



/**
     * Creates a new TblTPeminjaman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate2()
    {
        $model = new TblTPeminjaman();
        $modelsDetail = [new TblTPeminjamanDetail()];
        

            $model->tanggal_trans= date('Y-m-d');
            // var_dump($model->tanggal_pinjam);die();


        if ($model->load(Yii::$app->request->post()))
        
        {

            $diff= strtotime($model->tanggal_pinjam) - strtotime($model->tanggal_trans);
            $days = $diff / 60 / 60 / 24;
            // var_dump($days);die();

            $diff2 = strtotime($model->rencana_kembali) - strtotime($model->tanggal_pinjam);
            $days2 = $diff2 / 60 / 60 / 24;



            if(!($model->tanggal_trans < date('Y-m-d')) && !($model->tanggal_pinjam < $model->tanggal_trans) && !($model->rencana_kembali < $model->tanggal_pinjam) && $days>=2 && $days2<=5)
            {
                $modelsDetail = Model::createMultiple(TblTPeminjamanDetail::classname());
                Model::loadMultiple($modelsDetail, Yii::$app->request->post());

                $model->pemohon = Yii::$app->user->id;
                $model->created_by = Yii::$app->user->id;
                $model->approval = 1;
                $model->created_date = date('Y-m-d');
                $model->created_ip = Yii::$app->getRequest()->getUserIP();
                // validate all models  
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsDetail);

                if (!$valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                            foreach ($modelsDetail as $modelDetail) {
                                $modelDetail->peminjaman_id = $model->peminjaman_id;
                                $modelDetail->created_by = Yii::$app->user->id;
                                $modelDetail->created_date = date('Y-m-d');
                                $modelDetail->created_ip = Yii::$app->getRequest()->getUserIP();
                                $modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
                                if (! ($flag = $modelDetail->save(false))) {
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash(
                    'error','Request Gagal ! Periksa tanggal request Anda!'
                );
                return $this->render('create', [
                    'model' => $model,
                    'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPermintaanDetail] : $modelsDetail
                ]);
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'modelsDetail' => (empty($modelsDetail)) ? [new TbtTPeminjamanDetail] : $modelsDetail
        ]);
    }


















    /**
     * Updates an existing TblTPeminjaman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       
        if ($model->load(Yii::$app->request->post())) {
            $model->modified_by = Yii::$app->user->id;
            $model->modified_date = date('Y-m-d');
            $model->modified_ip = Yii::$app->getRequest()->getUserIP();
            
            $model->save();
            return $this->redirect(['view', 'id' => $model->peminjaman_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblTPeminjaman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $searchModel = new TblTPeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);
        $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();
        Yii::$app->db->createCommand("delete from tbl_t_peminjaman_detail  where peminjaman_id = $id")->execute();
        Yii::$app->db->createCommand("delete from tbl_t_peminjaman where peminjaman_id = $id")->execute();
        $detail->save();

        return $this->render('index', [
                'model' => $model,
                'detail' => $detail,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionApprove($id)
    {
        $searchModel = new TblTPeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $model->approval = 2;
        $model->approval_by = $user;
        $model->save();
        $modelsDetail = \frontend\models\TblTPeminjamanDetail::find()->where(['peminjaman_id' => $model->peminjaman_id])->all();
        //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Disetujui'")->queryScalar();
        foreach ($modelsDetail as $modelDetail) {
                            //$modelDetail->peminjaman_id = $model->peminjaman_id;
                            //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
                            $modelDetail->status_pinjam = 2;
                            $modelDetail->save();
        }
//        $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();
//        Yii::$app->db->createCommand("Update tbl_t_peminjaman_detail set status_pinjam = 2, where permintaan_id = $id")->execute();
//        
//       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
//        //$detail->status_by = Yii::$app->user->id;
//        $model->save();
        
        
        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionReject($id)
    {
        $searchModel = new TblTPeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        //$detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        //Yii::$app->db->createCommand("Update tbl_t_peminjaman set approval = 3, approval_by = $user where peminjaman_id = $id")->execute();
        
       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
        //$detail->status_by = Yii::$app->user->id;
        //$model->save();
        $model->approval = 3;
        $model->approval_by = $user;
        $model->save();
        $modelsDetail = \frontend\models\TblTPeminjamanDetail::find()->where(['peminjaman_id' => $model->peminjaman_id])->all();
        //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Disetujui'")->queryScalar();
        foreach ($modelsDetail as $modelDetail) {
                            //$modelDetail->peminjaman_id = $model->peminjaman_id;
                            //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
                            $modelDetail->status_pinjam = 3;
                            $modelDetail->save();
        }
        
        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }
    
    public function actionSerah($id)
    {
        $searchModel = new TblTPeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);
        
        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPeminjamanDetail,
        ]);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        $modelDetail = \frontend\models\TblTPeminjamanDetail::find()->where(['peminjaman_id' => $model->peminjaman_id])->one();
        $modelDetail->status_kembali = 2;
        $modelDetail->status_kembali_by = $user;
        $modelDetail->save();
        //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Disetujui'")->queryScalar();
//        foreach ($modelsDetail as $modelDetail) {
//                            //$modelDetail->peminjaman_id = $model->peminjaman_id;
//                            //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
//                            $modelDetail->status_pinjam = 2;
//                            $modelDetail->save();
//        }
//        $detail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$model->peminjaman_id])->one();
//        Yii::$app->db->createCommand("Update tbl_t_peminjaman_detail set status_pinjam = 2, where permintaan_id = $id")->execute();
//        
//       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
//        //$detail->status_by = Yii::$app->user->id;
//        $model->save();
        
        
        return $this->render('view', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
            ]);
    }
    
    public function actionKembali($id)
    {
        $searchModel = new TblTPeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $models1TblPeminjamanDetail = TblTPeminjamanDetail::find()->where(['peminjaman_id'=>$id]);
        
        $dataProviderPeminjamanDetail = new \yii\data\ActiveDataProvider([
            'query'=>$models1TblPeminjamanDetail,
        ]);
        $user = Yii::$app->user->id; 
        $model = $this->findModel($id);
        //$detail = TblTPermintaanDetail::find()->where(['permintaan_id'=>$model->permintaan_id])->one();
        //Yii::$app->db->createCommand("Update tbl_t_peminjaman set approval = 3, approval_by = $user where peminjaman_id = $id")->execute();
        
       //$detail->status = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_permintaan' AND nama = 'Rejected'")->queryScalar();
        //$detail->status_by = Yii::$app->user->id;
        //$model->save();
        $modelDetail = \frontend\models\TblTPeminjamanDetail::find()->one();
        $modelDetail->status_kembali = 1;
        $modelDetail->status_kembali_by = $user;
        $modelDetail->save();
        //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Disetujui'")->queryScalar();
//        foreach ($modelsDetail as $modelDetail) {
//                            //$modelDetail->peminjaman_id = $model->peminjaman_id;
//                            //$modelDetail->status_pinjam = Yii::$app->db->createCommand("SELECT tbl_r_status.no FROM tbl_r_status WHERE kode = 'status_pinjam' AND nama = 'Requested'")->queryScalar();
//                            $modelDetail->status_pinjam = 3;
//                            $modelDetail->save();
//        }
        
        return $this->render('view', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderPeminjamanDetail'=>$dataProviderPeminjamanDetail,
            ]);
    }
    
    /**
     * Finds the TblTPeminjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TblTPeminjaman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblTPeminjaman::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
