<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_r_kerusakan".
 *
 * @property integer $kerusakan_id
 * @property integer $barang_id
 * @property integer $berita_acara_id
 * @property string $no_laporan
 * @property string $tanggal
 * @property integer $jumlah
 * @property string $satuan
 * @property string $keterangan
 * @property string $created_date
 * @property integer $created_by
 * @property string $created_ip
 * @property string $modified_date
 * @property integer $modified_by
 * @property string $modified_ip
 * @property integer $bulan
 * @property string $tahun
 *
 * @property TblRBarang $barang
 */
class TblRKerusakan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_r_kerusakan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['barang_id', 'berita_acara_id'], 'required'],
            [['barang_id', 'berita_acara_id', 'jumlah','gedung_id', 'created_by', 'modified_by', 'bulan'], 'integer'],
            [['tanggal', 'created_date', 'modified_date', 'tahun'], 'safe'],
            [['keterangan'], 'string'],
            [['no_laporan'], 'string', 'max' => 30],
            [['satuan', 'created_ip', 'modified_ip'], 'string', 'max' => 15],
            [['barang_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRBarang::className(), 'targetAttribute' => ['barang_id' => 'barang_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kerusakan_id' => 'ID',
            'barang_id' => 'Nama Barang',
            'gedung_id' => 'Gedung',
            'berita_acara_id' => 'Berita Acara ID',
            'no_laporan' => 'No Laporan',
            'tanggal' => 'Tanggal',
            'jumlah' => 'Jumlah',
            'satuan' => 'Satuan',
            'keterangan' => 'Keterangan',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
            'created_ip' => 'Created Ip',
            'modified_date' => 'Modified Date',
            'modified_by' => 'Modified By',
            'modified_ip' => 'Modified Ip',
            'bulan' => 'Bulan',
            'tahun' => 'Tahun',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBarang()
    {
        return $this->hasOne(TblRBarang::className(), ['barang_id' => 'barang_id']);
    }
}
