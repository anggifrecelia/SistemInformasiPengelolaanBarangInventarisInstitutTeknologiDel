<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TblTPengadaan */

$this->title = 'Update Tbl Tpengadaan: ' . ' ' . $model->pengadaan_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tpengadaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pengadaan_id, 'url' => ['view', 'id' => $model->pengadaan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-tpengadaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
