<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblTBukuInventorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Buku Inventory';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tbuku-inventory-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('/tbl-t-buku-inventory/_menu') ?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
  

    <p>
<?= Html::button('Daftarkan Barang Aset Baru', ['value'=>Url::to('index.php?r=tbl-t-buku-inventory/create'),'class' => 'btn btn-success', 'id'=>'modalButton']) ?>



<?php
        Modal::begin([
                'header'=>'<h4>Form Pendaftaran Barang Aset</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
       ]);

        echo "<div id = 'modalContent'> </div>";
        Modal::end();

    ?>

    </p>




   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Nama Barang',
                'format' => 'raw',
                'value' => function ($model) {
                    $barang = \backend\models\TblRBarang::find()->where(['barang_id'=>$model->barang_id])->one();
                    return $barang->nama;
                },
            ],
            [
                'attribute' => 'Gedung',
                'value' => function ($model) {
                    $gedung = backend\models\TblRGedung::find()->where(['gedung_id'=>$model->gedung_id])->one();
                    return $gedung->nama;
                },
            ],
            'baik',
            'rusak',
            'jumlah',
            'available',

            [
                'attribute' => 'Satuan',
                'format' => 'raw',
                'value' => function ($model) {
                    $satuan = backend\models\TblRStatus::find()->where(['kode'=>'satuan_barang', 'no'=>$model->satuan])->one();
                    if($satuan !=Null){
                        return $satuan->nama;
                    }else{
                        return Null;
                    }

                },
            ],

            [ 'attribute' => 'Keterangan',
                'format' => 'raw',
                'value' => function ($model){
                    return '<div>'.'<center>'.Html::a('Detail', ['tbl-t-buku-inventory/view','id'=>$model->buku_id],['class'=>'  btn btn-success']).'</center>'.'</div>';
                },
            ],

            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function($model){
                    return '<div><center>' . Html::a('Lapor Kerusakan', ['update', 'id'=>$model->buku_id, 'satuan'=>$model->satuan], ['class' => 'btn btn-success'
                        ]). '</center></div>';
                }
            ],
        ],
    ]); ?>
</div>
