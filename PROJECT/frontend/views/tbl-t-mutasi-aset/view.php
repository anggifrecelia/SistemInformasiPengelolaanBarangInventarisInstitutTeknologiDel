<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblTMutasiAset */

$this->title = $model->mutasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Tmutasi Asets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tmutasi-aset-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mutasi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mutasi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mutasi_id',
            'buku_id',
            'kode_trans',
            'tanggal_trans',
            'lokasi',
            'baik',
            'rusak',
            'alasan_mutasi',
            'created_date',
            'created_by',
            'created_ip',
            'modified_date',
            'modified_by',
            'modified_ip',
        ],
    ]) ?>

</div>
