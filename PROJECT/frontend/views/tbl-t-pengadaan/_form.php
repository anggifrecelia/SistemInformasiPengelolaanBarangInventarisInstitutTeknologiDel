<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\TblRStatus;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model frontend\models\TblTPengadaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-tpengadaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipe')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'kategori_barang')")->all(),'no', 'nama'),['prompt'=>'-Pilih Kategori Barang-'])?>
  
    
            <?= $form->field($model, 'kode_trans')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_trans')->widget(
        DatePicker::classname(), [
            'name' => 'dp_3',
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'value' => '23-Februari-1998',
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ],
        ]); ?>

    <?= $form->field($model, 'bulan_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'bulan')")->all(),'no', 'nama'),['prompt'=>'-Pilih Bulan-'])?>

    <?= $form->field($model, 'tahun_trans')->dropDownList(ArrayHelper::map(TblRStatus::find()->where("(kode = 'tahun')")->all(),'no', 'nama'),['prompt'=>'-Pilih Tahun-'])?>

   
    <div class="form-group">
        <p align="center">
             <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </p>
       
    </div>

    <?php ActiveForm::end(); ?>

</div>
