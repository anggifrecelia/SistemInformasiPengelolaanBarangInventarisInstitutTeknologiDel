<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TblTPermintaanDetail;

/**
 * TblTPermintaanDetailSearch represents the model behind the search form about `frontend\models\TblTPermintaanDetail`.
 */
class TblTPermintaanDetailSearch extends TblTPermintaanDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permintaan_detail_id', 'permintaan_id', 'barang_id', 'jumlah', 'status', 'status_by', 'created_by', 'modified_by'], 'integer'],
            [['satuan', 'keterangan', 'created_date', 'created_ip', 'modified_date', 'modified_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblTPermintaanDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'permintaan_detail_id' => $this->permintaan_detail_id,
            'permintaan_id' => $this->permintaan_id,
            'barang_id' => $this->barang_id,
            'jumlah' => $this->jumlah,
            'status' => $this->status,
            'status_by' => $this->status_by,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'modified_date' => $this->modified_date,
            'modified_by' => $this->modified_by,
        ]);

        $query->andFilterWhere(['like', 'satuan', $this->satuan])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'modified_ip', $this->modified_ip]);

        return $dataProvider;
    }
}
